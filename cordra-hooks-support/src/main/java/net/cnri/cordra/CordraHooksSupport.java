package net.cnri.cordra;

import java.util.concurrent.Callable;

import net.cnri.cordra.api.CordraClient;
import net.cnri.cordra.api.CordraException;

public interface CordraHooksSupport {
    CordraClient getCordraClient();

    void addShutdownHook(Callable<Void> shutdownHook);
    default void addShutdownHook(Runnable shutdownHook) {
        addShutdownHook(() -> { shutdownHook.run(); return null; });
    }

    String signWithCordraKey(String payload, boolean useJsonSerialization) throws CordraException;

    boolean verifyWithCordraKey(String jwt) throws CordraException;

}
