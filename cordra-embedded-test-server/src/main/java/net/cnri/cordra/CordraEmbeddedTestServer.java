package net.cnri.cordra;

import com.google.gson.JsonObject;
import net.cnri.cordra.api.EmbeddedServerCordraClient;
import net.cnri.cordra.util.GsonUtility;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CordraEmbeddedTestServer {
    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Usage: java -jar server.jar CORDRA_WAR_PATH CLIENT_CONFIG_FILE EXTRA_LIB_PATH CORDRA_CONFIG_FILE");
            System.exit(1);
        }
        Path cordraWarPath = Paths.get(args[0]);
        if (Files.notExists(cordraWarPath)) {
            System.err.println("Cordra war not found at given path " + args[0]);
            System.exit(1);
        }
        Path configPath = Paths.get(args[1]);
        if (Files.exists(configPath)) {
            System.out.println("Existing config file " + args[1] + " found. Will not start test server.");
            System.exit(0);
        }
        Path jarPath = null;
        if (args.length > 2) {
            jarPath = Paths.get(args[2]);
            System.out.println("Adding jar files to classpath from: " + jarPath);
        }
        String cordraConfig = null;
        if (args.length > 3) {
            Path cordraConfigPath = Paths.get(args[3]);
            if (Files.exists(cordraConfigPath)) {
                cordraConfig = new String(Files.readAllBytes(cordraConfigPath), StandardCharsets.UTF_8);
                System.out.println("Using Cordra config: " + args[3]);
            }
        }

        @SuppressWarnings("resource")
        EmbeddedServerCordraClient instance = EmbeddedServerCordraClient
                .newInstanceWithConfig(cordraWarPath, cordraConfig, jarPath, null);
        System.out.println("Cordra running at base uri " + instance.getBaseUri());
        JsonObject config = new JsonObject();
        config.addProperty("baseUri", instance.getBaseUri());
        config.addProperty("username", "admin");
        config.addProperty("password", "changeit");
        String configStr = GsonUtility.getPrettyGson().toJson(config);
        Files.write(configPath, configStr.getBytes(StandardCharsets.UTF_8));
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("Shutting down test server");
            try {
                instance.close();
                Files.deleteIfExists(configPath);
            } catch (Exception e) {
                // no-op
            }
            System.out.println("Cleanup complete");
        }));
    }
}
