package net.cnri.cordra.api;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;

public interface CallResponseHandler {
    void setMediaType(String mediaType);
    void setFilename(String filename);
    void setRange(boolean partial, boolean unsatisfied, Long start, Long end, Long size);
    void setLength(long length);
    OutputStream getOutputStream() throws IOException;
    Writer getWriter() throws IOException;
}
