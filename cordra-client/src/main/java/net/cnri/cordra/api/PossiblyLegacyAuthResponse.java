package net.cnri.cordra.api;

public class PossiblyLegacyAuthResponse extends AuthResponse {
    // for use with Gson response

    @Deprecated
    @SuppressWarnings("hiding")
    public boolean isActiveSession = false;
}
