package net.cnri.cordra.api;

public class AccessTokenResponse extends AuthResponse {
    public String access_token;
    public String token_type;
}

