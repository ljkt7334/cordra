package net.cnri.cordra.api;

import java.io.InputStream;
import com.google.gson.JsonElement;

public interface CallResponseHandlerEnabledCordraClient extends CordraClient {

    default void getPayload(String id, String payloadName, CallResponseHandler handler) throws CordraException {
        getPayload(id, payloadName, handler, new Options().setUseDefaultCredentials(true));
    }

    void getPayload(String id, String payloadName, CallResponseHandler handler, Options options) throws CordraException;

    default void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler) throws CordraException {
        getPartialPayload(id, payloadName, start, end, handler, new Options().setUseDefaultCredentials(true));
    }

    void getPartialPayload(String id, String payloadName, Long start, Long end, CallResponseHandler handler, Options options) throws CordraException;

    default void call(String objectId, String methodName, JsonElement params, CallResponseHandler handler) throws CordraException {
        call(objectId, methodName, params, handler, new Options().setUseDefaultCredentials(true));
    }

    void call(String objectId, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException;

    default void call(String objectId, String methodName, InputStream input, CallResponseHandler handler) throws CordraException {
        call(objectId, methodName, input, handler, new Options().setUseDefaultCredentials(true));
    }

    void call(String objectId, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException;

    default void callForType(String type, String methodName, JsonElement params, CallResponseHandler handler) throws CordraException {
        callForType(type, methodName, params, handler, new Options().setUseDefaultCredentials(true));
    }

    void callForType(String type, String methodName, JsonElement params, CallResponseHandler handler, Options options) throws CordraException;

    default void callForType(String type, String methodName, InputStream input, CallResponseHandler handler) throws CordraException {
        callForType(type, methodName, input, handler, new Options().setUseDefaultCredentials(true));
    }

    void callForType(String type, String methodName, InputStream input, CallResponseHandler handler, Options options) throws CordraException;
}
