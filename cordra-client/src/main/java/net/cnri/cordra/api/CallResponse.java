package net.cnri.cordra.api;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;

public class CallResponse implements Closeable {
    public CallHeaders headers = new CallHeaders();
    public InputStream body;

    @Override
    public void close() throws IOException {
        body.close();
    }
}
