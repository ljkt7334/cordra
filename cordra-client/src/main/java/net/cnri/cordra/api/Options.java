package net.cnri.cordra.api;

import com.google.gson.JsonObject;

import java.security.PrivateKey;
import java.util.List;

public class Options {
    public String authHeader;
    public JsonObject authTokenInput;
    public String doipClientId;
    public JsonObject doipAuthentication;
    public String userId;
    public String username;
    public String password;
    public String asUserId;
    public PrivateKey privateKey;
    public String token;
    public boolean isDryRun;
    public boolean full;
    public boolean useDefaultCredentials = false;
    public boolean reindexBatchLockObjects = true;
    public boolean includeCrud = false;
    public boolean includeResponseContext = false;
    public JsonObject requestContext;
    public CallHeaders callHeaders;
    public List<String> filter;

    public Options() { }

    public String getUsername() {
        return username;
    }

    public Options setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Options setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getAsUserId() {
        return asUserId;
    }

    public Options setAsUserId(String asUserId) {
        this.asUserId = asUserId;
        return this;
    }

    public boolean isDryRun() {
        return isDryRun;
    }

    public Options setDryRun(boolean isDryRun) {
        this.isDryRun = isDryRun;
        return this;
    }

    public Options setIncludeCrud(boolean includeCrud) {
        this.includeCrud = includeCrud;
        return this;
    }

    public Options setIncludeResponseContext(boolean includeResponseContext) {
        this.includeResponseContext = includeResponseContext;
        return this;
    }

    public Options setFull(boolean full) {
        this.full = full;
        return this;
    }

    public boolean isUseDefaultCredentials() {
        return useDefaultCredentials;
    }

    public Options setUseDefaultCredentials(boolean useDefaultCredentials) {
        this.useDefaultCredentials = useDefaultCredentials;
        return this;
    }

    public String getUserId() {
        return userId;
    }

    public Options setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    public Options setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
        return this;
    }

    public String getToken() {
        return token;
    }

    public Options setToken(String token) {
        this.token = token;
        return this;
    }

    public JsonObject getRequestContext() {
        return requestContext;
    }

    public Options setRequestContext(JsonObject requestContext) {
        this.requestContext = requestContext;
        return this;
    }

    public CallHeaders getCallHeaders() {
        return callHeaders;
    }

    public Options setCallHeaders(CallHeaders callHeaders) {
        this.callHeaders = callHeaders;
        return this;
    }

    public String getAuthHeader() {
        return authHeader;
    }

    public Options setAuthHeader(String authHeader) {
        this.authHeader = authHeader;
        return this;
    }

    public List<String> getFilter() {
        return filter;
    }

    public Options setFilter(List<String> filter) {
        this.filter = filter;
        return this;
    }
}
