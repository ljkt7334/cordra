package net.cnri.cordra.util.cmdline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.HttpCordraClient;
import net.cnri.cordra.api.SearchResults;
import net.cnri.cordra.util.SearchUtil;

public class SchemaImporter {

    private String baseUri;
    private String username;
    private String password;
    private String name;
    private String schema;
    private String javascript;
    private String schemaBaseUri;
    private String fullObject;
    private boolean isUpdate;
    private boolean deleteExistingJs;

    public static void main(String[] args) throws Exception {
        new SchemaImporter().run(args);
    }

    void run(String[] args) throws Exception {
        OptionSet options = parseOptions(args);
        extractOptions(options);
        JsonObject schemaInstanceJsonObject = buildSchemaInstanceJsonObject();
        if (isUpdate) {
            updateSchemaOnCordra(schemaInstanceJsonObject);
        } else {
            createSchemaOnCordra(schemaInstanceJsonObject);
        }
        System.out.println("Success");
    }

    private OptionSet parseOptions(String[] args) throws IOException {
        OptionParser parser = new OptionParser();
        parser.acceptsAll(Arrays.asList("h", "help")).forHelp();
        parser.acceptsAll(Arrays.asList("b", "base-uri"), "Cordra base URI").withRequiredArg().required();
        parser.acceptsAll(Arrays.asList("u", "username")).withRequiredArg().required();
        parser.acceptsAll(Arrays.asList("p", "password"), "Can be entered as standard input").withRequiredArg();
        parser.acceptsAll(Arrays.asList("o", "object"), "Object file (contains name, schema, optional JavaScript, etc.)").withRequiredArg();
        parser.acceptsAll(Arrays.asList("n", "name"), "Name of schema").requiredUnless("o").withRequiredArg();
        parser.acceptsAll(Arrays.asList("s", "schema"), "JSON schema file").requiredUnless("o").withRequiredArg();
        parser.acceptsAll(Arrays.asList("j", "javascript"), "Enrichment JavaScript file, optional").withRequiredArg();
        parser.acceptsAll(Collections.singletonList("schema-base-uri"), "baseUri property of Schema object").withRequiredArg();
        parser.acceptsAll(Collections.singletonList("update"), "Update existing schema");
        parser.acceptsAll(Collections.singletonList("delete-javascript"), "Delete existing javascript if update object doesn't contain any. Implies --update");
        OptionSet options;
        try {
            options = parser.parse(args);
        } catch (Exception e) {
            System.out.println("Error parsing options: " + e.getMessage());
            parser.printHelpOn(System.out);
            System.exit(1);
            return null;
        }
        if (options.has("h")) {
            parser.printHelpOn(System.out);
            System.exit(1);
            return null;
        }
        return options;
    }

    private void extractOptions(OptionSet options) throws IOException {
        baseUri = (String)options.valueOf("base-uri");
        username = (String)options.valueOf("username");
        password = (String)options.valueOf("password");
        isUpdate = options.has("update");
        if (options.has("delete-javascript")) {
            deleteExistingJs = true;
            isUpdate = true;
        }
        if (password == null) {
            System.out.print("Password: ");
            try (
                InputStreamReader isr = new InputStreamReader(System.in, StandardCharsets.UTF_8);
                BufferedReader reader = new BufferedReader(isr)
            ) {
                password = reader.readLine();
            }
        }
        if (options.has("o")) {
            String objectFilename = (String)options.valueOf("o");
            try {
                fullObject = new String(Files.readAllBytes(Paths.get(objectFilename)), StandardCharsets.UTF_8);
            } catch (Exception e) {
                System.err.println("Exception reading object file: " + objectFilename);
                throw e;
            }
        }
        name = (String) options.valueOf("name");
        String schemaFilename = (String) options.valueOf("schema");
        String javascriptFilename = (String) options.valueOf("javascript");
        try {
            schema = new String(Files.readAllBytes(Paths.get(schemaFilename)), StandardCharsets.UTF_8);
        } catch (Exception e) {
            System.err.println("Exception reading schema file: " + schemaFilename);
            throw e;
        }
        javascript = null;
        if (javascriptFilename != null) {
            try {
                javascript = new String(Files.readAllBytes(Paths.get(javascriptFilename)), StandardCharsets.UTF_8);
            } catch (Exception e) {
                System.err.println("Exception reading javascript file: " + javascriptFilename);
                throw e;
            }
        }
        schemaBaseUri = (String) options.valueOf("schema-base-uri");
    }

    private JsonObject buildSchemaInstanceJsonObject() {
        JsonObject schemaInstanceJsonObject;
        if (fullObject == null) {
            schemaInstanceJsonObject = new JsonObject();
        } else {
            JsonObject fullJsonObject = JsonParser.parseString(fullObject).getAsJsonObject();
            if (fullJsonObject.has("content")) {
                schemaInstanceJsonObject = fullJsonObject.get("content").getAsJsonObject();
            } else {
                schemaInstanceJsonObject = fullJsonObject;
            }
        }
        if (!schemaInstanceJsonObject.has("identifier")) schemaInstanceJsonObject.addProperty("identifier", "");
        if (name != null) schemaInstanceJsonObject.addProperty("name", name);
        if (schemaBaseUri != null) schemaInstanceJsonObject.addProperty("baseUri", schemaBaseUri);
        if (schema != null) schemaInstanceJsonObject.add("schema", JsonParser.parseString(schema));
        if (javascript != null) schemaInstanceJsonObject.addProperty("javascript", javascript);
        return schemaInstanceJsonObject;
    }

    private void createSchemaOnCordra(JsonObject schemaInstanceJsonObject) throws CordraException, IOException {
        try (HttpCordraClient cordraClient = new HttpCordraClient(baseUri, username, password)) {
            cordraClient.create("Schema", schemaInstanceJsonObject);
        }
    }

    private void updateSchemaOnCordra(JsonObject schemaInstanceJsonObject) throws CordraException, IOException {
        String schemaName = schemaInstanceJsonObject.get("name").getAsString();
        try (
            HttpCordraClient cordraClient = new HttpCordraClient(baseUri, username, password);
            SearchResults<CordraObject> searchResults = cordraClient.search("+type:Schema +/name:\"" + SearchUtil.escape(schemaName) + "\"");
        ) {
            for (CordraObject co : searchResults) {
                if (schemaName.equals(co.content.getAsJsonObject().get("name").getAsString())) {
                    if (!deleteExistingJs && !schemaInstanceJsonObject.has("javascript")) {
                        JsonElement existingJs = co.content.getAsJsonObject().get("javascript");
                        if (existingJs != null) {
                            schemaInstanceJsonObject.add("javascript", existingJs);
                        }
                    }
                    cordraClient.update(co.id, schemaInstanceJsonObject);
                    return;
                }
            }
            throw new BadRequestCordraException("Could not find existing schema named " + schemaName);
        }
    }
}
