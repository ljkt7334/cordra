module.exports = function (context, javaDirectIo, usedDirectOutputAtomicBoolean, hasInput) {
    let gotParams = false;
    let gotDirectInput = false;
    const directIo = {};
    if (hasInput) {
        directIo.getInputAsJavaInputStream = function () {
            if (gotParams) throw new Error("Can't get direct input after using context.params");
            gotDirectInput = true;
            return javaDirectIo.getInputAsInputStream();
        };
        directIo.getInputAsJavaReader = function () {
            if (gotParams) throw new Error("Can't get direct input after using context.params");
            gotDirectInput = true;
            return javaDirectIo.getInputAsReader();
        };
        directIo.getInputAsUint8Array = function () {
            if (gotParams) throw new Error("Can't get direct input after using context.params");
            gotDirectInput = true;
            const byteArray = javaDirectIo.getInputAsBytes();
            if (!byteArray) return byteArray;
            const uint8Array = new Uint8Array(byteArray.length);
            uint8Array.set(Java.from(byteArray));
            return uint8Array;
        };
        directIo.getInputAsString = function () {
            if (gotParams) throw new Error("Can't get direct input after using context.params");
            gotDirectInput = true;
            return javaDirectIo.getInputAsString();
        };
        directIo.getInputAsJson = function () {
            if (gotParams) throw new Error("Can't get direct input after using context.params");
            gotDirectInput = true;
            const inputString = javaDirectIo.getInputAsString();
            if (!inputString) return undefined;
            return JSON.parse(inputString);
        };
    }
    directIo.getOutputAsJavaOutputStream = function () {
        usedDirectOutputAtomicBoolean.set(true);
        return javaDirectIo.getOutputAsOutputStream();
    };
    directIo.getOutputAsJavaWriter = function () {
        usedDirectOutputAtomicBoolean.set(true);
        return javaDirectIo.getOutputAsWriter();
    };
    directIo.writeOutputUint8Array = function (bytes) {
        usedDirectOutputAtomicBoolean.set(true);
        const javaByteArray = Java.to(Array.prototype.slice.call(bytes), 'byte[]');
        javaDirectIo.writeOutputBytes(javaByteArray);
    };
    directIo.writeOutputString = function (string) {
        usedDirectOutputAtomicBoolean.set(true);
        javaDirectIo.writeOutputString(string);
    };
    directIo.writeOutputJson = function (json) {
        usedDirectOutputAtomicBoolean.set(true);
        javaDirectIo.writeOutputString(JSON.stringify(json));
    };
    if (hasInput) {
        directIo.getInputMediaType = function () {
            return javaDirectIo.getInputMediaType();
        };
        directIo.getInputFilename = function () {
            return javaDirectIo.getInputFilename();
        };
    }
    directIo.setOutputMediaType = function (mediaType) {
        javaDirectIo.setOutputMediaType(mediaType);
    };
    directIo.setOutputFilename = function (filename) {
        javaDirectIo.setOutputFilename(filename);
    };
    if (hasInput) {
        if (!context.params) {
            Object.defineProperty(context, 'params', {
                configurable: true,
                enumerable: true,
                get: function () {
                    if (gotDirectInput) throw new Error("Can't use context.params after getting direct input");
                    const params = directIo.getInputAsJson();
                    gotParams = true;
                    delete context.params;
                    context.params = params;
                    return params;
                },
                set: function (value) {
                    delete context.params;
                    context.params = value;
                }
            });
        }
    }
    Object.defineProperty(context, 'directIo', {
        value: directIo
    });
}
