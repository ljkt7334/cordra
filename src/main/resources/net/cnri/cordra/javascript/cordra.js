module.exports.get = function (id, full) {
    return JSON.parse(_cordraReturningStrings.get(id, full));
};

module.exports.search = function (queryOrQueryPlusParams, pageNumOrParams, pageSize, sortFieldsString) {
    var query;
    var paramsObj;
    if (queryOrQueryPlusParams && typeof queryOrQueryPlusParams === 'object' && !(queryOrQueryPlusParams instanceof String)) {
        query = queryOrQueryPlusParams.query;
        paramsObj = queryOrQueryPlusParams;
    } else if (pageNumOrParams && typeof pageNumOrParams === 'object' && !(pageNumOrParams instanceof Number)) {
        query = queryOrQueryPlusParams;
        paramsObj = pageNumOrParams;
    } else {
        query = queryOrQueryPlusParams;
        paramsObj = {};
        if (pageNumOrParams !== undefined) paramsObj.pageNum = pageNumOrParams;
        if (pageSize !== undefined) paramsObj.pageSize = pageSize;
        if (sortFieldsString !== undefined) paramsObj.sortFields = sortFieldsString;
    }
    if (paramsObj.pageNum === undefined) paramsObj.pageNum = 0;
    if (paramsObj.pageSize === undefined) paramsObj.pageSize = -1;
    if (paramsObj.sortFields && (typeof paramsObj.sortFields === 'string' || paramsObj.sortFields instanceof String)) {
        paramsObj.sortFields = paramsObj.sortFields.split(",").map(stringToSortField);
    }
    if (paramsObj.sortFields) {
        for (var i = 0; i < paramsObj.sortFields.length; i++) {
            if (typeof paramsObj.sortFields[i] === 'string' || paramsObj.sortFields[i] instanceof String) {
                paramsObj.sortFields[i] = stringToSortField(paramsObj.sortFields[i]);
            }
        }
    }
    return JSON.parse(_cordraReturningStrings.search(query, JSON.stringify(paramsObj)));
};

function stringToSortField(sortFieldString) {
    if (!sortFieldString) return { name: "" };
    var parts = sortFieldString.split(' ');
    if (parts.length === 1) {
        return { name: parts[0] };
    } else {
        var reverse = parts[1].toUpperCase() === 'DESC';
        return { name: parts[0], reverse: reverse };
    }
}

module.exports.getPayloadAsJavaInputStream = function (id, payloadName) {
    return _cordraReturningStrings.getPayloadAsInputStream(id, payloadName);
};

module.exports.getPayloadAsJavaReader = function (id, payloadName) {
    return _cordraReturningStrings.getPayloadAsReader(id, payloadName);
};

module.exports.getPayloadAsUint8Array = function (id, payloadName) {
    var byteArray = _cordraReturningStrings.getPayloadAsBytes(id, payloadName);
    if (!byteArray) return byteArray;
    var uint8Array = new Uint8Array(byteArray.length);
    uint8Array.set(Java.from(byteArray));
    return uint8Array;
};

module.exports.getPayloadAsString = function (id, payloadName) {
    return _cordraReturningStrings.getPayloadAsString(id, payloadName);
};

module.exports.getPayloadAsJson = function (id, payloadName) {
    var s = _cordraReturningStrings.getPayloadAsString(id, payloadName);
    if (!s) return undefined;
    return JSON.parse(s);
};

module.exports.getPartialPayloadAsJavaInputStream = function (id, payloadName, start, end) {
    return _cordraReturningStrings.getPartialPayloadAsInputStream(id, payloadName, start, end);
};

module.exports.getPartialPayloadAsJavaReader = function (id, payloadName, start, end) {
    return _cordraReturningStrings.getPartialPayloadAsReader(id, payloadName, start, end);
};

module.exports.getPartialPayloadAsUint8Array = function (id, payloadName, start, end) {
    var byteArray = _cordraReturningStrings.getPartialPayloadAsBytes(id, payloadName, start, end);
    if (!byteArray) return byteArray;
    var uint8Array = new Uint8Array(byteArray.length);
    uint8Array.set(Java.from(byteArray));
    return uint8Array;
};

module.exports.getPartialPayloadAsString = function (id, payloadName, start, end) {
    return _cordraReturningStrings.getPartialPayloadAsString(id, payloadName, start, end);
};

function CordraError(response, responseCode) {
    if (!(this instanceof CordraError)) {
        throw new TypeError("Class constructor CordraError cannot be invoked without 'new'");
    }
    if (typeof response === 'string' || typeof response === 'number' || typeof response === 'boolean') {
        response = { message: response };
    }
    var instance = new Error(messageForResponse(response));
    instance.responseCode = responseCode;
    instance.response = response;
    Object.defineProperty(instance, 'message', {
        get: function () {
            return messageForResponse(instance.response);
        },
        set: function (msg) {
            if (instance.response === undefined || instance.response === null) {
                instance.response = { message: msg };
            } else if (typeof instance.response === 'object' && !Array.isArray(instance.response)) {
                instance.response.message = msg;
            } else if (typeof instance.response === 'string' || typeof instance.response === 'number' || typeof instance.response === 'boolean') {
                instance.response = msg;
            }
        },
        configurable: true,
        enumerable: false
    });
    Object.setPrototypeOf(instance, Object.getPrototypeOf(this));
    if (Error.captureStackTrace) {
        Error.captureStackTrace(instance, CordraError);
    }
    return instance;
}
module.exports.CordraError = CordraError;

function messageForResponse(response) {
    if (response === undefined || response === null) return undefined;
    if (Array.isArray(response)) return undefined;
    if (typeof response === 'object') return response.message;
    return String(response);
}

CordraError.prototype = Object.create(Error.prototype, {
    constructor: {
        value: CordraError,
        enumerable: false,
        writable: true,
        configurable: true
    },
    name: {
        value: 'CordraError',
        enumerable: false,
        writable: true,
        configurable: true
    }
});
