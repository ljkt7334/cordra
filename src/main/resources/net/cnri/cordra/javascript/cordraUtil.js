module.exports.hashJson = function (jsElement, algorithm) {
    if (jsElement === undefined) return undefined;
    if (!algorithm) algorithm = "SHA-256";
    return _cordraUtil.hashJson(JSON.stringify(jsElement), algorithm);
};

module.exports.escapeForQuery = function (s) {
    return _cordraUtil.escapeForQuery(s);
};

module.exports.verifyHashes = function (coNashorn) {
    return JSON.parse(_cordraUtil.verifyHashes(JSON.stringify(coNashorn)));
};

module.exports.verifySecret = function (objectJsonOrString, jsonPointer, secret) {
    var objectString;
    if (typeof objectJsonOrString === 'string') {
        objectString = objectJsonOrString;
    } else {
        objectString = JSON.stringify(objectJsonOrString);
    }
    return JSON.parse(_cordraUtil.verifySecret(objectString, jsonPointer, secret));
};

module.exports.signWithKey = function (payload, key, useJsonSerialization) {
    if (typeof payload !== 'string') payload = JSON.stringify(payload);
    var jws = _cordraUtil.signWithKey(payload, JSON.stringify(key), !!useJsonSerialization);
    if (useJsonSerialization) {
        return JSON.parse(jws);
    } else {
        return jws;
    }
};

module.exports.signWithCordraKey = function (payload, useJsonSerialization) {
    if (typeof payload !== 'string') payload = JSON.stringify(payload);
    var jws = _cordraUtil.signWithCordraKey(payload, !!useJsonSerialization);
    if (useJsonSerialization) {
        return JSON.parse(jws);
    } else {
        return jws;
    }
};

module.exports.verifyWithCordraKey = function (jwt) {
    var isValid = _cordraUtil.verifyWithCordraKey(jwt);
    return isValid;
}

module.exports.verifyWithKey = function (jwt, publicKey) {
    var isValid = _cordraUtil.verifyWithKey(jwt, JSON.stringify(publicKey));
    return isValid;
}

module.exports.extractJwtPayload = function (jwt) {
    return JSON.parse(_cordraUtil.extractJwtPayload(jwt));
}

module.exports.getCordraPublicKey = function () {
    return JSON.parse(_cordraUtil.getCordraPublicKey());
};

module.exports.getDoipProcessorConfig = function () {
    return JSON.parse(_cordraUtil.getDoipProcessorConfig());
};

module.exports.getGroupsForUser = function (userId) {
    return JSON.parse(_cordraUtil.getGroupsForUser(userId));
};

module.exports.validateWithSchema = function (json, jsonSchema) {
    if (typeof json !== 'string') json = JSON.stringify(json);
    if (typeof jsonSchema !== 'string') jsonSchema = JSON.stringify(jsonSchema);
    return JSON.parse(_cordraUtil.validateWithSchema(json, jsonSchema));
}
