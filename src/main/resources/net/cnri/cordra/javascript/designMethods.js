module.exports.staticMethods = {};
module.exports.methods = {};

try {
    var globalExports = require('/cordra/design');
    if (globalExports && globalExports.staticMethods) {
        Object.assign(module.exports.staticMethods, globalExports.staticMethods);
    }
    if (globalExports && globalExports.methods) {
        Object.assign(module.exports.methods, globalExports.methods);
    }
} catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') throw e;
}

try {
    var cordraDesignExports = require('/cordra/schemas/CordraDesign');
    if (cordraDesignExports && cordraDesignExports.staticMethods) {
        Object.assign(module.exports.staticMethods, cordraDesignExports.staticMethods);
    }
    if (cordraDesignExports && cordraDesignExports.methods) {
        Object.assign(module.exports.methods, cordraDesignExports.methods);
    }
} catch (e) {
    if (e.code !== 'MODULE_NOT_FOUND') throw e;
}
