package net.cnri.cordra.model;

public class Range {
    private final Long start;
    private final Long end;

    public Range(Long start, Long end) {
        this.start = start;
        this.end = end;
    }

    public Long getStart() {
        return start;
    }

    public Long getEnd() {
        return end;
    }

    public boolean isPartial() {
        return start != null || end != null;
    }

    // Note: result must be either new Range(null,null) (i.e. not partial) or both start and end must be both non-null
    public Range withSize(long size) {
        if (!isPartial()) return this;
        Long newStart = start;
        Long newEnd = end;
        if (newEnd == null) {
            newEnd = size - 1;
        }
        if (newStart == null) {
            newStart = size - newEnd.longValue();
            newEnd = size - 1;
        }
        if (newStart < 0) {
            newStart = 0L;
        }
        if (newEnd > size - 1) {
            newEnd = size - 1;
        }
        if (newStart == 0 && newEnd == size - 1) return new Range(null, null);
        return new Range(newStart, newEnd);
    }

    public boolean isSatisfiable(long size) {
        if (start == null && end == null) return true;
        if (start == null) return end > 0;
        if (end == null) return start < size;
        if (start >= size) return false;
        if (end < start) return false;
        return true;
    }
}
