package net.cnri.cordra;

import net.cnri.cordra.model.Version;

import java.util.List;

public class InitDataResponse {
    public Version version;
    public DesignPlusSchemas design;
    public boolean isActiveSession = false;
    public String username;
    public String userId;
    public List<String> typesPermittedToCreate;
}
