package net.cnri.cordra;

public class InternalCordraClientFactory {

    private static InternalCordraClient internalCordraClient;

    public static synchronized InternalCordraClient get() {
        if (internalCordraClient == null) {
            internalCordraClient = new InternalCordraClient(CordraServiceFactory.getCordraService());
        }
        return internalCordraClient;
    }

    public static synchronized void reset() {
        internalCordraClient = null;
    }
}
