package net.cnri.cordra.indexer;

import com.google.gson.JsonObject;

public class IndexerConfig {
    public String module = "lucene"; // solr | lucene | elasticsearch | memory
    public JsonObject options = new JsonObject();
//    baseUri; //optional used by solr single instance
//    zkHosts; //optional used by solr cluster e.g."zkServerA:2181,zkServerB:2181,zkServerC:2181"
//    isStoreFields = true;

    public static IndexerConfig getNewDefaultInstance() {
        IndexerConfig indexerConfig = new IndexerConfig();
        indexerConfig.module = "lucene";
        indexerConfig.options.addProperty("isStoreFields", "true");
        return indexerConfig;
    }
}
