package net.cnri.cordra;

import com.google.gson.JsonObject;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.indexer.CordraIndexer;
import net.cnri.cordra.model.ObjectDelta;
import net.cnri.cordra.model.SearchRequest;
import net.cnri.cordra.storage.CordraStorage;

import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VersionManager {

    private final CordraStorage storage;
    private final CordraIndexer indexer;
    private final CordraService cordra;

    public static final String PUBLISHED_BY = "publishedBy";
    public static final String PUBLISHED_ON = "publishedOn";
    public static final String VERSION_OF = "versionOf";
    public static final String IS_VERSION = "isVersion";

    public VersionManager(CordraStorage storage, CordraIndexer indexer, CordraService cordra) {
        this.storage = storage;
        this.indexer = indexer;
        this.cordra = cordra;
    }

    public void validateExistingObject(String existingObjectId, CordraObject existingObject) throws CordraException, VersionException {
        if (existingObject == null) {
            throw new VersionException("Cannot publish a version for an object that does not exist: " + existingObjectId);
        }
        if (!isTipObject(existingObject)) {
            throw new VersionException("Cannot publish a version for an object that is not the tip.");
        }
        if (CordraService.DESIGN_OBJECT_ID.equals(existingObjectId)) {
            throw new InternalErrorCordraException("Object not valid for versioning: " + existingObjectId);
        }
    }

    public ObjectDelta buildObjectDeltaForVersion(String versionId, String existingObjectId, CordraObject existingObject, boolean clonePayloads) throws CordraException {
        String type = existingObject.type;
        String jsonData = existingObject.getContentAsString();
        CordraObject.AccessControlList acl = existingObject.acl;
        JsonObject userMetadata = existingObject.userMetadata;
        List<Payload> payloads = existingObject.payloads;
        if (clonePayloads) {
            setInputStreamsForPayloads(existingObjectId, payloads);
        } else {
            payloads = Collections.emptyList();
        }
        return new ObjectDelta(versionId, type, jsonData, acl, userMetadata, payloads, null);
    }

    public void setProtoObjectMetadataForVersion(CordraObject protoVersion, String existingObjectId, CordraObject existingObject, String userId) {
        protoVersion.metadata.createdOn = existingObject.metadata.createdOn;
        protoVersion.metadata.modifiedOn = existingObject.metadata.modifiedOn;
        protoVersion.metadata.createdBy = existingObject.metadata.createdBy;
        protoVersion.metadata.modifiedBy = existingObject.metadata.modifiedBy;
        protoVersion.metadata.publishedBy = userId;
        protoVersion.metadata.versionOf = existingObjectId;
        protoVersion.metadata.publishedOn = System.currentTimeMillis();
        protoVersion.metadata.isVersion = true;
        protoVersion.metadata.internalMetadata = copyInternalMetadataForVersion(existingObject.metadata.internalMetadata);
    }

    private static String[] internalMetadataWhiteList = {"hash", "salt", "iterations", "algorithm", "secureProperties", "payloadIndexState", "payloadIndexCordraServiceId"};

    private JsonObject copyInternalMetadataForVersion(JsonObject existingInternalMetadata) {
        if (existingInternalMetadata == null) {
            return null;
        }
        JsonObject result = new JsonObject();
        for (String propName : internalMetadataWhiteList) {
            if (existingInternalMetadata.has(propName)) {
                result.add(propName, existingInternalMetadata.get(propName));
            }
        }
        return result;
    }

    private void setInputStreamsForPayloads(String originalId, List<Payload> srcElements) throws CordraException {
        if (srcElements == null) return;
        for (Payload srcElement : srcElements) {
            srcElement.setInputStream(storage.getPayload(originalId, srcElement.name));
        }
    }

    public boolean isTipObject(CordraObject co) {
        if (co.metadata == null) return true;
        String tipId = co.metadata.versionOf;
        if (tipId == null) {
            return true;
        } else {
            return false;
        }
    }

    // co is guaranteed to be a tip object
    public List<CordraObject> getVersionsFor(CordraObject co, AuthenticationResult authResult) throws CordraException, InterruptedException, ScriptException {
        List<CordraObject> result = new ArrayList<>();
        String query = VERSION_OF+":\""+co.id + "\" objatt_" + VERSION_OF +":\""+co.id + "\"";
        boolean excludeVersions = false;
        cordra.ensureIndexUpToDate();
        SearchRequest queryAndParams = cordra.customizeAndRestrictQuery(query, QueryParams.DEFAULT, false, authResult, excludeVersions);
        try (SearchResults<CordraObject> results = indexer.search(queryAndParams.query)) {
            for (CordraObject current : results) {
                result.add(current);
            }
        }
        result.add(co); //Should we include the tip in the response?
        return result;
    }
}
