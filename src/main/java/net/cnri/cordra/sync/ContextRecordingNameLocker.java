package net.cnri.cordra.sync;

import net.cnri.cordra.api.CordraException;

import java.util.List;
import org.slf4j.MDC;

public class ContextRecordingNameLocker implements NameLocker {

    private static final String INSIDE_OBJECT_LOCK = "insideObjectLock";

    private final NameLocker delegate;

    public ContextRecordingNameLocker(NameLocker delegate) {
        this.delegate = delegate;
    }

    @Override
    public void lock(String name) throws CordraException {
        delegate.lock(name);
        MDC.put(INSIDE_OBJECT_LOCK, "true");
    }

    @Override
    public void release(String name) {
        delegate.release(name);
        MDC.remove(INSIDE_OBJECT_LOCK);
    }

    @Override
    public boolean isLocked(String name) {
        return delegate.isLocked(name);
    }

    @Override
    public void lockInOrder(List<String> names) throws CordraException {
        delegate.lockInOrder(names);
        MDC.put(INSIDE_OBJECT_LOCK, "true");
    }

    @Override
    public void releaseAll(List<String> names) {
        delegate.releaseAll(names);
        MDC.remove(INSIDE_OBJECT_LOCK);
    }
}
