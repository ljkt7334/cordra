package net.cnri.cordra.web.admin;

import com.google.gson.Gson;
import net.cnri.cordra.*;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.web.ServletErrorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;

@WebServlet("/updateHandles/*")
public class UpdateAllHandlesServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(UpdateAllHandlesServlet.class);

    private InternalCordraClient internalCordra;
    private Gson gson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            internalCordra.updateAllHandleRecords(options);
            Writer w = resp.getWriter();
            w.write("{}");
            w.flush();
            w.close();
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error updateAllHandleRecords", e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            AllHandlesUpdater.UpdateStatus status = internalCordra.getHandleUpdateStatus(options);
            String json = gson.toJson(status);
            Writer w = resp.getWriter();
            w.write(json);
            w.flush();
            w.close();
        } catch (CordraException e) {
            ServletErrorUtil.internalServerError(resp);
            logger.error("Error getHandleUpdateStatus", e);
        }
    }
}
