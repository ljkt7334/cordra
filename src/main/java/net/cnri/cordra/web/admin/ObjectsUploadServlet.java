package net.cnri.cordra.web.admin;

import com.google.gson.*;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.PreAuthenticatedOptions;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@WebServlet({ "/uploadObjects", "/uploadObjects/ "})
public class ObjectsUploadServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(ObjectsUploadServlet.class);

    private InternalCordraClient internalCordra;
    private static Gson gson = GsonUtility.getGson();

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            InternalRequestOptions options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            AuthenticationResult authResult = internalCordra.internalAuthenticate(options);
            options = buildPreAuthenticatedOptions(authResult);
            // get input before parameters in case someone uses the wrong Content-Type and servlet wants to process the body
            String json;
            try (InputStream inputStream = req.getInputStream()) {
                boolean deleteCurrentObjects = ServletUtil.getBooleanParameter(req, "deleteCurrentObjects");
                if (deleteCurrentObjects) {
                    doDeleteCurrentObjects(options);
                    //cordra.ensureIndexUpToDate();
                }
                json = ServletUtil.streamToString(inputStream, req.getCharacterEncoding());
            }
            List<JsonObject> input = parseInput(json);
            sortSchemasFirst(input);
            boolean schemasUploadFinished = false;
            for (JsonObject obj : input) {
                String type = obj.get("type").getAsString();
                String id = obj.get("id").getAsString();
                List<Payload> payloads = new ArrayList<>();
                CordraObject.AccessControlList acl = null;
                if (obj.has("acl")) {
                    JsonElement aclEl = obj.get("acl");
                    acl = gson.fromJson(aclEl, CordraObject.AccessControlList.class);
                }
                JsonObject userMetadata = null;
                if (obj.has("userMetadata")) {
                    userMetadata = obj.get("userMetadata").getAsJsonObject();
                }
                JsonElement content = obj.get("content");
                //String jsonData = content.toString();
                CordraObject co = new CordraObject();
                co.id = id;
                co.type = type;
                co.content = content;
                co.acl = acl;
                co.userMetadata = userMetadata;
                co.payloads = payloads;
                if ("CordraDesign".equals(type)) {
                    internalCordra.update(co, options);
                } else {
                    if (!schemasUploadFinished && !"Schema".equals(type)) {
                        // Formerly:
                        // We've finished uploading the schemas. Make sure index is up to date before continuing.
                        // This may not be necessary. Saw problems without, but could not duplicate.
                        //cordra.ensureIndexUpToDate();
                        schemasUploadFinished = true;
                    }
                    internalCordra.create(co, options);
                }
            }
            resp.getWriter().println("{\"message\": \"success\"}");
        } catch (InternalErrorCordraException e) {
            logger.error("Error loading objects", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Error loading objects", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private PreAuthenticatedOptions buildPreAuthenticatedOptions(AuthenticationResult authResult) {
        PreAuthenticatedOptions options = new PreAuthenticatedOptions();
        options.username = authResult.username;
        options.userId = authResult.userId;
        options.grantAuthenticatedAccess = authResult.grantAuthenticatedAccess;
        // this is setting up the PreAuthenticatedOptions so that it will use the groupIds as-is each time
        options.hookSpecifiedGroupIds = authResult.groupIds;
        options.bypassCordraGroupObjects = true;
        options.exp = authResult.exp;
        return options;
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    private void sortSchemasFirst(List<JsonObject> input) {
        input.sort(new Comparator<JsonObject>() {
            @Override
            public int compare(JsonObject a, JsonObject b) {
                String aType = a.get("type").getAsString();
                String bType = b.get("type").getAsString();
                if ("CordraDesign".equals(aType)) {
                    return -1;
                }
                if ("CordraDesign".equals(bType)) {
                    return 1;
                }
                if ("Schema".equals(aType) && !"Schema".equals(bType)) {
                    return -1;
                }
                if ("Schema".equals(bType) && !"Schema".equals(aType)) {
                    return 1;
                }
                return 0;
            }
        });
    }

    private void doDeleteCurrentObjects(InternalRequestOptions options) throws CordraException {
        boolean origExcludeVersions = options.excludeVersions;
        options.excludeVersions = false;
        deleteByQuery("*:* -type:Schema", options);
        deleteByQuery("*:*", options);
        options.excludeVersions = origExcludeVersions;
        //cordra.ensureIndexUpToDate();
    }

    private void deleteByQuery(String query, InternalRequestOptions options) throws CordraException {
        List<String> ids;
        //cordra.ensureIndexUpToDate();
        try (SearchResults<String> results = internalCordra.searchHandles(query, options)) {
            ids = resultsToList(results);
        }
        deleteAll(ids, options);
    }

    private static List<String> resultsToList(SearchResults<String> results) {
        List<String> ids = new ArrayList<>();
        for (String id : results) {
            ids.add(id);
        }
        return ids;
    }

    private void deleteAll(List<String> ids, Options options) throws CordraException {
        for (String id : ids) {
            if ("design".equals(id)) {
                continue;
            }
            try {
                internalCordra.delete(id, options);
            } catch (NotFoundCordraException e) {
                // no-op
            }
        }
    }

    private static List<JsonObject> parseInput(String json) {
        try {
            JsonArray results = JsonParser.parseString(json).getAsJsonObject().get("results").getAsJsonArray();
            List<JsonObject> result = new ArrayList<>();
            for (JsonElement obj : results) {
                result.add(obj.getAsJsonObject());
            }
            return result;
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

}
