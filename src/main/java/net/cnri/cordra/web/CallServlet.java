package net.cnri.cordra.web;

import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CallHeaders;
import net.cnri.cordra.api.CallResponseHandler;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.auth.InternalRequestOptions;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.util.HttpUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@WebServlet("/call/*")
public class CallServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(CallServlet.class);

    private InternalCordraClient internalCordra;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGetOrPost(req, resp, true);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGetOrPost(req, resp, false);
    }

    private void doGetOrPost(HttpServletRequest reqParam, HttpServletResponse resp, boolean isGet) throws IOException {
        try {
            EmptyCheckingHttpServletRequestWrapper req = EmptyCheckingHttpServletRequestWrapper.wrap(reqParam);
            String objectId = req.getParameter("objectId");
            String type = req.getParameter("type");
            if (type == null && objectId == null) throw new BadRequestCordraException("objectId or type required");
            String method = req.getParameter("method");
            if (method== null) throw new BadRequestCordraException("method required");
            InternalRequestOptions options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            options.isGet = isGet;
            String params = null;
            String mediaType = null;
            String filename = null;
            if (isGet || req.isEmpty()) {
                params = req.getParameter("params");
                if (params == null) params = "";
            }
            if (params == null || params.isEmpty()) {
                mediaType = req.getContentType();
                filename = HttpUtil.getContentDispositionFilename(req.getHeader("Content-Disposition"));
            }
            options.setCallHeaders(new CallHeaders(mediaType, filename));
            CallResponseHandler handler = new CallResponseHandlerForCallServlet(req, resp);
            InputStream in;
            if (params == null) {
                in = req.getInputStream();
            } else {
                in = new ByteArrayInputStream(params.getBytes(StandardCharsets.UTF_8));
            }
            if (type != null) {
                internalCordra.callForType(type, method, in, handler, options);
            } else {
                internalCordra.call(objectId, method, in, handler, options);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected error calling method", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Unexpected error calling method", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private static class CallResponseHandlerForCallServlet implements CallResponseHandler {

        private final HttpServletRequest req;
        private final HttpServletResponse resp;
        private boolean hasSetCharacterEncoding;

        public CallResponseHandlerForCallServlet(HttpServletRequest req, HttpServletResponse resp) {
            this.req = req;
            this.resp = resp;
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return resp.getOutputStream();
        }

        @Override
        public Writer getWriter() throws IOException {
            if (!hasSetCharacterEncoding) {
                resp.setCharacterEncoding("UTF-8");
                hasSetCharacterEncoding = true;
            }
            return resp.getWriter();
        }

        @Override
        public void setMediaType(String mediaType) {
            if (mediaType.toLowerCase(Locale.ROOT).contains("charset")) {
                hasSetCharacterEncoding = true;
            }
            resp.setContentType(mediaType);
        }

        @Override
        public void setFilename(String filename) {
            String disposition = req.getParameter("disposition");
            if (disposition == null) disposition = "inline";
            resp.setHeader("Content-Disposition", HttpUtil.contentDispositionHeaderFor(disposition, filename));
        }

        @Override
        public void setLength(long length) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void setRange(boolean partial, boolean unsatisfied, Long start, Long end, Long size) {
            throw new UnsupportedOperationException();
        }
    }
}
