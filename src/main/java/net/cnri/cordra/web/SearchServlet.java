package net.cnri.cordra.web;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.model.SearchRequest;
import net.cnri.cordra.util.SearchJsonObject;
import org.elasticsearch.ElasticsearchStatusException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@WebServlet("/search/*")
public class SearchServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(SearchServlet.class);

    private InternalCordraClient internalCordra;
    private Gson gson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    private SearchRequest readSearchRequest(HttpServletRequest req) throws NumberFormatException {
        String query = req.getParameter("query");
        String queryJsonString = req.getParameter("queryJson");
        String pageNumString = req.getParameter("pageNum");
        if (pageNumString == null) pageNumString = "0";
        String pageSizeString = req.getParameter("pageSize");
        if (pageSizeString == null) pageSizeString = "-1";
        String sortFieldsString = req.getParameter("sortFields");
        boolean ids = ServletUtil.getBooleanParameter(req, "ids");
        int pageNum = Integer.parseInt(pageNumString);
        int pageSize = Integer.parseInt(pageSizeString);
        if (pageNum < 0) {
            pageNum = 0;
        }
        boolean isFull = ServletUtil.getBooleanParameter(req, "full", true);
        String filterJson = req.getParameter("filter");
        String facetsJson = req.getParameter("facets");
        String filterQueriesJson = req.getParameter("filterQueries");
        SearchRequest result = new SearchRequest();
        result.query = query;
        if (queryJsonString != null) {
            result.queryJson = JsonParser.parseString(queryJsonString);
        }
        result.pageNum = pageNum;
        result.pageSize = pageSize;
        result.ids = ids;
        result.sortFields = SortField.getSortFieldsFromString(sortFieldsString);
        result.full = isFull;
        if (filterJson != null) {
            result.filter = gson.fromJson(filterJson, new TypeToken<List<String>>(){}.getType());
        }
        if (facetsJson != null) {
            result.facets = gson.fromJson(facetsJson, new TypeToken<List<FacetSpecification>>(){}.getType());
        }
        if (filterQueriesJson != null) {
            result.filterQueries = gson.fromJson(filterQueriesJson, new TypeToken<List<String>>(){}.getType());
        }
        return result;
    }

    private SearchRequest readSearchRequestFromJson(HttpServletRequest req) throws IOException {
        JsonElement jsonElement = JsonParser.parseReader(req.getReader());
        if (jsonElement == null || !jsonElement.isJsonObject()) return null;
        JsonObject jsonObject = jsonElement.getAsJsonObject();
        if (jsonObject.has("sortFields") && !jsonObject.get("sortFields").isJsonArray()) {
            String sortFieldsString = jsonObject.get("sortFields").getAsString();
            List<SortField> sortFields = SortField.getSortFieldsFromString(sortFieldsString);
            jsonObject.add("sortFields", gson.toJsonTree(sortFields));
        }
        SearchRequest searchRequest = gson.fromJson(jsonObject, SearchRequest.class);
        return searchRequest;
    }

    private void doSearch(HttpServletRequest req, HttpServletResponse resp, SearchRequest searchRequest) throws IOException {
        if (searchRequest == null || searchRequest.query == null && searchRequest.queryJson == null) {
            ServletErrorUtil.badRequest(resp, "Missing query in search request");
            return;
        }
        if (searchRequest.query != null && searchRequest.queryJson != null) {
            ServletErrorUtil.badRequest(resp, "Only one of 'query' or 'queryJson' may be used");
            return;
        }
        try {
//            cordra.ensureIndexUpToDate();
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            String query = null;
            if (searchRequest.queryJson != null) {
                query = SearchJsonObject.queryFromJsonElement(searchRequest.queryJson);
            } else {
                query = searchRequest.query;
            }
//            boolean isPostProcess = true;
            List<String> filter = searchRequest.filter;
            if (filter != null && !searchRequest.full) {
                filter = filter.stream()
                        .map(pointer -> "/content" + pointer)
                        .collect(Collectors.toList());
            }
            QueryParams params = new QueryParams(searchRequest.pageNum, searchRequest.pageSize, searchRequest.sortFields, searchRequest.facets, searchRequest.filterQueries, filter);
            if (searchRequest.ids) {
                try (SearchResults<String> results = internalCordra.searchHandles(query, params, options)) {
                    CordraSearcher.writeResults(results, resp.getWriter(), params);
                }
            } else {
                try (SearchResults<CordraObject> results = internalCordra.search(query, params, options)) {
                    CordraSearcher.writeResults(results, resp.getWriter(), params, searchRequest.full);
                } catch (UncheckedCordraException e) {
                    throw e.getCause();
                }
            }
        } catch (InternalErrorCordraException e) {
            if (looksLikeParseFailure(e)) {
                ServletErrorUtil.badRequest(resp, "Query parse failure");
            } else {
                logger.error("Error in doSearch", e);
                ServletErrorUtil.internalServerError(resp);
            }
        } catch (CordraException e) {
            if (looksLikeParseFailure(e)) {
                ServletErrorUtil.badRequest(resp, "Query parse failure");
            } else {
                ServletErrorUtil.writeCordraException(resp, e);
            }
        } catch (Exception e) {
            logger.error("Error in doSearch", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    public static boolean looksLikeParseFailure(CordraException e) {
        if (e.getCause() instanceof ElasticsearchStatusException) {
            for (Throwable t : e.getCause().getSuppressed()) {
                if (t instanceof org.elasticsearch.client.ResponseException) {
                    if (t.getMessage() != null && t.getMessage().contains("Failed to parse query")) {
                        return true;
                    }
                }
            }
        }
        return e.getMessage() != null && (e.getMessage().contains("Parse failure")
                || e.getMessage().contains("Cannot parse")
                || e.getMessage().contains("org.apache.lucene.queryparser.classic.ParseException")
                || e.getMessage().contains("org.apache.solr.search.SyntaxError"));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        if (ServletUtil.isForm(req)) {
            doGet(req, resp);
        } else {
            SearchRequest searchRequest = readSearchRequestFromJson(req);
            if (searchRequest == null) searchRequest = readSearchRequest(req);
            doSearch(req, resp, searchRequest);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        try {
            SearchRequest searchRequest = readSearchRequest(req);
            doSearch(req, resp, searchRequest);
        } catch (NumberFormatException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
            return;
        }
    }
}
