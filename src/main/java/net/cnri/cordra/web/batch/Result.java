package net.cnri.cordra.web.batch;

import com.google.gson.JsonElement;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.CordraObject;

public class Result {
    public long position;
    int responseCode;
    JsonElement response;

    public Result(long position, int responseCode, JsonElement response) {
        this.position = position;
        this.responseCode = responseCode;
        this.response = response;
    }

    public static Result from(CordraObject co, long position) {
        JsonElement coJson = GsonUtility.getGson().toJsonTree(co);
        return new Result(position, 200, coJson);
    }

    public static Result from(CordraException e, long position) {
        return new Result(position, e.getResponseCode(), e.getResponse());
    }
}
