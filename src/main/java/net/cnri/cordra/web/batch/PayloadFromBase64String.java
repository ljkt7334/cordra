package net.cnri.cordra.web.batch;

import net.cnri.cordra.api.Payload;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;

public class PayloadFromBase64String extends Payload {

    private String base64Payload;

    public void setBase64Payload(String base64Payload) {
        this.base64Payload = base64Payload;
    }

    public String getBase64Payload() {
        return this.base64Payload;
    }

    public void setBase64String(InputStream in) throws IOException {
        byte[] bytes = IOUtils.toByteArray(in);
        base64Payload = Base64.getEncoder().encodeToString(bytes);
    }

    @Override
    public InputStream getInputStream() {
        return new ByteArrayInputStream(Base64.getDecoder().decode(base64Payload.replace('-', '+').replace('_', '/')));
    }
}
