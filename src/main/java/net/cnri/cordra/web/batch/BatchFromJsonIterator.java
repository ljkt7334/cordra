package net.cnri.cordra.web.batch;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.BadRequestCordraException;
import net.cnri.cordra.api.CordraObject;
import net.cnri.cordra.api.InternalErrorCordraException;
import net.cnri.cordra.api.UncheckedCordraException;

import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class BatchFromJsonIterator implements Iterator<CordraObject> {

    private final JsonReader reader;
    private Gson gson;

    public BatchFromJsonIterator(Reader readerParam) throws IOException, BadRequestCordraException {
        this.gson = GsonUtility.getGson();
        this.reader = new JsonReader(readerParam);
        JsonToken nextToken = reader.peek();
        if (nextToken.equals(JsonToken.BEGIN_OBJECT)) {
            reader.beginObject();
            consumeReaderUntilStartOfResults(reader);
        } else if (!nextToken.equals(JsonToken.BEGIN_ARRAY)) {
            throw new BadRequestCordraException("Invalid JSON input");
        }
        reader.beginArray();
    }

    @Override
    public boolean hasNext() {
        try {
            return reader.hasNext();
        } catch (Exception e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e));
        }
    }

    @Override
    public CordraObject next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        try {
            JsonObject obj = gson.fromJson(reader, JsonObject.class);
            CordraObject co = BatchUpload.fromJsonWithPayloadsAsStrings(obj);
            return co;
        } catch (JsonSyntaxException e) {
            throw new UncheckedCordraException(new BadRequestCordraException("Unable to parse object", e));
        } catch (Exception e) {
            throw new UncheckedCordraException(new InternalErrorCordraException(e.getMessage(), e));
        }
    }

    private static void consumeReaderUntilStartOfResults(JsonReader reader) throws IOException, BadRequestCordraException {
        while (reader.hasNext()) {
            String name = reader.nextName();
            if ("results".equals(name)) {
                return;
            } else {
                reader.skipValue();
            }
        }
        throw new BadRequestCordraException("JSON input does not contain 'results'");
    }
}