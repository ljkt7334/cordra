package net.cnri.cordra.web.doip;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import net.dona.doip.OutDoipMessage;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class OutDoipMessageHttpResponse implements OutDoipMessage {

    private static Logger logger = LoggerFactory.getLogger(OutDoipMessageHttpResponse.class);

    private final HttpServletResponse resp;
    private boolean isClosed = false;
    private Closeable openCloseable;

    public OutDoipMessageHttpResponse(HttpServletResponse resp) {
        this.resp = resp;
    }

    public void setStatus(int status) {
        resp.setStatus(status);
    }

    public void setContentType(String contentType) {
        resp.setContentType(contentType);
    }

    public void setContentDisposition(String contentDisposition) {
        resp.setHeader("Content-Disposition", contentDisposition);
    }

    public void setDoipResponseHeader(JsonObject doipResponseHeader) {
        String headerString = doipResponseHeader.toString();
        headerString = processJsonForHttpHeader(headerString);
        resp.setHeader("Doip-Response", headerString);
    }

    public static String processJsonForHttpHeader(String json) {
        StringBuilder result = null;
        for (int i = json.length() - 1; i >= 0; i--) {
            char ch = json.charAt(i);
            if (ch == '\n' || ch == '\r' || ch >= 0x7F) {
                if (result == null) result = new StringBuilder(json);
                if (ch == '\n' || ch == '\r') result.replace(i, i + 1, " ");
                else result.replace(i, i + 1, jsonEncodeAsUnicodeEscape(ch));
            }
        }
        if (result == null) return json;
        return result.toString();
    }

    private static String jsonEncodeAsUnicodeEscape(char ch) {
        String s = Integer.toHexString(ch);
        if (s.length() == 1) return "\\u000" + s;
        if (s.length() == 2) return "\\u00" + s;
        if (s.length() == 3) return "\\u0" + s;
        return "\\u" + s;
    }

    @Override
    public void writeJson(JsonElement json) throws IOException {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        writeJson(json.toString());
    }

    @Override
    public void writeJson(String json) throws IOException {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(json);
        isClosed = true;
    }

    @Override
    public void writeJson(byte[] json) throws IOException {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getOutputStream().write(json);
        isClosed = true;
    }

    @Override
    public Writer getJsonWriter() {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        if (openCloseable != null) throw new IllegalStateException("already opened segment output stream or writer");
        try {
            Writer writer = new JsonSegmentWriter(resp.getOutputStream());
            openCloseable = writer;
            return new BufferedWriter(writer);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        return null;
    }

    @Override
    public void writeBytes(byte[] bytes) throws IOException {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        if (resp.getContentType() == null) {
            resp.setContentType("application/octet-stream");
        }
        resp.getOutputStream().write(bytes);
        isClosed = true;
    }

    @Override
    public void writeBytes(InputStream in) throws IOException {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        if (resp.getContentType() == null) {
            resp.setContentType("application/octet-stream");
        }
        IOUtils.copy(in, resp.getOutputStream());
        isClosed = true;
    }

    @Override
    public OutputStream getBytesOutputStream() throws IOException {
        if (isClosed) throw new IllegalStateException("Only a single segment can be written");
        if (openCloseable != null) throw new IllegalStateException("already opened segment output stream or writer");
        if (resp.getContentType() == null) {
            resp.setContentType("application/octet-stream");
        }
        OutputStream output = new BytesSegmentOutputStream(resp.getOutputStream());
        openCloseable = output;
        return new BufferedOutputStream(output);
    }

    public void closeSegmentOutput() throws IOException {
        if (openCloseable != null) {
            openCloseable.close();
        }
    }

    @Override
    public void close() throws IOException {
        if (isClosed) return;
        if (openCloseable != null) {
            openCloseable.close();
        }
        isClosed = true;
    }

    private class JsonSegmentWriter extends OutputStreamWriter {
        private JsonSegmentWriter(OutputStream out) {
            super(out);
        }

        @Override
        public void write(int c) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.write(c);
        }

        @Override
        public void write(char[] cbuf, int off, int len) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.write(cbuf, off, len);
        }

        @Override
        public void write(String str, int off, int len) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.write(str, off, len);
        }

        @Override
        public void flush() throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.flush();
        }

        @Override
        public void write(char[] cbuf) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.write(cbuf);
        }

        @Override
        public void write(String str) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.write(str);
        }

        @Override
        public Writer append(CharSequence csq) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            return super.append(csq);
        }

        @Override
        public Writer append(CharSequence csq, int start, int end) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            return super.append(csq, start, end);
        }

        @Override
        public Writer append(char c) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            return super.append(c);
        }

        @Override
        public void close() throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            flush();
        }
    }

    private class BytesSegmentOutputStream extends OutputStream {

        private final OutputStream out;

        public BytesSegmentOutputStream(OutputStream out) {
            this.out = out;
        }

        @Override
        public void write(int b) throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            out.write(b);
        }

        @Override
        public void write(byte[] b) throws IOException {
            if (b == null) throw new NullPointerException();
            if (isClosed) throw new IllegalStateException("closed");
            if (b.length == 0) return;
            out.write(b);
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            if (b == null) throw new NullPointerException();
            if (isClosed) throw new IllegalStateException("closed");
            if (len == 0) return;
            out.write(b, off, len);
        }

        @Override
        public void flush() throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            super.flush();
        }

        @Override
        public void close() throws IOException {
            if (isClosed) throw new IllegalStateException("closed");
            flush();
            out.flush();
            super.close();
            openCloseable = null;
        }
    }
}
