package net.cnri.cordra.web.doip;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import net.cnri.cordra.auth.Credentials;
import net.cnri.cordra.util.HttpUtil;
import net.cnri.cordra.web.EmptyCheckingHttpServletRequestWrapper;
import net.dona.doip.BadDoipException;
import net.dona.doip.InDoipMessage;
import net.dona.doip.InDoipSegment;
import net.dona.doip.InDoipSegmentFromInputStream;
import net.dona.doip.InDoipSegmentFromJson;

public class InDoipMessageFromHttpRequest implements InDoipMessage {

    private final EmptyCheckingHttpServletRequestWrapper req;
    private InDoipSegment curr;
    private volatile boolean isClosed;
    private BadDoipException terminalException;
    private CompletableFuture<?> completer;
    private SpliteratorImpl spliterator;

    public InDoipMessageFromHttpRequest(EmptyCheckingHttpServletRequestWrapper req) {
        this.req = req;
        this.spliterator = new SpliteratorImpl();
    }

    public static void augmentRequestFromHttpRequestIfNecessary(JsonObject doipRequestJson, HttpServletRequest req) {
        addAuthenticationFromAuthHeaderIfNecessary(doipRequestJson, req);
        augmentRequestFromQueryParams(doipRequestJson, req);
        String mediaType = req.getContentType();
        String filename = HttpUtil.getContentDispositionFilename(req.getHeader("Content-Disposition"));
        if (mediaType != null || filename != null) {
            JsonObject attributes;
            if (doipRequestJson.has("attributes")) {
                attributes = doipRequestJson.get("attributes").getAsJsonObject();
            } else {
                attributes = new JsonObject();
                doipRequestJson.add("attributes", attributes);
            }
            if (filename != null) {
                attributes.addProperty("filename", filename);
            }
            if (mediaType != null) {
                attributes.addProperty("mediaType", mediaType);
            }
        }
    }

    private static JsonElement translateAuthHeader(String authHeader) {
        if (isBasicAuth(authHeader)) {
            JsonObject authentication = new JsonObject();
            authHeader = authHeader.trim();
            Credentials c = new Credentials(authHeader);
            authentication.addProperty("username", c.getUsername());
            authentication.addProperty("password", c.getPassword());
            return authentication;
        } else if (isBearerToken(authHeader)) {
            JsonObject authentication = new JsonObject();
            String token = getTokenFromAuthHeader(authHeader);
            authentication.addProperty("token", token);
            return authentication;
        } else {
            if (authHeader == null) return null;
            String[] parts = authHeader.trim().split(" +");
            if (parts.length != 2) return null;
            if (!"Doip".equalsIgnoreCase(parts[0])) return null;
            try {
                String json = new String(Base64.getDecoder().decode(parts[1]), StandardCharsets.UTF_8);
                return JsonParser.parseString(json);
            } catch (Exception e) {
                // ignore
                return null;
            }
        }
    }

    private static String getTokenFromAuthHeader(String authHeader) {
        return authHeader.substring(authHeader.indexOf(" ") + 1);
    }

    private static boolean isBearerToken(String authHeader) {
        if (authHeader == null) return false;
        String[] parts = authHeader.trim().split(" +");
        if (parts.length != 2) return false;
        if (!"Bearer".equalsIgnoreCase(parts[0])) return false;
//        if (parts[1].contains(".")) return false;
        return true;
    }

    private static boolean isBasicAuth(String authHeader) {
        return authHeader.length() > 5 && authHeader.substring(0, 6).equalsIgnoreCase("Basic ");
    }

    private static void addAuthenticationFromAuthHeaderIfNecessary(JsonObject doipRequestJson, HttpServletRequest req) {
        String authHeader = req.getHeader("Authorization");
        if (authHeader != null) {
            if (!doipRequestJson.has("authentication")) {
                JsonElement authentication = translateAuthHeader(authHeader);
                if (authentication != null) {
                    doipRequestJson.add("authentication", authentication);
                }
            }
        }
    }

    private static void augmentRequestFromQueryParams(JsonObject doipRequestJson, HttpServletRequest req) {
        Map<String, String[]> params = req.getParameterMap();
        if (hasDoipAttributeQueryParams(params)) {
            JsonObject attributes;
            if (doipRequestJson.has("attributes")) {
                attributes = doipRequestJson.get("attributes").getAsJsonObject();
            } else {
                attributes = new JsonObject();
            }
            DoipRequestUtil.buildAttributesFromQueryParams(params, attributes);
            doipRequestJson.add("attributes", attributes);
        }
        setPropertyFromParams("targetId", params, doipRequestJson);
        setPropertyFromParams("operationId", params, doipRequestJson);
        setPropertyFromParams("clientId", params, doipRequestJson);
    }

    private static void setPropertyFromParams(String name, Map<String, String[]> params, JsonObject doipRequestJson) {
        String paramValue = null;
        if (params.containsKey(name)) {
            paramValue = params.get(name)[0];
        }
        doipRequestJson.addProperty(name, paramValue);
    }

    private static boolean hasDoipAttributeQueryParams(Map<String, String[]> params ) {
        for (Map.Entry<String, String[]> param : params.entrySet()) {
            String paramName = param.getKey();
            if ("attributes".equals(paramName)) {
                return true;
            } else if (paramName.startsWith("attributes.")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Stream<InDoipSegment> stream() {
        Stream<InDoipSegment> stream = StreamSupport.stream(spliterator, false);
        return stream.onClose(this::close);
    }

    @Override
    public void close() {
        if (terminalException != null) return;
        while (!isClosed) spliterator.tryAdvance(x -> {});
    }

    @Override
    public Iterator<InDoipSegment> iterator() {
        return Spliterators.iterator(spliterator);
    }

    public BadDoipException getTerminalException() {
        return terminalException;
    }

    private BadDoipException terminalException(String message) {
        terminalException = new BadDoipException(message);
        if (completer != null) completer.completeExceptionally(terminalException);
        return terminalException;
    }

    public void setCompleter(CompletableFuture<?> completer) {
        this.completer = completer;
    }

    private boolean isJson(String mediaType) {
        if (mediaType == null) return false;
        String type = mediaType.toLowerCase(Locale.ROOT);
        if ("application/json".equals(type)) return true;
        if (type.endsWith("+json")) return true;
        if (type.contains("+json ") || type.contains("+json\t") || type.contains("+json;")) return true;
        if (!type.startsWith("application/json")) return false;
        char nextChar = type.charAt("application/json".length());
        if (nextChar == ' ' || nextChar == '\t' || nextChar ==';') return true;
        return false;
    }

    private class SpliteratorImpl extends Spliterators.AbstractSpliterator<InDoipSegment> {
        FileItemIterator partIterator = null;
        private volatile InDoipSegment possibleSecondSegment;

        public SpliteratorImpl() {
            super(Long.MAX_VALUE, Spliterator.IMMUTABLE | Spliterator.NONNULL | Spliterator.ORDERED);
        }

        @Override
        @SuppressWarnings("resource")
        public boolean tryAdvance(Consumer<? super InDoipSegment> action) {
            if (terminalException != null) throw new UncheckedIOException(terminalException);
            if (isClosed) return false;
            try {
                if (ServletFileUpload.isMultipartContent(req)) {
                    return tryAdvanceMultipart(action);
                } else {
                    return tryAdvanceNonMultipart(action);
                }
            } catch (IOException | FileUploadException e) {
                throw new RuntimeException(e);
            }
        }

        public boolean tryAdvanceNonMultipart(Consumer<? super InDoipSegment> action) throws IOException {
            if (curr == null) { //Is first segment
                String contentType = req.getContentType();
                if (isJson(contentType)) {
                    JsonObject doipRequestJson = new JsonObject();
                    JsonElement json = JsonParser.parseReader(req.getReader());
                    if (json != null) {
                        doipRequestJson.add("input", json);
                    }
                    augmentRequestFromHttpRequestIfNecessary(doipRequestJson, req);
                    curr = new InDoipSegmentFromJson(doipRequestJson);
                } else {
                    //Support for non-json single body request
                    //e.g somebody posts some bytes to a custom op.
                    //We need to construct the first segment headers, which is JSON, and assign it to curr
                    //And we need to construct the second segment, which is binary, and hold on to it in possibleSecondPart
                    JsonObject doipRequestJson = new JsonObject();
                    augmentRequestFromHttpRequestIfNecessary(doipRequestJson, req);
                    curr = new InDoipSegmentFromJson(doipRequestJson);
                    if (!req.isEmpty()) {
                        possibleSecondSegment = new InDoipSegmentFromInputStream(false, req.getInputStream());
                    }
                }
                action.accept(curr);
                return true;
            } else if (possibleSecondSegment != null) {
                curr = possibleSecondSegment;
                possibleSecondSegment = null;
                action.accept(curr);
                return true;
            } else {
                isClosed = true;
                if (completer != null) completer.complete(null);
                return false;
            }
        }

        @SuppressWarnings("resource")
        public boolean tryAdvanceMultipart(Consumer<? super InDoipSegment> action) throws IOException, FileUploadException {
            if (partIterator == null) {
                ServletFileUpload upload = new ServletFileUpload();
                partIterator = upload.getItemIterator(req);
            }
            if (possibleSecondSegment != null) {
                curr = possibleSecondSegment;
                possibleSecondSegment = null;
                action.accept(curr);
                return true;
            }
            if (partIterator.hasNext()) {
                FileItemStream item = partIterator.next();
                String contentType = item.getContentType();
                //String partName = item.getFieldName();
                InputStream in = item.openStream();
                boolean isJson = false;
                if (contentType != null) {
                    if (isJson(contentType)) {
                        isJson = true;
                    }
                }
                if (curr == null) { //Is first segment
                    JsonObject doipRequestJson = new JsonObject();
                    augmentRequestFromHttpRequestIfNecessary(doipRequestJson, req);
                    curr = new InDoipSegmentFromJson(doipRequestJson);
                    possibleSecondSegment = new InDoipSegmentFromInputStream(false, in);
                } else {
                    curr = new InDoipSegmentFromInputStream(isJson, in);
                }
                action.accept(curr);
                return true;
            } else {
                isClosed = true;
                if (completer != null) completer.complete(null);
                return false;
            }
        }
    }
}
