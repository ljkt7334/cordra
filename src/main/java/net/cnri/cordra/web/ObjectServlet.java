package net.cnri.cordra.web;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.ServletAuthUtil;
import net.cnri.cordra.model.*;
import net.cnri.cordra.util.HttpUtil;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.JsonUtil;
import net.cnri.util.StreamUtil;
import net.cnri.util.StringUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.*;
import java.util.stream.Collectors;

@WebServlet("/objects/*")
@MultipartConfig(fileSizeThreshold=1024*1024)
public class ObjectServlet extends HttpServlet {
    private static Logger logger = LoggerFactory.getLogger(ObjectServlet.class);
    private static final String JSON = "json";
    private static final String CONTENT = "content";
    private static final String ACL = "acl";
    private static final String USER_METADATA = "userMetadata";

    private CordraService cordra;
    private InternalCordraClient internalCordra;
    private Gson gson;
    private Gson prettyGson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            prettyGson = GsonUtility.getPrettyGson();
            cordra = CordraServiceFactory.getCordraService();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = ServletUtil.getPath(req);
        if (objectId != null && !objectId.isEmpty()) objectId = objectId.substring(1);
        if (objectId == null || objectId.isEmpty()) {
            String query = req.getParameter("query");
            String queryJson = req.getParameter("queryJson");
            if (query == null && queryJson == null) {
                ServletErrorUtil.badRequest(resp, "Missing object id in GET");
                return;
            } else {
                doSearch(req, resp);
                return;
            }
        }
        String jsonPointer = req.getParameter("jsonPointer");
        String filterJson = req.getParameter("filter");
        String payload = req.getParameter("payload");
        boolean isWantText = ServletUtil.getBooleanParameter(req, "text");
        boolean isFull = ServletUtil.getBooleanParameter(req, "full");
        boolean isPretty = ServletUtil.getBooleanParameter(req, "pretty", false);
        boolean hasResponseContext = ServletUtil.getBooleanParameter(req, "includeResponseContext");

        if (payload != null) {
            boolean metadata = ServletUtil.getBooleanParameter(req, "metadata");
            if (metadata) {
                doGetPayloadMetadata(req, resp, objectId, payload);
            } else {
                doGetPayload(req, resp, objectId, payload);
            }
        } else {
            if (jsonPointer != null) {
                if (JsonUtil.isValidJsonPointer(jsonPointer)) {
                    doGetJsonPointer(req, resp, objectId, jsonPointer, isWantText, isPretty, isFull);
                } else {
                    ServletErrorUtil.badRequest(resp, "Invalid JSON Pointer " + jsonPointer);
                }
            } else {
                doGetObject(req, resp, objectId, isPretty, hasResponseContext, filterJson, isFull, isWantText);
            }
        }
    }

    private void doGetObject(HttpServletRequest req, HttpServletResponse resp, String objectId, boolean isPretty, boolean hasResponseContext, String filterJson, boolean isFull, boolean isWantText)  throws IOException {
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            if (filterJson != null) {
                options.filter = gson.fromJson(filterJson, new TypeToken<List<String>>(){}.getType());
                if (!isFull) {
                    options.filter = options.filter.stream()
                            .map(pointer -> "/content" + pointer)
                            .collect(Collectors.toList());
                }
            }
            options.includeResponseContext = true;
            CordraObject co = internalCordra.get(objectId, options);
            if (co == null) {
                throw new NotFoundCordraException("Missing object: " + objectId);
            }
            if (co.responseContext != null && co.responseContext.has("permission")) {
                resp.setHeader("X-Permission", co.responseContext.get("permission").getAsString());
            }
            if (!hasResponseContext) {
                co.responseContext = null;
            }
            resp.setHeader("Location", StringUtils.encodeURLPath("/objects/" + objectId));
            if (co.type != null) {
                resp.setHeader("X-Schema", co.type);
            }
            String json;
            if (isFull) {
                json = toJson(co, isPretty);
                resp.getWriter().write(json);
            } else {
                if (isWantText) {
                    InternalCordraClient.JsonAndMediaType jsonAndMediaType = internalCordra.getJsonAndMediaTypeAtPointer(objectId, "", options);
                    writeTextElement(resp, jsonAndMediaType.json, jsonAndMediaType.mediaType);
                } else {
                    json = toJson(co.content, isPretty);
                    resp.getWriter().write(json);
                }
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private String toJson(Object obj, boolean isPretty) {
        String json;
        if (isPretty) {
            json = prettyGson.toJson(obj);
        } else {
            json = gson.toJson(obj);
        }
        return json;
    }

    private void doGetPayloadMetadata(HttpServletRequest req, HttpServletResponse resp, String objectId, String payloadName) throws IOException {
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            FileMetadataResponse metadataResponse = internalCordra.getPayloadMetadata(objectId, payloadName, options);
            gson.toJson(metadataResponse, resp.getWriter());
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetPayload(HttpServletRequest req, HttpServletResponse resp, String objectId, String payloadName) throws IOException {
        try {
            Range range = getRangeFromRequest(req);
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            CallResponseHandler handler = new CallResponseHandlerForPayload(req, resp);
            internalCordra.getPartialPayload(objectId, payloadName, range.getStart(), range.getEnd(), handler, options);
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private void doGetJsonPointer(HttpServletRequest req, HttpServletResponse resp, String objectId, String jsonPointer, boolean isWantText, boolean isPretty, boolean isFull) throws IOException {
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            if (!isFull) {
                jsonPointer = "/content" + jsonPointer;
            }
            if (isWantText) {
                InternalCordraClient.JsonAndMediaType jsonAndMediaType = internalCordra.getJsonAndMediaTypeAtPointer(objectId, jsonPointer, options);
                writeTextElement(resp, jsonAndMediaType.json, jsonAndMediaType.mediaType);
            } else {
                JsonElement jsonElement = internalCordra.getJsonAtPointer(objectId, jsonPointer, options);
                String json = toJson(jsonElement, isPretty);
                resp.getWriter().write(json);
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Something went wrong getting " + objectId, e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    Range getRangeFromRequest(HttpServletRequest req) {
        if (req.getHeader("If-Range") != null) return new Range(null, null);
        String rangeHeader = req.getHeader("Range");
        if (rangeHeader == null) return new Range(null, null);
        if (!rangeHeader.startsWith("bytes=")) return new Range(null, null);
        if (rangeHeader.contains(",")) return new Range(null, null);
        String rangePart = rangeHeader.substring(6);
        String[] parts = rangePart.split("-");
        if (parts.length == 1 && rangePart.endsWith("-")) {
            return new Range(Long.parseLong(parts[0]), null);
        }

        if (parts.length != 2) return new Range(null, null);
        try {
            if (parts[0].isEmpty()) {
                return new Range(null, Long.parseLong(parts[1]));
            } else if (parts[1].isEmpty()) {
                return new Range(Long.parseLong(parts[0]), null);
            } else {
                return new Range(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
            }
        } catch (NumberFormatException e) {
            return new Range(null, null);
        }
    }

    private void writeTextElement(HttpServletResponse resp, JsonElement jsonElement, String mediaType) throws IOException {
        if (isValueElement(jsonElement)) {
            if (mediaType == null) {
                mediaType = "text/plain";
            }
            resp.setContentType(mediaType);
            if (!mediaType.contains(";")) {
                resp.setCharacterEncoding("UTF-8");
            }
            String text;
            if (jsonElement.isJsonNull()) {
                text = "null";
            } else {
                text = jsonElement.getAsString();
            }
            resp.getWriter().write(text);
        } else {
            ServletErrorUtil.badRequest(resp, "Requested json is not textual");
        }
    }

    private boolean isValueElement(JsonElement jsonElement) {
        return (jsonElement.isJsonPrimitive() || jsonElement.isJsonNull());
    }

    private void doSearch(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/search");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        createOrUpdate(req, resp, false);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (ServletUtil.isForm(req)) {
            doGet(req, resp);
        } else {
            createOrUpdate(req, resp, true);
        }
    }

    private void createOrUpdate(HttpServletRequest req, HttpServletResponse resp, boolean isCreate) throws IOException {
        // Note: due to longstanding Tomcat behaviour, ensure that getParts() is called before getParameter() for multipart PUT requests
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = ServletUtil.getPath(req);
        if (objectId != null && !objectId.isEmpty()) objectId = objectId.substring(1);
        else objectId = null;

        if (isCreate && objectId != null && !objectId.isEmpty()) {
            ServletErrorUtil.badRequest(resp, "Unexpected object id in POST");
            return;
        }
        if (!isCreate && (objectId == null || objectId.isEmpty())) {
            ServletErrorUtil.badRequest(resp, "Missing object id in PUT");
            return;
        }

        String handle = null;
        List<Payload> payloads = null;
        try {
            Request requestData = getJsonAndPayloadsFromRequest(req);
            String objectType = req.getParameter("type");
            if (objectType != null && !cordra.isKnownType(objectType)) {
                ServletErrorUtil.badRequest(resp, "Unknown type " + objectType);
                return;
            }
            if (isCreate) {
                if (objectType == null || objectType.isEmpty()) {
                    ServletErrorUtil.badRequest(resp, "Missing object type");
                    return;
                }
            }
            List<String> payloadsToDelete = new ArrayList<>();
            String[] payloadsToDeleteArray = req.getParameterValues("payloadToDelete");
            if (payloadsToDeleteArray != null) {
                for (String payloadName : payloadsToDeleteArray) {
                    payloadsToDelete.add(payloadName);
                }
            }
            if (isCreate) {
                handle = req.getParameter("handle");
                if (handle == null) {
                    String suffix = req.getParameter("suffix");
                    if (suffix != null) handle = internalCordra.getHandleForSuffix(suffix);
                }
            } else {
                handle = objectId;
            }

            String jsonData = requestData.json;
            String aclString = requestData.acl;
            String userMetadataString = requestData.userMetadata;
            payloads = requestData.payloads;
            if (payloads != null) {
                for (Payload payload : payloads) {
                    payloadsToDelete.remove(payload.name);
                }
            }
            if (jsonData == null) {
                ServletErrorUtil.badRequest(resp, "Missing JSON");
                return;
            }
            CordraObject.AccessControlList acl = null;
            if (aclString != null) {
                try {
                    acl = gson.fromJson(aclString, CordraObject.AccessControlList.class);
                } catch (JsonParseException e) {
                    ServletErrorUtil.badRequest(resp, "Invalid ACL format");
                    return;
                }
            }
            JsonObject userMetadata = null;
            if (userMetadataString != null) {
                try {
                    userMetadata = JsonParser.parseString(userMetadataString).getAsJsonObject();
                } catch (Exception e) {
                    ServletErrorUtil.badRequest(resp, "Invalid userMetadata format");
                    return;
                }
            }
            CordraObject requestCordraObject = new CordraObject();
            requestCordraObject.type = objectType;
            requestCordraObject.id = handle;
            requestCordraObject.payloads = payloads;
            requestCordraObject.content = ServletUtil.getContentAsJsonElement(jsonData);
            requestCordraObject.acl = acl;
            requestCordraObject.userMetadata = userMetadata;
            for (String payloadName : payloadsToDelete) {
                requestCordraObject.deletePayload(payloadName);
            }

            CordraObject co;
            String jsonPointer = req.getParameter("jsonPointer");
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            options.includeResponseContext = true;
            if (isCreate) {
                co = internalCordra.create(requestCordraObject, options);
            } else {
                if (jsonPointer != null) {
                    co = internalCordra.updateAtJsonPointer(objectId, jsonPointer, requestCordraObject.content, options);
                } else {
                    co = internalCordra.update(requestCordraObject, options);
                }
            }
            if (co.responseContext != null && co.responseContext.has("permission")) {
                resp.setHeader("X-Permission", co.responseContext.get("permission").getAsString());
            }
            boolean hasResponseContext = ServletUtil.getBooleanParameter(req, "includeResponseContext");
            if (!hasResponseContext) {
                co.responseContext = null;
            }
            resp.setStatus(HttpServletResponse.SC_OK);
            resp.setHeader("Location", StringUtils.encodeURLPath("/objects/" + co.id));
            resp.setHeader("X-Schema", co.type);
            boolean isFull = ServletUtil.getBooleanParameter(req, "full");
            if (isFull) {
//                if (co.metadata != null) co.metadata.internalMetadata = null;
                String fullJson = gson.toJson(co);
                resp.getWriter().write(fullJson);
            } else if (!isCreate && jsonPointer != null) {
                JsonNode node = JacksonUtil.gsonToJackson(co.content);
                node = JacksonUtil.getJsonAtPointer(jsonPointer, node);
                JacksonUtil.printJson(resp.getWriter(), node);
            } else {
                resp.getWriter().write(co.getContentAsString());
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected exception in doPost", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (InvalidException e) {
            ServletErrorUtil.badRequest(resp, e.getMessage());
        } catch (Exception e) {
            logger.error("Unexpected exception in doPost", e);
            ServletErrorUtil.internalServerError(resp);
        } finally {
            if (payloads != null) {
                for (Payload payload : payloads) {
                    try { payload.getInputStream().close(); } catch (Exception e) { }
                }
            }
        }
    }

    private static class Request {
        String json;
        String acl;
        String userMetadata;
        List<Payload> payloads;

        public Request(String json) {
            this.json = json;
        }

        public Request(String json, String acl, String userMetadata, List<Payload> payloads) {
            this.json = json;
            this.acl = acl;
            this.userMetadata = userMetadata;
            this.payloads = payloads;
        }
    }

    private Request getJsonAndPayloadsFromRequest(HttpServletRequest req) throws IOException, ServletException {
        // Note: due to longstanding Tomcat behaviour, ensure that getParts() is called before getParameter() for multipart PUT requests
        if (ServletUtil.isMultipart(req)) {
            String jsonData = null;
            String acl = null;
            String userMetadata = null;
            List<Payload> payloads = new ArrayList<>();
            Collection<Part> parts = req.getParts();
            for(Part part : parts) {
                String partName = part.getName();
                String filename = HttpUtil.getContentDispositionFilename(part.getHeader("Content-Disposition"));
                if ((JSON.equals(partName) || CONTENT.equals(partName)) && filename == null) {
                    jsonData = getPartAsString(part);
                } else if (ACL.equals(partName) && filename == null) {
                    acl = getPartAsString(part);
                } else if (USER_METADATA.equals(partName) && filename == null) {
                    userMetadata = getPartAsString(part);
                } else {
                    String payloadName = partName;
                    if (filename != null) {
                        // form-data without filename treated as parameters, so does not generate a payload
                        Payload payload = new Payload();
                        payload.name = payloadName;
                        payload.setInputStream(part.getInputStream());
                        payload.mediaType = part.getContentType();
                        payload.filename = filename;
                        payloads.add(payload);
                    }
                }
            }
            return new Request(jsonData, acl, userMetadata, payloads);
        } else if (ServletUtil.isForm(req) && "POST".equals(req.getMethod())) {
            String content = req.getParameter(CONTENT);
            if (content == null) content = req.getParameter(JSON);
            return new Request(content);
        } else if (ServletUtil.isForm(req)) {
            return new Request(getJsonFromFormData(StreamUtil.readFully(req.getReader())));
        } else {
            return new Request(StreamUtil.readFully(req.getReader()));
        }
    }

    static String getJsonFromFormData(String data) {
        String[] strings = data.split("&");
        for (String string : strings) {
            int equals = string.indexOf('=');
            if (equals < 0) {
                String name = StringUtils.decodeURL(string);
                if (JSON.equals(name) || CONTENT.equals(name)) return "";
            } else {
                String name = StringUtils.decodeURL(string.substring(0, equals));
                if (JSON.equals(name) || CONTENT.equals(name)) {
                    String value = StringUtils.decodeURL(string.substring(equals + 1));
                    return value;
                }
            }
        }
        return null;
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        String objectId = ServletUtil.getPath(req);
        if (objectId != null && !objectId.isEmpty()) objectId = objectId.substring(1);
        if (objectId == null) objectId = "";
        boolean deleteObjectWithEmptyId = ServletUtil.getBooleanParameter(req, "deleteObjectWithEmptyId", false);
        if (deleteObjectWithEmptyId && !objectId.isEmpty()) {
            ServletErrorUtil.badRequest(resp, "Specified object id to delete but also deleteObjectWithEmptyId");
            return;
        }
        if (objectId.isEmpty() && !deleteObjectWithEmptyId) {
            ServletErrorUtil.badRequest(resp, "Missing object id in DELETE");
            return;
        }
        String jsonPointer = req.getParameter("jsonPointer");
        String payloadName = req.getParameter("payload");
        if (jsonPointer != null && payloadName != null) {
            ServletErrorUtil.badRequest(resp, "DELETE specified both jsonPointer and payload");
            return;
        }
        if ("".equals(jsonPointer)) {
            ServletErrorUtil.badRequest(resp, "Cannot delete empty json pointer");
            return;
        }
        try {
            Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            if (jsonPointer == null && payloadName == null) {
                internalCordra.delete(objectId, options);
                resp.getWriter().write("{}");
            } else {
                if (jsonPointer != null) {
                    boolean isFull = ServletUtil.getBooleanParameter(req, "full");
                    if (!isFull) {
                        jsonPointer = "/content" + jsonPointer;
                    }
                    internalCordra.deleteJsonAtPointer(objectId, jsonPointer, options);
                }
                if (payloadName != null) {
                    internalCordra.deletePayload(objectId, payloadName, options);
                }
                resp.getWriter().write("{}");
            }
        } catch (InternalErrorCordraException e) {
            logger.error("Unexpected error calling listMethods", e);
            ServletErrorUtil.internalServerError(resp);
        } catch (CordraException e) {
            ServletErrorUtil.writeCordraException(resp, e);
        } catch (Exception e) {
            logger.error("Unexpected error calling listMethods", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }

    private static String getPartAsString(Part part) throws IOException {
        InputStream in = part.getInputStream();
        String result = IOUtils.toString(in, "UTF-8").trim();
        in.close();
        return result;
    }


    private static class CallResponseHandlerForPayload implements CallResponseHandler {
        private final HttpServletRequest req;
        private final HttpServletResponse resp;
        private boolean hasSetCharacterEncoding;

        public CallResponseHandlerForPayload(HttpServletRequest req, HttpServletResponse resp) {
            this.req = req;
            this.resp = resp;
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return resp.getOutputStream();
        }

        @Override
        public Writer getWriter() throws IOException {
            if (!hasSetCharacterEncoding) {
                resp.setCharacterEncoding("UTF-8");
                hasSetCharacterEncoding = true;
            }
            return resp.getWriter();
        }

        @Override
        public void setMediaType(String mediaType) {
            if (mediaType.toLowerCase(Locale.ROOT).contains("charset")) {
                hasSetCharacterEncoding = true;
            }
            resp.setContentType(mediaType);
        }

        @Override
        public void setFilename(String filename) {
            String disposition = req.getParameter("disposition");
            if (disposition == null) disposition = "inline";
            resp.setHeader("Content-Disposition", HttpUtil.contentDispositionHeaderFor(disposition, filename));
        }

        @Override
        public void setLength(long length) {
            resp.setContentLengthLong(length);
        }

        @Override
        public void setRange(boolean partial, boolean unsatisfied, Long start, Long end, Long size) {
            if (unsatisfied) {
                resp.setStatus(HttpServletResponse.SC_REQUESTED_RANGE_NOT_SATISFIABLE);
                if (size != null) {
                    resp.setHeader("Content-Range", "bytes */" + size);
                }
            } else if (partial) {
                resp.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
                if (size == null) {
                    if (start != null && end != null) {
                        resp.setHeader("Content-Range", "bytes " + start + "-" + end + "/*");
                    }
                } else if (start == null || end == null) {
                    resp.setHeader("Content-Range", "bytes */" + size);
                } else {
                    resp.setHeader("Content-Range", "bytes " + start + "-" + end + "/" + size);
                }
            }
        }
    }
}
