package net.cnri.cordra.javascript;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import net.cnri.cordra.CordraSearcher;
import net.cnri.cordra.CordraService;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.*;
import net.cnri.util.StreamUtil;

public class CordraServiceForLifeCycleHooks {

    private CordraService cordraService;

    public CordraServiceForLifeCycleHooks() {
    }

    public void init(@SuppressWarnings("hiding") CordraService cordraService) {
        this.cordraService = cordraService;
    }

    public String get(String id, Boolean full) throws CordraException {
        try {
            if (full == null) {
                Boolean legacy = cordraService.getDesign().useLegacyContentOnlyJavaScriptHooks;
                if (legacy != null && legacy.booleanValue()) {
                    full = false;
                }
            }
            if (full == null || full.booleanValue() == true) {
                return cordraService.getFullObjectJson(id);
            } else {
                return cordraService.getObjectJson(id);
            }
        } catch (NotFoundCordraException e) {
            return null;
        }
    }

//    public String get(String id) throws CordraException {
//        return cordraService.getObjectJson(id);
//    }

    public String search(String query, String paramsAsString) throws CordraException, IOException {
        StringWriter stringWriter = new StringWriter();
        QueryParams params = QueryParams.DEFAULT;
        if (paramsAsString != null && !paramsAsString.isEmpty()) {
            params = GsonUtility.getGson().fromJson(paramsAsString, QueryParams.class);
        }
        try {
            cordraService.ensureIndexUpToDate();
            String restrictedQuery = "(" + query + ") -isVersion:true -objatt_isVersion:true -id:design";
            try (SearchResults<CordraObject> results = cordraService.search(restrictedQuery, params, false, null)) {
                CordraSearcher.writeResults(results, stringWriter, params, true);
            }
        } catch (UncheckedCordraException e) {
            throw e.getCause();
        }
        return stringWriter.toString();
    }

    // TODO not yet implemented in cordra.js
//    public String searchAsUser(String query, String paramsAsString, String userId, List<String> groupIds, boolean grantAuthenticatedAccess) throws CordraException, IOException {
//        StringWriter stringWriter = new StringWriter();
//        QueryParams params = QueryParams.DEFAULT;
//        if (paramsAsString != null && !paramsAsString.isEmpty()) {
//            params = GsonUtility.getGson().fromJson(paramsAsString, QueryParams.class);
//        }
//        try {
//            cordraService.ensureIndexUpToDate();
//            cordraService.searchWithQueryCustomizationAndRestriction(query, params, stringWriter,
//                    false, userId, null, true, grantAuthenticatedAccess, groupIds, true);
//        } catch (ScriptException | InterruptedException e) {
//            throw new InternalErrorCordraException(e);
//        }
//        return stringWriter.toString();
//    }

    public InputStream getPayloadAsInputStream(String id, String payloadName) throws CordraException {
        return cordraService.getPayloadByNameOrNull(id, payloadName, null, null);
    }

    public Reader getPayloadAsReader(String id, String payloadName) throws CordraException {
        InputStream is = getPayloadAsInputStream(id, payloadName);
        if (is == null) return null;
        return new InputStreamReader(is, StandardCharsets.UTF_8);
    }

    public byte[] getPayloadAsBytes(String id, String payloadName) throws CordraException, IOException {
        try (InputStream in = getPayloadAsInputStream(id, payloadName)) {
            if (in == null) return null;
            return StreamUtil.readFully(in);
        }
    }

    public String getPayloadAsString(String id, String payloadName) throws CordraException, IOException {
        byte[] bytes = getPayloadAsBytes(id, payloadName);
        if (bytes == null) return null;
        return new String(bytes, StandardCharsets.UTF_8);
    }

    public InputStream getPartialPayloadAsInputStream(String id, String payloadName, Long start, Long end) throws CordraException {
        return cordraService.getPayloadByNameOrNull(id, payloadName, start, end);
    }

    public Reader getPartialPayloadAsReader(String id, String payloadName, Long start, Long end) throws CordraException {
        InputStream is = getPartialPayloadAsInputStream(id, payloadName, start, end);
        if (is == null) return null;
        return new InputStreamReader(is, StandardCharsets.UTF_8);
    }

    public byte[] getPartialPayloadAsBytes(String id, String payloadName, Long start, Long end) throws CordraException, IOException {
        try (InputStream in = getPartialPayloadAsInputStream(id, payloadName, start, end)) {
            if (in == null) return null;
            return StreamUtil.readFully(in);
        }
    }

    public String getPartialPayloadAsString(String id, String payloadName, Long start, Long end) throws CordraException, IOException {
        byte[] bytes = getPartialPayloadAsBytes(id, payloadName, start, end);
        if (bytes == null) return null;
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
