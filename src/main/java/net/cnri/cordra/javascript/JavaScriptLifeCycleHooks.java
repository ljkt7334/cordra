package net.cnri.cordra.javascript;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import jdk.nashorn.api.scripting.JSObject;
import jdk.nashorn.api.scripting.NashornException;
import jdk.nashorn.api.scripting.ScriptObjectMirror;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.DefaultAcls;
import net.cnri.cordra.auth.RequestAuthenticationInfo;
import net.cnri.cordra.auth.AuthenticationResult;
import net.cnri.cordra.model.ObjectDelta;
import net.cnri.cordra.model.SearchRequest;
import net.cnri.util.javascript.JavaScriptEnvironment;
import net.cnri.util.javascript.JavaScriptRunner;
import net.handle.hdllib.HandleValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import javax.script.ScriptException;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

public class JavaScriptLifeCycleHooks {

    private static final Logger logger = LoggerFactory.getLogger(JavaScriptLifeCycleHooks.class);
    static final Gson gson = GsonUtility.getPrettyGson();

    public static final String ON_OBJECT_RESOLUTION = "onObjectResolution";
    public static final String ON_PAYLOAD_RESOLUTION = "onPayloadResolution";
    public static final String BEFORE_DELETE = "beforeDelete";
    public static final String BEFORE_SCHEMA_VALIDATION = "beforeSchemaValidation";
    public static final String BEFORE_SCHEMA_VALIDATION_WITH_ID = "beforeSchemaValidationWithId";
    public static final String BEFORE_STORAGE = "beforeStorage";
    public static final String OBJECT_FOR_INDEXING = "objectForIndexing";
    public static final String GENERATE_ID = "generateId";
    public static final String IS_GENERATE_ID_LOOPABLE = "isGenerateIdLoopable";
    public static final String CREATE_HANDLE_VALUES = "createHandleValues";
    public static final String CUSTOMIZE_QUERY = "customizeQuery";
    public static final String CUSTOMIZE_QUERY_AND_PARAMS = "customizeQueryAndParams";
    public static final String AUTHENTICATE = "authenticate";
    public static final String GET_AUTH_CONFIG = "getAuthConfig";

    public static final String AFTER_CREATE_OR_UPDATE = "afterCreateOrUpdate";
    public static final String AFTER_DELETE = "afterDelete";

    private final DateTimeFormatter dateTimeFormatter = CordraService.dateTimeFormatter;

    private final JavaScriptEnvironment javaScriptEnvironment;
    private final boolean traceRequests;
    private final CordraRequireLookup cordraRequireLookup;
    private final Supplier<Design> designSupplier;

    public JavaScriptLifeCycleHooks(JavaScriptEnvironment javaScriptEnvironment, boolean traceRequests, CordraRequireLookup cordraRequireLookup, Supplier<Design> designSupplier) {
        this.javaScriptEnvironment = javaScriptEnvironment;
        this.traceRequests = traceRequests;
        this.cordraRequireLookup = cordraRequireLookup;
        this.designSupplier = designSupplier;
        warmUp();
    }

    private void warmUp() {
        Thread t = new Thread(() -> {
            logger.info("Warming up JavaScriptLifeCycleHooks");
            try {
                javaScriptEnvironment.warmUp();
                logger.info("Warmed up JavaScriptLifeCycleHooks");
            } catch (Exception e) {
                logger.error("Error warming up JavaScriptLifeCycleHooks", e);
            }
        });
        t.setDaemon(true);
        t.start();
    }

    public JavaScriptRunner getJavaScriptRunner() {
        JavaScriptRunner runner = javaScriptEnvironment.getRunner(null, logger);
        Map<String, String> contextMap = MDC.getCopyOfContextMap();
        RequestContext requestContext = RequestContextHolder.get();
        runner.getEventLoop().submit(() -> {
            contextMap.forEach(MDC::put);
            RequestContextHolder.set(requestContext);
        });
        return runner;
    }

    public void recycleJavaScriptRunner(JavaScriptRunner runner) {
        long start = System.currentTimeMillis();
        try {
            runner.getEventLoop().submit(() -> {
                MDC.clear();
                RequestContextHolder.clear();
            });
            javaScriptEnvironment.recycle(runner);
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript recycleJavaScriptRunner: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    public String generateIdFromJavaScript(JavaScriptRunner runner, CordraObject co, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        long start = System.currentTimeMillis();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(GENERATE_ID, CordraRequireLookup.DESIGN_MODULE_ID, false, false, 400, runner);
            if (methodJSObject != null) {
                String input = gson.toJson(co);
                String output = runJavaScriptFunctionWithRunner(methodJSObject, input, context, false, 400, runner);
                if (output == null) return null;
                JsonElement outputJsonElement = JsonParser.parseString(output);
                if (outputJsonElement.isJsonNull()) return null;
                if (outputJsonElement.isJsonPrimitive()) return outputJsonElement.getAsString();
                throw new InternalErrorCordraException(GENERATE_ID + " returned object or array");
            }
            return null;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(GENERATE_ID + " generateIdFromJavaScript: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    public List<HandleValue> generateHandleValuesFromJavaScript(CordraObject co, Map<String, Object> context) throws ScriptException, InterruptedException, CordraException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        String inputString = gson.toJson(co);
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            Object moduleExports = runner.requireById(CordraRequireLookup.HANDLE_MINTING_CONFIG_MODULE_ID);
            if (!(moduleExports instanceof JSObject)) return Collections.emptyList();
            Object method = ((JSObject)moduleExports).getMember(CREATE_HANDLE_VALUES);
            if (!(method instanceof JSObject)) return Collections.emptyList();
            JSObject obj = (JSObject)runner.jsonParse(inputString);
            JSObject contextJSObject = jsonObjectifyContext(runner, context);
            Object[] params = new Object[] { obj, contextJSObject };
            Object resBeforeAwait = runner.submitAndGet(() -> ((JSObject)method).call(null, params));
            JSObject res = (JSObject)runner.awaitPromise(resBeforeAwait);
            String outputString = runner.jsonStringify(res);
            return net.handle.hdllib.GsonUtility.getGson().fromJson(outputString, new TypeToken<List<HandleValue>>() {}.getType());
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    public SearchRequest customizeQueryAndParams(SearchRequest queryAndParams, Map<String, Object> context) throws CordraException, InterruptedException, ScriptException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return queryAndParams; //Unchanged
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(CUSTOMIZE_QUERY_AND_PARAMS, CordraRequireLookup.DESIGN_MODULE_ID, false, false, 400, runner);
            if (methodJSObject == null) {
                return queryAndParams;
            } else {
                String inputString = gson.toJson(queryAndParams);
                String output = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodJSObject, inputString, context, false, 400, runner);
                if (output == null) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned undefined");
                JsonElement outputJsonElement = JsonParser.parseString(output);
                if (outputJsonElement.isJsonNull()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned null");
                if (outputJsonElement.isJsonPrimitive()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned primitive");
                if (outputJsonElement.isJsonArray()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY_AND_PARAMS + " returned array");
                SearchRequest result = gson.fromJson(outputJsonElement, SearchRequest.class);
                return result;
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(CUSTOMIZE_QUERY_AND_PARAMS + " runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    public String customizeQuery(String query, Map<String, Object> context) throws CordraException, InterruptedException, ScriptException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return query; //Unchanged
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(CUSTOMIZE_QUERY, CordraRequireLookup.DESIGN_MODULE_ID, false, false, 400, runner);
            if (methodJSObject == null) {
                return query; //Unchanged
            } else {
                JSObject contextJSObject = jsonObjectifyContext(runner, context);
                String output = runJavaScriptFunctionWithRunner(methodJSObject, query, contextJSObject, false, 400, runner);
                if (output == null) throw new InternalErrorCordraException(CUSTOMIZE_QUERY + " returned undefined");
                JsonElement outputJsonElement = JsonParser.parseString(output);
                if (outputJsonElement.isJsonNull()) throw new InternalErrorCordraException(CUSTOMIZE_QUERY + " returned null");
                if (outputJsonElement.isJsonPrimitive()) return outputJsonElement.getAsString();
                throw new InternalErrorCordraException(CUSTOMIZE_QUERY + " returned object or array");
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("customizeQuery runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    public DefaultAcls getAuthConfig(String type, Map<String, Object> context) throws InterruptedException, ScriptException, CordraException {
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return null;
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(GET_AUTH_CONFIG, moduleId, false, false, 403, runner);
            if (methodJSObject == null) {
                return null;
            } else {
                JSObject contextJSObject = jsonObjectifyContext(runner, context);
                String output = runJavaScriptFunctionWithRunner(methodJSObject, null, contextJSObject, true, 403, runner);
                if (output == null) return null;
                JsonElement outputJsonElement = JsonParser.parseString(output);
                if (outputJsonElement.isJsonNull()) return null;
                if (outputJsonElement.isJsonPrimitive()) throw new InternalErrorCordraException(AUTHENTICATE + " returned primitive");
                if (outputJsonElement.isJsonArray()) throw new InternalErrorCordraException(AUTHENTICATE + " returned array");
                DefaultAcls defaultAcls = gson.fromJson(outputJsonElement, DefaultAcls.class);
                return defaultAcls;
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("getAuthConfig: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    public AuthenticationResult authenticate(RequestAuthenticationInfo authInfo, Map<String, Object> context) throws CordraException, InterruptedException, ScriptException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return null;
        }
        long start = System.currentTimeMillis();
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(AUTHENTICATE, CordraRequireLookup.DESIGN_MODULE_ID, false, false, 401, runner);
            if (methodJSObject == null) {
                return null;
            } else {
                String inputString = gson.toJson(authInfo);
                Object inputObj = parseJsonForJavaScript(runner, inputString);
                JSObject contextJSObject = jsonObjectifyContext(runner, context);
                String output = runJavaScriptFunctionWithRunner(methodJSObject, inputObj, contextJSObject, false, 401, runner);
                if (output == null) return null;
                JsonElement outputJsonElement = JsonParser.parseString(output);
                if (outputJsonElement.isJsonNull()) return null;
                if (outputJsonElement.isJsonPrimitive()) throw new InternalErrorCordraException(AUTHENTICATE + " returned primitive");
                if (outputJsonElement.isJsonArray()) throw new InternalErrorCordraException(AUTHENTICATE + " returned array");
                AuthenticationResult authResult = gson.fromJson(outputJsonElement, AuthenticationResult.class);
                return authResult;
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("authenticate runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    public GenerateIdJavaScriptStatus hasJavaScriptGenerateIdFunction(JavaScriptRunner runner) throws InterruptedException, ScriptException, CordraException {
        if (!cordraRequireLookup.exists(CordraRequireLookup.DESIGN_MODULE_ID)) {
            return new GenerateIdJavaScriptStatus(false, false);
        }
        JSObject methodJSObject = findJavaScriptFunctionWithRunner(GENERATE_ID, CordraRequireLookup.DESIGN_MODULE_ID, false, false, 400, runner);
        Object isLoopableJSObject = findJavaScriptMemberWithRunner(IS_GENERATE_ID_LOOPABLE, CordraRequireLookup.DESIGN_MODULE_ID, false, false, 400, runner);
        if (methodJSObject == null) {
            return new GenerateIdJavaScriptStatus(false, false);
        } else if (isLoopableJSObject instanceof Boolean) {
            boolean isLoopable = (Boolean)isLoopableJSObject;
            return new GenerateIdJavaScriptStatus(true, isLoopable);
        } else {
            return new GenerateIdJavaScriptStatus(true, false);
        }
    }

    public static class GenerateIdJavaScriptStatus {
        public final boolean hasFunction;
        public final boolean isLoopable;

        public GenerateIdJavaScriptStatus(boolean hasFunction, boolean isLoopable) {
            this.hasFunction = hasFunction;
            this.isLoopable = isLoopable;
        }
    }

    public void beforeDelete(CordraObject co, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException {
        runJavaScriptFunction(co, BEFORE_DELETE, context, 403);
    }

    public void afterDelete(CordraObject co, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException {
        runJavaScriptFunction(co, AFTER_DELETE, context, 400);
    }

    public boolean typeHasOnObjectResolution(String type) throws CordraException, ScriptException, InterruptedException {
        return typeHasJavaScriptFunction(type, JavaScriptLifeCycleHooks.ON_OBJECT_RESOLUTION, 403);
    }

    public CordraObject onObjectResolution(CordraObject co, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException {
        return runJavaScriptFunction(co, JavaScriptLifeCycleHooks.ON_OBJECT_RESOLUTION, context, 403);
    }

    public void warmUpModule(String moduleId) {
        try {
            if (!cordraRequireLookup.exists(moduleId)) return;
            // TODO investigate whether there is benefit to warming up multiple instances of JavaScriptRunner
            JavaScriptRunner runner = getJavaScriptRunner();
            try {
                runner.requireById(moduleId);
            } finally {
                recycleJavaScriptRunner(runner);
            }
        } catch (Exception e) {
            logger.warn("Exception warming up JavaScript for " + moduleId, e);
        }
    }

    public ObjectDelta beforeSchemaValidation(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException, InvalidException {
        return beforeSchemaValidationGeneric(BEFORE_SCHEMA_VALIDATION, type, originalObject, objectDelta, context);
    }

    public ObjectDelta beforeSchemaValidationWithId(String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException, InvalidException {
        ObjectDelta res = beforeSchemaValidationGeneric(BEFORE_SCHEMA_VALIDATION_WITH_ID, type, originalObject, objectDelta, context);
        if (originalObject == null && (res.id == null || !res.id.equals(objectDelta.id))) {
            throw new InternalErrorCordraException("Cannot change id of object in beforeSchemaValidationWithId");
        }
        return res;
    }

    private ObjectDelta beforeSchemaValidationGeneric(String hookName, String type, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException, InvalidException {
        originalObject = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(originalObject);
        long start = System.currentTimeMillis();
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return objectDelta; // unchanged
        }
        boolean isUpdate = originalObject != null;
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(hookName, moduleId, false, false, 400, runner);
            if (methodJSObject != null) {
                Design design = designSupplier.get();
                if (!Boolean.TRUE.equals(design.useLegacyContentOnlyJavaScriptHooks)) {
                    return beforeSchemaValidationFull(methodJSObject, originalObject, objectDelta, context, isUpdate, runner);
                } else {
                    context.put("useLegacyContentOnlyJavaScriptHooks", Boolean.TRUE);
                    return beforeSchemaValidationLegacy(methodJSObject, objectDelta, context, runner);
                }
            }
            return objectDelta; // unchanged;
        } finally {
            recycleJavaScriptRunner(runner);
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(hookName + " runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    private ObjectDelta beforeSchemaValidationLegacy(JSObject methodJSObject, ObjectDelta objectDelta, Map<String, Object> context, JavaScriptRunner runner) throws CordraException, ScriptException, InterruptedException, InvalidException {
        String input = objectDelta.jsonData;
        String output = runJavaScriptFunctionWithRunnerDefaultReturnInputThrowsInvalidException(methodJSObject, input, context, false, 400, runner);
        return new ObjectDelta(objectDelta.id, objectDelta.type, output, objectDelta.acl, objectDelta.userMetadata, objectDelta.payloads, objectDelta.payloadsToDelete);
    }

    private ObjectDelta beforeSchemaValidationFull(JSObject methodJSObject, CordraObject originalObject, ObjectDelta objectDelta, Map<String, Object> context, boolean isUpdate, JavaScriptRunner runner) throws CordraException, ScriptException, InterruptedException, InvalidException {
        CordraObject inputObject;
        if (isUpdate) {
            inputObject = objectDelta.asCordraObjectForUpdate(originalObject);
        } else {
            inputObject = objectDelta.asCordraObjectForCreate();
        }
        String input = gson.toJson(inputObject);
        String output = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodJSObject, input, context, false, 400, runner);
        if (isUpdate) {
            return ObjectDelta.fromStringifiedCordraObjectForUpdate(originalObject, output, objectDelta.payloads);
        } else {
            return ObjectDelta.fromStringifiedCordraObjectForCreate(output, objectDelta.payloads);
        }
    }


    public void afterCreateOrUpdate(CordraObject co, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException {
        runJavaScriptFunction(co, AFTER_CREATE_OR_UPDATE, context, 400);
    }

    public void beforeStorage(CordraObject co, Map<String, Object> context) throws CordraException, ScriptException, InterruptedException {
        runJavaScriptFunction(co, BEFORE_STORAGE, context, 400);
    }

    private Object findJavaScriptMemberWithRunner(String memberName, String moduleId, boolean isStatic, boolean isMethod, int defaultErrorResponseCode, JavaScriptRunner runner) throws ScriptException, InterruptedException, CordraException {
        long start = System.currentTimeMillis();
        try {
            Object moduleExports = runner.requireById(moduleId);
            if (!(moduleExports instanceof JSObject)) return null;
            Object member;
            if (isMethod) {
                String methodsMemberName = isStatic ? "staticMethods" : "methods";
                Object methods = ((JSObject) moduleExports).getMember(methodsMemberName);
                if (!(methods instanceof JSObject)) return null;
                member = ((JSObject) methods).getMember(memberName);
            } else {
                member = ((JSObject) moduleExports).getMember(memberName);
            }
            return member;
        } catch (RuntimeException e) {
            if (e.getCause() instanceof CordraException) throw (CordraException)e.getCause();
            throw e;
        } catch (ScriptException e) {
            CordraException exception = extractCordraException(defaultErrorResponseCode, e, runner);
            if (exception == null) {
                logUnexpectedScriptException(e);
                throw e;
            } else {
                throw exception;
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(memberName + " findJavaScriptMemberWithRunner: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    private JSObject findJavaScriptFunctionWithRunner(String functionName, String moduleId, boolean isStatic, boolean isMethod, int defaultErrorResponseCode, JavaScriptRunner runner) throws ScriptException, InterruptedException, CordraException {
        Object method = findJavaScriptMemberWithRunner(functionName, moduleId, isStatic, isMethod, defaultErrorResponseCode, runner);
        if (!(method instanceof JSObject)) return null;
        return (JSObject) method;
    }

    private String runJavaScriptFunctionWithRunner(JSObject method, String input, Map<String, Object> context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException, ScriptException, InterruptedException {
        JSObject obj = (JSObject) parseJsonForJavaScript(runner, input);
        JSObject contextJSObject = jsonObjectifyContext(runner, context);
        return runJavaScriptFunctionWithRunner(method, obj, contextJSObject, isStatic, defaultErrorResponseCode, runner);
    }

    private String runJavaScriptFunctionWithRunnerDefaultReturnInput(JSObject method, String input, Map<String, Object> context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException, ScriptException, InterruptedException {
        Object obj = parseJsonForJavaScript(runner, input);
        JSObject contextJSObject = jsonObjectifyContext(runner, context);
        String output = runJavaScriptFunctionWithRunner(method, obj, contextJSObject, isStatic, defaultErrorResponseCode, runner);
        if (output == null) output = runner.jsonStringify(obj);
        return output;
    }

    private String runJavaScriptFunctionWithRunnerDefaultReturnInputThrowsInvalidException(JSObject method, String input, Map<String, Object> context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws CordraException, ScriptException, InterruptedException, InvalidException {
        Object obj = parseJsonForJavaScript(runner, input, true);
        JSObject contextJSObject = jsonObjectifyContext(runner, context);
        String output = runJavaScriptFunctionWithRunner(method, obj, contextJSObject, isStatic, defaultErrorResponseCode, runner);
        if (output == null) output = runner.jsonStringify(obj);
        return output;
    }

    private String runJavaScriptFunctionWithRunner(JSObject method, Object obj, JSObject context, boolean isStatic, int defaultErrorResponseCode, JavaScriptRunner runner) throws ScriptException, InterruptedException, CordraException {
        long start = System.currentTimeMillis();
        try {
            Object[] params;
            if (isStatic) {
                // static method call
                params = new Object[] { context };
            } else {
                params = new Object[] { obj, context };
            }
            Object resObj = runner.submitAndGet(() -> method.call(null, params));
            resObj = runner.awaitPromise(resObj);
            if (ScriptObjectMirror.isUndefined(resObj)) {
                return null;
            } else if (resObj == null) {
                return "null";
            } else if (resObj instanceof JSObject) {
                return runner.jsonStringify(resObj);
            } else {
                return gson.toJson(resObj);
            }
        } catch (RuntimeException e) {
            if (e.getCause() instanceof CordraException) throw (CordraException)e.getCause();
            throw e;
        } catch (ScriptException e) {
            CordraException exception = extractCordraException(defaultErrorResponseCode, e, runner);
            if (exception == null) {
                logUnexpectedScriptException(e);
                throw e;
            } else {
                throw exception;
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript runJavaScriptFunctionWithRunner: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    public Object parseJsonForJavaScript(JavaScriptRunner runner, String params) throws CordraException {
        try {
            return parseJsonForJavaScript(runner, params, false);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
    }

    private Object parseJsonForJavaScript(JavaScriptRunner runner, String params, boolean throwsInvalidException) throws CordraException, InvalidException {
        if (params == null) return null;
        long start = System.currentTimeMillis();
        try {
            return runner.jsonParse(params);
        } catch (ScriptException e) {
            // Note: when throwsInvalidException is false, this function is only used when the "params" is known to be JSON.
            // Notably it is not used directly to parse user-supplied CallServlet input.
            String message;
            if (!(e.getCause() instanceof NashornException)) {
                message = "Invalid JSON: " + e.getMessage();
            } else {
                message = ((NashornException) e.getCause()).getEcmaError().toString();
            }
            if (throwsInvalidException) {
                throw new InvalidException(message, e);
            } else {
                throw new InternalErrorCordraException(message, e);
            }
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript parseJsonForJavaScript: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    public static CordraException extractCordraException(int defaultResponseCode, ScriptException e, JavaScriptRunner runner) {
        if (!(e.getCause() instanceof NashornException)) return null;
        try {
            Object errorObj = ((NashornException) e.getCause()).getEcmaError();
            if (errorObj instanceof String || errorObj instanceof CharSequence) {
                // Nashorn uses a type ConsString for concatenated strings
                return cordraExceptionFromStatusCode(defaultResponseCode, CordraException.responseForMessage(errorObj.toString()), e.getCause());
            }
            if (errorObj instanceof JSObject) {
                Object nameObj = ((JSObject) errorObj).getMember("name");
                // "InvalidException" here is legacy, used by Fipro; leave undocumented...
                if ("CordraError".equals(nameObj) || "InvalidException".equals(nameObj)) {
                    int responseCode;
                    try {
                        Object responseCodeObj = ((JSObject) errorObj).getMember("responseCode");
                        if (responseCodeObj instanceof String) {
                            responseCode = Integer.parseInt((String) responseCodeObj);
                        } else if (responseCodeObj instanceof Number) {
                            responseCode = ((Number) responseCodeObj).intValue();
                        } else {
                            responseCode = defaultResponseCode;
                        }
                    } catch (Exception sce) {
                        responseCode = defaultResponseCode;
                    }
                    if (!((JSObject)errorObj).hasMember("response")) {
                        Object messageObj = ((JSObject) errorObj).getMember("message");
                        if (messageObj instanceof String || messageObj instanceof CharSequence) {
                            return cordraExceptionFromStatusCode(defaultResponseCode, CordraException.responseForMessage(messageObj.toString()), e.getCause());
                        }
                    }
                    Object responseObj = ((JSObject) errorObj).getMember("response");
                    String responseString;
                    if (responseObj == null || ScriptObjectMirror.isUndefined(responseObj)) {
                        responseString = null;
                    } else if (responseObj instanceof JSObject) {
                        responseString = runner.jsonStringify(responseObj);
                    } else {
                        responseString = gson.toJson(responseObj);
                    }
                    Throwable cause = e.getCause();
                    return CordraException.fromStatusCode(responseCode, responseString, cause);
                }
            }
            return null;
        } catch (Exception ex) {
            return null;
        }
    }

    private static CordraException cordraExceptionFromStatusCode(int responseCode, JsonElement response, Throwable cause) {
        try {
            return CordraException.fromStatusCode(responseCode, response, cause);
        } catch (IncompatibleClassChangeError e) {
            logger.warn("Unexpected mismatch of cordra jar versions on call to CordraException.fromStatusCode");
            return CordraException.fromStatusCode(responseCode, gson.toJson(response), cause);
        }
    }

    public CordraObject runJavaScriptFunction(CordraObject co, String functionName, Map<String, Object> context, int defaultErrorResponseCode) throws CordraException, ScriptException, InterruptedException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(co.type);
        return runJavaScriptFunction(co, functionName, moduleId, context, defaultErrorResponseCode);
    }

    private CordraObject runJavaScriptFunction(CordraObject co, String functionName, String moduleId, Map<String, Object> context, int defaultErrorResponseCode) throws CordraException, ScriptException, InterruptedException {
        long start = System.currentTimeMillis();
        if (!cordraRequireLookup.exists(moduleId)) {
            return co; // unchanged
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(functionName, moduleId, false, false, defaultErrorResponseCode, runner);
            if (methodJSObject != null) {
                Design design = designSupplier.get();
                if (!Boolean.TRUE.equals(design.useLegacyContentOnlyJavaScriptHooks)) {
                    String input = gson.toJson(co);
                    String output = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodJSObject, input, context, false, defaultErrorResponseCode, runner);
                    return gson.fromJson(output, CordraObject.class);
                } else {
                    context.put("useLegacyContentOnlyJavaScriptHooks", Boolean.TRUE);
                    String input = co.getContentAsString();
                    String output = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodJSObject, input, context, false, defaultErrorResponseCode, runner);
                    co.content = JsonParser.parseString(output);
                    return co;
                }
            }
            return co; // unchanged;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace(functionName + " runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    public static class CallResult {
        public final String result;
        public final String before;
        public final String after;

        public CallResult(String result, String before, String after) {
            this.result = result;
            this.before = before;
            this.after = after;
        }
    }

    private static boolean isTruthy(Object obj) {
        if (obj == null || ScriptObjectMirror.isUndefined(obj)) return false;
        if (obj instanceof Boolean) return ((Boolean) obj).booleanValue();
        if (obj instanceof CharSequence) return ((CharSequence)obj).length() > 0;
        if (obj instanceof Number) {
            double num = ((Number) obj).doubleValue();
            if (Double.isNaN(num)) return false;
            if (num == 0.0) return false;
            return true;
        }
        return true;
    }

    public boolean callAllowsGet(String method, String moduleId, boolean isStatic) throws CordraException {
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            boolean isMethod = true;
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(method, moduleId, isStatic, isMethod, 400, runner);
            if (methodJSObject == null) {
                if (CordraRequireLookup.DESIGN_METHODS_MODULE_NAME.equals(moduleId)) moduleId = "The Cordra service";
                throw new NotFoundCordraException(moduleId + " does not have a " + (isStatic ? "static" : "") + " method called " + method);
            }
            Object allowGet = methodJSObject.getMember("allowGet");
            return isTruthy(allowGet);
        } catch (CordraException | RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new InternalErrorCordraException(e);
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    public CallResult call(String method, String moduleId, boolean isStatic, String coJson, Map<String, Object> context, DirectIo directIo, boolean isGet) throws ScriptException, InterruptedException, CordraException {
        String result;
        String before = null;
        String after = null;
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            boolean isMethod = true;
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(method, moduleId, isStatic, isMethod, 400, runner);
            if (methodJSObject == null) {
                if (CordraRequireLookup.DESIGN_METHODS_MODULE_NAME.equals(moduleId)) moduleId = "The Cordra service";
                throw new NotFoundCordraException(moduleId + " does not have a " + (isStatic ? "static" : "") + " method called " + method);
            }
            if (isGet) {
                Object allowGet = methodJSObject.getMember("allowGet");
                if (!isTruthy(allowGet)) {
                    throw new ForbiddenCordraException("Method " + method + " does not allow GET");
                }
            }
            Object obj = null;
            if (!isStatic) {
                obj = parseJsonForJavaScript(runner, coJson);
                before = runner.jsonStringify(obj);
            }
            JSObject contextJSObject = jsonObjectifyContext(runner, context);
            AtomicBoolean usedDirectOutput = new AtomicBoolean();
            JSObject prepareCallContext = (JSObject) runner.requireById(CordraRequireLookup.PREPARE_CALL_CONTEXT_MODULE_NAME);
            prepareCallContext.call(null, contextJSObject, directIo, usedDirectOutput, true);
            result = runJavaScriptFunctionWithRunner(methodJSObject, obj, contextJSObject, isStatic, 400, runner);
            if (usedDirectOutput.get()) {
                result = null;
            }
            if (!isStatic) {
                after = runner.jsonStringify(obj);
            }
        } finally {
            recycleJavaScriptRunner(runner);
        }
        return new CallResult(result, before, after);
    }

    private void addRequestContextToContext(Map<String, Object> context) {
        RequestContext requestContextObj = RequestContextHolder.get();
        if (requestContextObj != null) {
            JsonObject requestContext = requestContextObj.getRequestContext();
            if (requestContext != null) {
                context.put("requestContext", requestContext);
            }
        }
    }

    public List<String> listMethods(boolean isStatic, String moduleId) throws InterruptedException, ScriptException {
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            Object moduleExports = runner.requireById(moduleId);
            if (!(moduleExports instanceof JSObject)) {
                return Collections.emptyList();
            }
            String methodsMemberName = isStatic ? "staticMethods" : "methods";
            Object methods = ((JSObject) moduleExports).getMember(methodsMemberName);
            if (!(methods instanceof JSObject)) {
                return Collections.emptyList();
            }
            List<String> result = new ArrayList<>();
            for (String key : ((JSObject) methods).keySet()) {
                Object value = ((JSObject) methods).getMember(key);
                if (value instanceof JSObject && ((JSObject) value).isFunction()) {
                    result.add(key);
                }
            }
            return result;
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    // need to be careful about internalMetadata as it is used for indexing
    public CordraObject objectForIndexing(CordraObject co) throws CordraException, ScriptException, InterruptedException {
        long start = System.currentTimeMillis();
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(co.type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return co; // unchanged
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(OBJECT_FOR_INDEXING, moduleId, false, false, 400, runner);
            if (methodJSObject != null) {
                CordraObject inputObject = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co); //do we need to remove payloads?
                String input = gson.toJson(inputObject);
                Map<String, Object> context = new HashMap<>();
                context.put("objectId", co.id);
                String outputJson = runJavaScriptFunctionWithRunnerDefaultReturnInput(methodJSObject, input, context, false, 400, runner);
                CordraObject coResult;
                try {
                    coResult = GsonUtility.getGson().fromJson(outputJson, CordraObject.class);
                    coResult.id = inputObject.id; //Ensure the JS cannot change the digital object id
                    CordraService.restoreInternalMetadata(co, coResult);
                    return ObjectDelta.applyPayloadsToCordraObject(coResult, co.payloads);
                } catch (JsonParseException e) {
                    throw new InternalErrorCordraException("Couldn't parse json into CordraObject", e);
                }
            }
            return co; //unchanged
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("objectForIndexing runJavaScriptFunction: start " + startTime + ", " + delta + "ms");
            }
            recycleJavaScriptRunner(runner);
        }
    }

    private void logUnexpectedScriptException(ScriptException e) {
        if (e.getCause() instanceof NashornException) {
            NashornException ne = (NashornException) e.getCause();
            String message = ne.getMessage();
            if (message == null) message = "";
            if (ne.getEcmaError() instanceof JSObject) {
                JSObject jsError = (JSObject) ne.getEcmaError();
                if (jsError.hasMember("name") && !message.contains(jsError.getMember("name").toString())) message += ": " + jsError.getMember("name");
                if (jsError.hasMember("message") && !message.contains(jsError.getMember("message").toString())) message += ": " + jsError.getMember("message");
            }
            logger.error("Unexpected script exception: " + message + "\n" + NashornException.getScriptStackString(ne));
        }
    }

    private JSObject jsonObjectifyContext(JavaScriptRunner runner, Map<String, Object> context) throws CordraException {
        long start = System.currentTimeMillis();
        try {
            addRequestContextToContext(context);
            Map<String, Object> noJSObject = new HashMap<>();
            for (Map.Entry<String, Object> entry : context.entrySet()) {
                Object value = entry.getValue();
                if (value instanceof JSObject || ScriptObjectMirror.isUndefined(value)) continue;
                noJSObject.put(entry.getKey(), value);
            }
            JSObject res;
            try {
                res = (JSObject) runner.jsonParse(gson.toJson(noJSObject));
            } catch (ScriptException | JsonParseException e) {
                throw new InternalErrorCordraException("Error in enrichment", e);
            }
            for (Map.Entry<String, Object> entry : context.entrySet()) {
                Object value = entry.getValue();
                if (value instanceof JSObject || ScriptObjectMirror.isUndefined(value)) {
                    res.setMember(entry.getKey(), value);
                }
            }
            return res;
        } finally {
            if (traceRequests) {
                long end = System.currentTimeMillis();
                long delta = end - start;
                String startTime = dateTimeFormatter.format(Instant.ofEpochMilli(start));
                logger.trace("javascript jsonObjectifyContext: start " + startTime + ", " + delta + "ms");
            }
        }
    }

    //This could be improved with caching
    public boolean typeHasJavaScriptFunction(String type, String functionName, int defaultErrorResponseCode) throws InterruptedException, ScriptException, CordraException {
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(type);
        return moduleHasJavaScriptFunction(functionName, moduleId, defaultErrorResponseCode);
    }

    private boolean moduleHasJavaScriptFunction(String functionName, String moduleId, int defaultErrorResponseCode) throws ScriptException, InterruptedException, CordraException {
        if (!cordraRequireLookup.exists(moduleId)) {
            return false;
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(functionName, moduleId, false, false, defaultErrorResponseCode, runner);
            if (methodJSObject == null) {
                return false;
            } else {
                return true;
            }
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }

    public boolean streamPayload(CordraObject co, Map<String, Object> context, DirectIo directIo) throws ScriptException, InterruptedException, CordraException {
        co = CordraService.copyOnlyIfNecessaryOfCordraObjectRemovingInternalMetadata(co);
        String moduleId = CordraRequireLookup.moduleIdForSchemaType(co.type);
        if (!cordraRequireLookup.exists(moduleId)) {
            return false;
        }
        JavaScriptRunner runner = getJavaScriptRunner();
        try {
            JSObject methodJSObject = findJavaScriptFunctionWithRunner(ON_PAYLOAD_RESOLUTION, moduleId, false, false, 403, runner);
            if (methodJSObject == null) {
                return false;
            }
            String coJson = gson.toJson(co);
            JSObject obj = (JSObject) parseJsonForJavaScript(runner, coJson);
            JSObject contextJSObject = jsonObjectifyContext(runner, context);
            AtomicBoolean usedDirectOutput = new AtomicBoolean();
            JSObject prepareCallContext = (JSObject) runner.requireById(CordraRequireLookup.PREPARE_CALL_CONTEXT_MODULE_NAME);
            prepareCallContext.call(null, contextJSObject, directIo, usedDirectOutput, false);
            runJavaScriptFunctionWithRunner(methodJSObject, obj, contextJSObject, false, 403, runner);
            return usedDirectOutput.get();
        } finally {
            recycleJavaScriptRunner(runner);
        }
    }
}
