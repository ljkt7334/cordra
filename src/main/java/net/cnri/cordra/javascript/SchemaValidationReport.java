package net.cnri.cordra.javascript;

import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;

import java.util.ArrayList;
import java.util.List;

public class SchemaValidationReport {
    public boolean success;
    public List<Error> errors;

    public void addError(Error e) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        errors.add(e);
    }

    public void addError(String pointer, String message) {
        addError(new Error(pointer, message));
    }

    public void addError(String message) {
        addError(new Error(message));
    }

    public static class Error {
        public String pointer;
        public String message;

        public Error(String pointer, String message) {
            this.pointer = pointer;
            this.message = message;
        }

        public Error(String message) {
            this.message = message;
        }
    }

    public static SchemaValidationReport from(ProcessingReport processingReport) {
        SchemaValidationReport result = new SchemaValidationReport();
        result.success = processingReport.isSuccess();
        for (ProcessingMessage msg : processingReport) {
            if (msg.getLogLevel() == LogLevel.ERROR || msg.getLogLevel() == LogLevel.FATAL) {
                String jsonPointer = msg.asJson().at("/instance/pointer").textValue();
                String message = msg.getMessage();
                if (jsonPointer != null) {
                    result.addError(jsonPointer, message);
                } else {
                    result.addError(message);
                }
            }
        }
        return result;
    }
}
