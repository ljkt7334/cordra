package net.cnri.cordra.auth;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import net.cnri.cordra.api.BadRequestCordraException;

public class AuthenticationBackOffCordraException extends BadRequestCordraException {
    private final String user;
    private final long backOffMillis;

    public AuthenticationBackOffCordraException(String user, long backOffMillis) {
        super(buildResponse(user, backOffMillis), 429);
        this.user = user;
        this.backOffMillis = backOffMillis;
    }

    @SuppressWarnings("unused")
    private static JsonElement buildResponse(String user, long backOffMillis) {
        JsonObject obj = new JsonObject();
        obj.addProperty("message", "Too many authentication requests for user " + user + ", please try again later");
        return obj;
    }

    public String getUser() {
        return user;
    }

    public long getBackOffMillis() {
        return backOffMillis;
    }
}
