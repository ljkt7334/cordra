package net.cnri.cordra.auth;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.JsonParser;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.Design.CookieConfig;
import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.ForbiddenCordraException;
import net.cnri.cordra.api.UnauthorizedCordraException;
import net.cnri.cordra.web.ChangePasswordServlet;
import net.cnri.cordra.web.ServletUtil;

public class ServletAuthUtil {
    public static final String INTERNAL_REQUEST_OPTIONS_ATT_NAME = ServletAuthUtil.class.getName() + ".InternalRequestOptions";
    private static final String CORDRA_EXCEPTION_ATT_NAME = ServletAuthUtil.class.getName() + ".CordraException";

    private static CordraService getCordraService() {
        try {
            return CordraServiceFactory.getCordraService();
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    public static InternalRequestOptions getOptionsFromRequest(HttpServletRequest req, HttpServletResponse resp) throws CordraException {
        // this caching is also used to prevent backing off the same request twice; see AuthenticationBackOffFilter.handle
        InternalRequestOptions options = (InternalRequestOptions) req.getAttribute(INTERNAL_REQUEST_OPTIONS_ATT_NAME);
        if (options != null) return options;
        CordraException exception = (CordraException) req.getAttribute(CORDRA_EXCEPTION_ATT_NAME);
        if (exception != null) throw exception;
        try {
            options = innerGetOptionsFromRequest(req, resp);
            req.setAttribute(INTERNAL_REQUEST_OPTIONS_ATT_NAME, options);
            return options;
        } catch (AuthenticationBackOffCordraException e) {
            throw e;
        } catch (CordraException e) {
            req.setAttribute(CORDRA_EXCEPTION_ATT_NAME, e);
            throw e;
        }
    }

    public static InternalRequestOptions innerGetOptionsFromRequest(HttpServletRequest req, HttpServletResponse resp) throws CordraException {
        InternalRequestOptions options;
        if (checkPotentialSessionAuthentication(req)) {
            throwIfDisallowedAuthenticatingOverHttp(req);
            options = getOptionsFromSessionsUsingRequest(req, resp);
        } else {
            if (isCordraUseLegacySessionsApi() && !isPasswordChange(req)) {
                options = new LegacySessionsCallbackOptions(req, resp);
            } else {
                options = new InternalRequestOptions();
            }
            String authHeader = req.getHeader("Authorization");
            if (authHeader != null) {
                throwIfDisallowedAuthenticatingOverHttp(req);
                options.authHeader = authHeader;
            }
        }
        String asUser = req.getHeader("As-User");
        if (asUser != null) {
            options.asUserId = asUser;
        }
        options.isDryRun = ServletUtil.getBooleanParameter(req, "dryRun");
        options.full = ServletUtil.getBooleanParameter(req, "full");
        String requestContext = req.getParameter("requestContext");
        if (requestContext != null) {
            // TODO currently not used except by InternalCordraClientTest; the requestContext used in practice comes from RequestContextFilter
            options.requestContext = JsonParser.parseString(requestContext).getAsJsonObject();
        }
        return options;
    }

    public static void throwIfDisallowedAuthenticatingOverHttp(HttpServletRequest req) throws CordraException {
        if (getCordraService().getDesign().allowInsecureAuthentication == Boolean.TRUE) return;
        if (req.isSecure()) return;
        throw new ForbiddenCordraException("Authentication requires HTTPS");
    }

    private static boolean isPasswordChange(HttpServletRequest req) {
        return ChangePasswordServlet.SERVLET_PATH.equals(req.getServletPath());
    }

    private static boolean checkPotentialSessionAuthentication(HttpServletRequest req) {
        String authHeader = req.getHeader("Authorization");
        if (isBearerTokenForSession(authHeader)) {
            return true;
        } else if (authHeader == null) {
            if (req.getRequestedSessionId() == null) return false;
            if (!isCordraUseLegacySessionsApi()) return false;
            return true;
        } else {
            return false;
        }
    }

    private static InternalRequestOptions getOptionsFromSessionsUsingRequest(HttpServletRequest req, HttpServletResponse resp) {
        // see also CordraService.introspectTokenForAuthenticationResult
        return new SessionOptions() {
            {
                this.authHeader = req.getHeader("Authorization");
                this.token = req.getRequestedSessionId();
            }

            @Override
            public PreAuthenticatedOptions evaluate() throws CordraException {
                validateSessionAuthentication(req, resp);
                HttpSession session = req.getSession(false);
                PreAuthenticatedOptions options = new PreAuthenticatedOptions();
                options.authHeader = req.getHeader("Authorization");
                options.token = session.getId();
                options.username = (String) session.getAttribute("username");
                options.userId = (String) session.getAttribute("userId");
                options.grantAuthenticatedAccess = ServletUtil.getBooleanAttribute(session, "grantAuthenticatedAccess");
                options.exp = (Long) session.getAttribute("exp");
                @SuppressWarnings("unchecked")
                List<String> hookSpecifiedGroupIds = (List<String>) session.getAttribute("hookSpecifiedGroupIds");
                options.hookSpecifiedGroupIds = hookSpecifiedGroupIds;
                options.bypassCordraGroupObjects = session.getAttribute("bypassCordraGroupObjects") == Boolean.TRUE;
                return options;
            }
        };
    }

    private static void validateSessionAuthentication(HttpServletRequest req, HttpServletResponse resp) throws CordraException {
        String authHeader = req.getHeader("Authorization");
        if (isBearerTokenForSession(authHeader)) {
            HttpSession session = req.getSession(false);
            if (session == null || session.getAttribute("userId") == null || isSessionExpired(session)) {
                if (session != null) invalidateQuietly(session);
                resp.setHeader("WWW-Authenticate", "Bearer realm=\"cordra\", error=\"invalid_token\"");
                AuthTokenServlet.ErrorResponse errorResponse = new AuthTokenServlet.ErrorResponse();
                errorResponse.error = "invalid_token";
                errorResponse.message = "Invalid token";
                throw new UnauthorizedCordraException(GsonUtility.getGson().toJsonTree(errorResponse));
            }
        } else if (authHeader == null) {
            if (req.getRequestedSessionId() == null) return;
            if (!isCordraUseLegacySessionsApi()) return;
            HttpSession session = req.getSession(false);
            if (session == null || session.getAttribute("userId") == null || isSessionExpired(session)) {
                if (session != null) invalidateQuietly(session);
                throw new UnauthorizedCordraException("Invalid token");
            }
            String csrfTokenFromRequest = getCsrfTokenFromRequest(req);
            String csrfTokenFromSession = (String) session.getAttribute("csrfToken");
            if (csrfTokenFromRequest == null && !isGetOrHead(req)) {
                throw new UnauthorizedCordraException("X-Csrf-Token required");
            }
            if (csrfTokenFromRequest != null && !csrfTokenFromRequest.equals(csrfTokenFromSession)) {
                throw new UnauthorizedCordraException("CSRF failure");
            }
        }
    }

    public static boolean isSessionExpired(HttpSession session) {
        Long exp = (Long) session.getAttribute("exp");
        if (exp == null) return false;
        return exp < System.currentTimeMillis() / 1000;
    }

    public static void invalidateQuietly(HttpSession session) {
        try {
            session.invalidate();
        } catch (IllegalStateException e) {
            // already invalidated; ignore
        }
    }

    private static String getCsrfTokenFromRequest(HttpServletRequest req) {
        String res = req.getHeader("X-Csrf-Token");
        if (res != null) return res;
        if ("POST".equals(req.getMethod()) && "application/x-www-form-urlencoded".equals(req.getContentType())) {
            return req.getParameter("csrfToken");
        } else {
            return null;
        }
    }

    private static boolean isGetOrHead(HttpServletRequest req) {
        return "GET".equals(req.getMethod()) || "HEAD".equals(req.getMethod());
    }

    private static boolean isCordraUseLegacySessionsApi() {
        return getCordraService().getDesign().useLegacySessionsApi == Boolean.TRUE;
    }

    private static boolean isBearerTokenForSession(String authHeader) {
        // TODO Reconsider the requirement that the Bearer token for a Cordra session have no dot
        if (authHeader == null) return false;
        String[] parts = authHeader.trim().split(" +");
        if (parts.length != 2) return false;
        if (!"Bearer".equalsIgnoreCase(parts[0])) return false;
        if (parts[1].contains(".")) return false;
        return true;
    }

    private static class LegacySessionsCallbackOptions extends ResultCallbackOptions {

        private static final SecureRandom random = new SecureRandom();

        private final HttpServletRequest req;
        private final HttpServletResponse resp;
        private final CordraService cordra;

        public LegacySessionsCallbackOptions(HttpServletRequest req, HttpServletResponse resp) {
            this.req = req;
            this.resp = resp;
            this.cordra = getCordraService();
        }

        @Override
        public void callback(AuthenticationResult result) {
            if (!result.active || result.anonymous) return;
            HttpSession session = req.getSession(true);
            String csrfToken = (String) session.getAttribute("csrfToken");
            if (csrfToken == null) {
                csrfToken = generateSecureToken();
                session.setAttribute("csrfToken", csrfToken);
                Cookie csrfTokenCookie = new Cookie("Csrf-token", csrfToken);
                if (cordra.getDesign().cookies == null || cordra.getDesign().cookies.csrfToken == null || cordra.getDesign().cookies.csrfToken.path == null) {
                    csrfTokenCookie.setPath(getNonEmptyContextPath(req));
                }
                if (cordra.getDesign().cookies != null) {
                    setUpCookie(csrfTokenCookie, cordra.getDesign().cookies.csrfToken);
                }
                resp.addCookie(csrfTokenCookie);
            }
            if (req.getServletContext().getEffectiveSessionTrackingModes().isEmpty()) {
                Cookie sessionCookie = new Cookie("JSESSIONID", session.getId());
                if (cordra.getDesign().cookies == null || cordra.getDesign().cookies.jsessionid == null || cordra.getDesign().cookies.jsessionid.path == null) {
                    sessionCookie.setPath(getNonEmptyContextPath(req));
                }
                if (cordra.getDesign().cookies == null || cordra.getDesign().cookies.jsessionid == null || cordra.getDesign().cookies.jsessionid.httpOnly == null) {
                    sessionCookie.setHttpOnly(true);
                }
                if (cordra.getDesign().cookies == null || cordra.getDesign().cookies.jsessionid == null || cordra.getDesign().cookies.jsessionid.secure == null) {
                    if (req.isSecure()) {
                        sessionCookie.setSecure(true);
                    }
                }
                if (cordra.getDesign().cookies != null) {
                    setUpCookie(sessionCookie, cordra.getDesign().cookies.jsessionid);
                }
                resp.addCookie(sessionCookie);
            }
            // see also CordraService.getToken
            if (result.username != null) session.setAttribute("username", result.username);
            session.setAttribute("userId", result.userId);
            session.setAttribute("grantAuthenticatedAccess", result.grantAuthenticatedAccess);
            session.setAttribute("hookSpecifiedGroupIds", result.hookSpecifiedGroupIds);
            session.setAttribute("bypassCordraGroupObjects", result.bypassCordraGroupObjects);
            if (result.exp != null) session.setAttribute("exp", result.exp);
        }

        private String generateSecureToken() {
            return new BigInteger(130, random).toString(32);
        }

        private void setUpCookie(Cookie cookie, CookieConfig config) {
            if (config == null) return;
            if (config.path != null) cookie.setPath(config.path);
            if (config.httpOnly != null) cookie.setHttpOnly(config.httpOnly);
            if (config.secure != null) cookie.setSecure(config.secure);
        }

        private static String getNonEmptyContextPath(HttpServletRequest req) {
            String contextPath = req.getContextPath();
            if (contextPath.isEmpty()) return "/";
            return contextPath;
        }
    }

}
