package net.cnri.cordra.auth;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.api.*;
import net.cnri.cordra.auth.AclEnforcer.Permission;

public class InternalAuthorizer {

    private final AclEnforcer aclEnforcer;

    public InternalAuthorizer(CordraService cordra) {
        this.aclEnforcer = cordra.getAclEnforcer();
    }

    public void authorizeReindexBatch(AuthenticationResult authResult) throws CordraException {
        ensureAdmin(authResult);
    }

    public void authorizeUpdateAllHandleRecords(AuthenticationResult authResult) throws CordraException {
        ensureAdmin(authResult);
    }

    public void authorizeGetHandleUpdateStatus(AuthenticationResult authResult) throws CordraException {
        ensureAdmin(authResult);
    }

    public void authorizeCall(AuthenticationResult authResult, String method, CordraObject co, String type) throws CordraException {
        if ("admin".equals(authResult.userId)) return;
        boolean isPermitted = aclEnforcer.isPermittedToCall(authResult, co, method, type);
        if (isPermitted) return;
        ensureAuthenticated(authResult);
        throw new ForbiddenCordraException("Forbidden");
    }

    public void authorizeRead(AuthenticationResult authResult, CordraObject co) throws CordraException {
        authorizePermission(authResult, co, Permission.READ);
    }

    public void authorizeReadPayloads(AuthenticationResult authResult, CordraObject co) throws CordraException {
        authorizePermission(authResult, co, Permission.READ_INCLUDING_PAYLOADS);
    }

    public void authorizeWrite(AuthenticationResult authResult, CordraObject co) throws CordraException {
        authorizePermission(authResult, co, Permission.WRITE);
    }

    private void authorizePermission(AuthenticationResult authResult, CordraObject co, Permission requiredPermission) throws CordraException {
        if ("admin".equals(authResult.userId)) return;
        Permission perm = aclEnforcer.permittedOperations(authResult, co);
        boolean result = AclEnforcer.doesPermissionAllowOperation(perm, requiredPermission);
//        if (result == false && perm != Permission.NONE && cordra.getCordraObjectOrNull(objectId) == null) {
//            // allow people to know when an object does not exist (if they have default read perm)
//            throw new NotFoundCordraException("Missing object: " + objectId);
//        }
        if (result) return;
        ensureAuthenticated(authResult);
        throw new ForbiddenCordraException("Forbidden");
    }

    public void authorizeCreate(AuthenticationResult authResult, String type) throws CordraException {
        if ("admin".equals(authResult.userId)) return;
        boolean isPermitted = aclEnforcer.isPermittedToCreate(authResult, type);
        if (isPermitted) return;
        ensureAuthenticated(authResult);
        throw new ForbiddenCordraException("Forbidden");
    }

    private void ensureAuthenticated(AuthenticationResult authResult) throws CordraException {
        if (authResult.anonymous) {
            throw new UnauthorizedCordraException("Unauthenticated");
        }
    }

    private void ensureAdmin(AuthenticationResult authResult) throws CordraException {
        ensureAuthenticated(authResult);
        if (!"admin".equals(authResult.userId)) {
            throw new ForbiddenCordraException("Forbidden");
        }
    }

}
