package net.cnri.cordra.auth;

import com.google.gson.JsonObject;

public class RequestAuthenticationInfo {
    public String userId;
    public String username;
    public String password;
    public String token;

    public String authHeader;
    public JsonObject authTokenInput;
    public String doipClientId;
    public JsonObject doipAuthentication;

    public String asUserId;
}
