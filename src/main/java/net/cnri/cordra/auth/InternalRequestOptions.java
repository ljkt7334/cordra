package net.cnri.cordra.auth;

import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.api.Options;

public class InternalRequestOptions extends Options {
    public boolean isGet;
    public boolean isChangePassword;
    public boolean excludeVersions = true; // TODO used by ObjectsUploadServlet, consider adding to QueryParams

    public AuthenticationResult cachedResult;
    public CordraException cachedException;
    public boolean alreadyBackedOff;
}
