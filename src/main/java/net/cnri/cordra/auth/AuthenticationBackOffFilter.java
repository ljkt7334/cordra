package net.cnri.cordra.auth;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.AsyncContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.cnri.cordra.CordraService;
import net.cnri.cordra.CordraServiceFactory;
import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.CordraException;
import net.cnri.cordra.web.ChangePasswordServlet;
import net.cnri.cordra.web.ServletUtil;
import net.cnri.cordra.web.doip.DoipPerformOperationServlet;

public class AuthenticationBackOffFilter implements Filter {

    private static final String EXEC_ATT = AuthenticationBackOffFilter.class.getName() + ".exec";

    // async dispatch passes to a container thread and returns immediately; 1 thread might actually be enough
    private ScheduledExecutorService exec = Executors.newScheduledThreadPool(16);
    private InternalCordraClient internalCordra;
    private CordraService cordra; //Used only to get the design

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.internalCordra = InternalCordraClientFactory.get();
        this.cordra = CordraServiceFactory.getCordraService();
        ((ScheduledThreadPoolExecutor)exec).setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        filterConfig.getServletContext().setAttribute(EXEC_ATT, exec);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        doHttpFilter((HttpServletRequest)request, (HttpServletResponse)response, chain);
    }

    public void doHttpFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        if (req.getServletPath().startsWith("/auth/")) {
            chain.doFilter(req, resp);
            return;
        }
        if (ServletUtil.isMultipart(req)) {
            // temporary workaround to an apparent bug in our Jetty version which prevents getParts after async dispatch
            chain.doFilter(req, resp);
            return;
        }
        if (cordra.isDisableBackOffRequestParking()) {
            chain.doFilter(req, resp);
            return;
        }
        try {
            InternalRequestOptions options;
            if (req.getServletPath().equals("/doip") || req.getServletPath().startsWith("/doip/")) {
                options = DoipPerformOperationServlet.getOptionsFromRequest(req);
            } else {
                options = ServletAuthUtil.getOptionsFromRequest(req, resp);
            }
            if (options != null) {
                if (req.getServletPath().equals(ChangePasswordServlet.SERVLET_PATH)) options.isChangePassword = true;
                internalCordra.internalAuthenticate(options);
            }
        } catch (AuthenticationBackOffCordraException e) {
            handle(req, resp, e);
            return;
        } catch (CordraException e) {
            // ignore, will see again
        }
        chain.doFilter(req, resp);
        return;
    }

    @Override
    public void destroy() {
        exec.shutdown();
    }

    public static void handle(HttpServletRequest req, HttpServletResponse resp, AuthenticationBackOffCordraException e) {
        InternalRequestOptions options = (InternalRequestOptions) req.getAttribute(ServletAuthUtil.INTERNAL_REQUEST_OPTIONS_ATT_NAME);
        if (options != null) options.alreadyBackedOff = true;
        ScheduledExecutorService exec = (ScheduledExecutorService) req.getServletContext().getAttribute(EXEC_ATT);
        AsyncContext async = req.startAsync(req, resp);
        exec.schedule(() -> async.dispatch(), e.getBackOffMillis(), TimeUnit.MILLISECONDS);
    }
}
