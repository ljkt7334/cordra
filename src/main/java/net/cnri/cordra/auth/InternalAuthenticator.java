package net.cnri.cordra.auth;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.cnri.cordra.*;
import net.cnri.cordra.api.*;
import net.cnri.cordra.util.JacksonUtil;
import net.cnri.cordra.util.SearchUtil;
import net.handle.apps.batch.BatchUtil;
import net.handle.hdllib.HandleException;
import net.handle.hdllib.HandleResolver;
import net.handle.hdllib.HandleValue;
import net.handle.hdllib.Util;
import net.handle.hdllib.trust.JsonWebSignature;
import net.handle.hdllib.trust.JsonWebSignatureFactory;
import net.handle.hdllib.trust.TrustException;

import javax.script.ScriptException;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class InternalAuthenticator {

    private final CordraService cordra;
    private final HandleResolver resolver;
    private final AuthenticatorBackOff authenticatorBackOff;

    private Options defaultCredentials;

    public InternalAuthenticator(CordraService cordra) {
        this.cordra = cordra;
        this.resolver = new HandleResolver();
        this.authenticatorBackOff = new AuthenticatorBackOff(() -> this.cordra.getDesign().disableAuthenticationBackOff != Boolean.TRUE);
        this.defaultCredentials = new PreAuthenticatedOptions();
        this.defaultCredentials.userId = "admin";
        this.defaultCredentials.username = "admin";
    }

    InternalAuthenticator(CordraService cordra, HandleResolver resolver) {
        this.cordra = cordra;
        this.resolver = resolver;
        this.authenticatorBackOff = new AuthenticatorBackOff(() -> this.cordra.getDesign().disableAuthenticationBackOff != Boolean.TRUE);
    }

    public AuthenticationResult authenticate(Options options) throws CordraException {
        if (options instanceof InternalRequestOptions) {
            InternalRequestOptions internalOptions = (InternalRequestOptions) options;
            if (internalOptions.cachedResult != null) return internalOptions.cachedResult;
            if (internalOptions.cachedException != null) throw internalOptions.cachedException;
        }
        boolean wasNoContext = RequestContextHolder.beforeAuthCall();
        try {
            AuthenticationResult result = doAuthenticate(options);
            // If there's an asUserId, but it already matches the result, consider it handled.
            // This allows the authenticate hook to handle it, but falls back to default logic otherwise.
            if (options.asUserId != null && !options.asUserId.equals(result.userId)) {
                if (!"admin".equals(result.userId)) {
                    throw new ForbiddenCordraException("Not authorized to use As-User");
                }
                result.userId = options.asUserId;
                result.grantAuthenticatedAccess = cordra.doesCordraObjectExist(result.userId);
                result.groupIds = cordra.getAclEnforcer().getGroupsForUser(options.asUserId);
            }
            if (options instanceof ResultCallbackOptions) {
                ((ResultCallbackOptions) options).callback(result);
            }
            if (options instanceof InternalRequestOptions) {
                ((InternalRequestOptions) options).cachedResult = result;
            }
            return result;
        } catch (AuthenticationBackOffCordraException e) {
            throw e;
        } catch (CordraException e) {
            if (options instanceof InternalRequestOptions) {
                ((InternalRequestOptions) options).cachedException = e;
            }
            throw e;
        } finally {
            RequestContextHolder.afterAuthCall(wasNoContext);
        }
    }

    private AuthenticationResult doAuthenticate(Options options) throws CordraException {
        if (options.useDefaultCredentials) options = defaultCredentials;
        unpackAuthOptions(options);
        if (!(options instanceof PreAuthenticatedOptions)) {
            AuthenticationResult authResult = handleHookAuthentication(options);
            if (authResult != null) return authResult;
        }
        if (options instanceof SessionOptions) {
            options = ((SessionOptions) options).evaluate();
        }
        if (options instanceof PreAuthenticatedOptions) {
            PreAuthenticatedOptions paOptions = (PreAuthenticatedOptions) options;
            List<String> groupIds;
            if (paOptions.bypassCordraGroupObjects) {
                groupIds = paOptions.hookSpecifiedGroupIds;
            } else {
                groupIds = cordra.getAclEnforcer().getGroupsForUserAndGroups(paOptions.userId, paOptions.hookSpecifiedGroupIds);
            }
            return new AuthenticationResult(true, options.userId, options.username, groupIds, paOptions.grantAuthenticatedAccess, paOptions.exp);
        }
        String userInfo = options.userId;
        if (userInfo == null) userInfo = options.username;
        if (options.token != null) {
            if (options.token.contains(".")) {
                AuthenticationResult authResult = checkJwtCredentials(options.token);
                return authResult;
            } else {
                return doAuthenticateSessionToken(options.token);
            }
        } else if (options.privateKey != null) {
            AuthenticationResult authResult = checkPrivateKey(userInfo, options.privateKey);
            return authResult;
        } else if (options.password != null) {
            long backoff;
            if (options instanceof InternalRequestOptions && ((InternalRequestOptions) options).alreadyBackedOff) {
                backoff = 0;
            } else {
                backoff = authenticatorBackOff.calculateBackOffFor(userInfo);
            }
            if (backoff == 0) {
                try {
                    AuthenticationResult authResult = checkCredentials(userInfo, options.password, isChangePassword(options));
                    authenticatorBackOff.reportResult(userInfo, authResult.active);
                    return authResult;
                } catch (UnauthorizedCordraException e) {
                    authenticatorBackOff.reportResult(userInfo, false);
                    throw e;
                }
            } else {
                throw new AuthenticationBackOffCordraException(userInfo, backoff);
            }
        } else if (options.authHeader != null) {
            // you have an auth header
            // but it's neither Basic or Bearer
            throw new UnauthorizedCordraException("Unknown authorization header");
        } else if (options.authTokenInput != null) {
            // If this is /auth/token, and JavaScript hook didn't handle the input,
            // do the default error handling -- which requires a specialized response.
            // Unclear how to untangle this coupling, but it is limited.
            AuthTokenServlet.handleCordraDefaultAuthTokenErrors(options);
            throw new UnauthorizedCordraException("Unknown /auth/token input");
        } else if (options.doipClientId != null || options.doipAuthentication != null) {
            throw new UnauthorizedCordraException("Unknown DOIP authentication");
        } else {
            return AuthenticationResult.anonymous();
        }
    }

    // returns null if default authentication should proceed
    private AuthenticationResult handleHookAuthentication(Options options) throws CordraException {
        RequestAuthenticationInfo authInfo = extractRequestAuthenticationInfo(options);
        if (!hasInfoForHook(authInfo)) return null;
        AuthenticationResult authResult;
        try {
            authResult = cordra.authenticateViaHook(authInfo);
        } catch (InternalErrorCordraException e) {
            if ("admin".equals(options.userId) || "admin".equals(options.username)) {
                //Allow default authentication to continue. This is a fail-safe that allows
                //admin to attempt to sign in  even when the authenticate JS hook is broken
                return null;
            } else if (options instanceof SessionOptions) {
                PreAuthenticatedOptions paOptions = ((SessionOptions) options).evaluate();
                if ("admin".equals(paOptions.userId)) return null;
                throw e;
            } else {
                throw e;
            }
        }
        if (authResult == null) return null;
        if (!authResult.active && !authResult.anonymous) {
            throw new UnauthorizedCordraException("Authentication failed");
        }
        if (authResult.active && authResult.userId == null) {
            throw new InternalErrorCordraException("authenticate hook returned active but no userId");
        }
        fixUpGroupsFromHook(authResult);
        if (authResult.grantAuthenticatedAccess == null) {
            authResult.grantAuthenticatedAccess = authResult.userId != null && cordra.doesCordraObjectExist(authResult.userId);
        }
        return authResult;
    }

    private void fixUpGroupsFromHook(AuthenticationResult authResult) throws CordraException {
        if (authResult.groupIds == null) authResult.groupIds = Collections.emptyList();
        authResult.hookSpecifiedGroupIds = authResult.groupIds;
        if (!authResult.bypassCordraGroupObjects) {
            authResult.groupIds = cordra.getAclEnforcer().getGroupsForUserAndGroups(authResult.userId, authResult.groupIds);
        }
    }

    private AuthenticationResult doAuthenticateSessionToken(String token) throws CordraException {
        AuthenticationResult authResult = cordra.introspectTokenForAuthenticationResult(token, true, null);
        if (!authResult.active) {
            throw new UnauthorizedCordraException("Invalid token");
        }
        return authResult;
    }

    private void unpackAuthOptions(Options options) {
        if (options.userId != null || options.username != null || options.password != null || options.privateKey != null || options.token != null) {
            // already unpacked
            return;
        }
        if (options.authHeader != null) {
            if (options.authHeader.startsWith("Bearer ")) {
                options.token = options.authHeader.substring(7);
            } else if (options.authHeader.startsWith("Basic ")) {
                Credentials c = new Credentials(options.authHeader);
                options.username = c.getUsername();
                options.password = c.getPassword();
            }
        } else if (options.authTokenInput != null) {
            TokenRequest tokenRequest = GsonUtility.getGson().fromJson(options.authTokenInput, TokenRequest.class);
            if ("urn:ietf:params:oauth:grant-type:jwt-bearer".equals(tokenRequest.grant_type)) {
                options.token = tokenRequest.assertion;
            } else if ("password".equals(tokenRequest.grant_type)) {
                options.username = tokenRequest.username;
                options.password = tokenRequest.password;
            } else if (tokenRequest.grant_type == null && tokenRequest.username != null && tokenRequest.password != null) {
                options.username = tokenRequest.username;
                options.password = tokenRequest.password;
            }
        } else if (options.doipClientId != null || options.doipAuthentication != null) {
            TokenRequest tokenRequest = GsonUtility.getGson().fromJson(options.doipAuthentication, TokenRequest.class);
            if (tokenRequest.token != null) {
                options.token = tokenRequest.token;
            } else if (tokenRequest.password != null) {
                if (options.doipClientId != null) options.userId = options.doipClientId;
                else options.username = tokenRequest.username;
                options.password = tokenRequest.password;
            }
        }
    }

    private boolean isChangePassword(Options options) {
        if (options instanceof InternalRequestOptions) {
            return ((InternalRequestOptions) options).isChangePassword;
        } else {
            return false;
        }
    }

    private RequestAuthenticationInfo extractRequestAuthenticationInfo(Options options) {
        RequestAuthenticationInfo authInfo = new RequestAuthenticationInfo();
        authInfo.userId = options.userId;
        authInfo.username = options.username;
        authInfo.password = options.password;
        authInfo.token = options.token;
        authInfo.authHeader = options.authHeader;
        authInfo.authTokenInput = options.authTokenInput;
        authInfo.doipClientId = options.doipClientId;
        authInfo.doipAuthentication = options.doipAuthentication;
        authInfo.asUserId = options.asUserId;
        return authInfo;
    }

    private boolean hasInfoForHook(RequestAuthenticationInfo authInfo) {
        if (authInfo.userId != null) return true;
        if (authInfo.username != null) return true;
        if (authInfo.password != null) return true;
        if (authInfo.token != null) return true;
        if (authInfo.authHeader != null) return true;
        if (authInfo.authTokenInput != null) return true;
        if (authInfo.doipClientId != null) return true;
        if (authInfo.doipAuthentication != null) return true;
        return false;
    }

    // for testing
    AuthenticationResult checkPrivateKey(String userId, PrivateKey privateKey) throws CordraException {
        ActualIssuerAndKeys result = getPublicKeysFor(userId);
        boolean isValid = false;
        for (PublicKey publicKey : result.keys) {
            try {
                byte[] bytes = new byte[1024];
                ThreadLocalRandom.current().nextBytes(bytes);
                Signature sig = Signature.getInstance("SHA256with" + publicKey.getAlgorithm());
                sig.initSign(privateKey);
                sig.update(bytes);
                byte[] sigBytes = sig.sign();
                sig.initVerify(publicKey);
                sig.update(bytes);
                isValid = sig.verify(sigBytes);
                if (isValid) break;
            } catch (Exception e) {
                // not valid
            }
        }
        if (!isValid) throw new UnauthorizedCordraException("Authentication failed");
        String iss = result.iss;
        if (!cordra.isUserAccountActive(iss)) {
            throw new UnauthorizedCordraException("Account is inactive");
        }
        boolean grantAuthenticatedAccess = cordra.doesCordraObjectExist(iss);
        List<String> groupIds = cordra.getAclEnforcer().getGroupsForUser(iss);
        return new AuthenticationResult(true, iss, null, groupIds, grantAuthenticatedAccess, null);
    }

    public AuthenticationResult checkJwtCredentials(String jwt) throws CordraException {
        JsonWebSignatureFactory signatureFactory = JsonWebSignatureFactory.getInstance();
        try {
            JsonWebSignature jws = signatureFactory.deserialize(jwt);
            String iss = cordraKeyPairSuccessfulAuthenticationIssuer(jws);
            if (iss != null) {
                if (!cordra.isUserAccountActive(iss)) {
                    throw new UnauthorizedCordraException("Account is inactive");
                }
                boolean grantAuthenticatedAccess = cordra.doesCordraObjectExist(iss);
                List<String> groupIds = cordra.getAclEnforcer().getGroupsForUser(iss);
                return new AuthenticationResult(true, iss, null, groupIds, grantAuthenticatedAccess, null);
            } else {
                throw new UnauthorizedCordraException("JWT validation error");
            }
        } catch (TrustException e) {
            throw new UnauthorizedCordraException("JWT validation error");
        }
    }

    // for testing
    String cordraKeyPairSuccessfulAuthenticationIssuer(String jwt) throws Exception {
        JsonWebSignature jws = JsonWebSignatureFactory.getInstance().deserialize(jwt);
        String iss = cordraKeyPairSuccessfulAuthenticationIssuer(jws);
        return iss;
    }

    private String cordraKeyPairSuccessfulAuthenticationIssuer(JsonWebSignature jws) {
        try {
            String payloadJson = jws.getPayloadAsString();
            JsonObject claims = JsonParser.parseString(payloadJson).getAsJsonObject();
            if (!checkClaims(claims)) return null;
            String iss = claims.get("iss").getAsString();
            ActualIssuerAndKeys result = getPublicKeysFor(iss);
            for (PublicKey publicKey : result.keys) {
                boolean isValid = jws.validates(publicKey);
                if (isValid) {
                    return result.iss;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    private ActualIssuerAndKeys getPublicKeysFor(String iss) throws CordraException {
        List<PublicKey> result = new ArrayList<>();
        if ("admin".equals(iss)) {
            PublicKey adminPublickey = cordra.getAdminPublicKey();
            result.add(adminPublickey);
            return new ActualIssuerAndKeys("admin", result);
        }
        CordraObject user = null;
        if (iss.contains("/")) {
            user = getUserObjectById(iss);
        }
        if (user == null) {
            user = getUserObjectByUsername(iss); // Try with iss as a username
        }
        if (user == null && !iss.contains("/")) {
            user = getUserObjectById(iss);
        }
        if (user != null) {
            PublicKey keyFromCordraObject = getPublicKeyFromCordraObject(user);
            if (keyFromCordraObject != null) {
                result.add(keyFromCordraObject);
                return new ActualIssuerAndKeys(user.id, result);
            }
        }
        try {
            String handle = iss; //Try with iss as a handle
            HandleValue[] values = BatchUtil.resolveHandle(handle, resolver, null);
            List<HandleValue> pubkeyValues = BatchUtil.getValuesOfType(values, "HS_PUBKEY");
            for (HandleValue value : pubkeyValues) {
                try {
                    PublicKey publicKey = Util.getPublicKeyFromBytes(value.getData(), 0);
                    result.add(publicKey);
                } catch (Exception e) {
                    // unable to parse public key
                }
            }
        } catch (HandleException e) {
            // error resolving handle
        }
        return new ActualIssuerAndKeys(iss, result);
    }

    private PublicKey getPublicKeyFromCordraObject(CordraObject user) throws InternalErrorCordraException {
        String type = user.type;
        JsonNode jsonNode = JacksonUtil.gsonToJackson(user.content);
        Map<String, JsonNode> pointerToSchemaMap;
        try {
            pointerToSchemaMap = cordra.getPointerToSchemaMap(type, jsonNode);
        } catch (InvalidException e) {
            throw new InternalErrorCordraException(e);
        }
        for (Map.Entry<String, JsonNode> entry : pointerToSchemaMap.entrySet()) {
            String jsonPointer = entry.getKey();
            JsonNode subSchema = entry.getValue();
            JsonNode authNode = SchemaUtil.getDeepCordraSchemaProperty(subSchema, "auth");
            if (authNode != null && authNode.isObject()) {
                authNode = authNode.get("type");
            }
            if (authNode == null || !"publicKey".equals(authNode.asText())) continue;
            JsonNode publicKeyNode = JacksonUtil.getJsonAtPointer(jsonPointer, jsonNode);
            String publicKeyJson = JacksonUtil.printJson(publicKeyNode);
            PublicKey publicKey = GsonUtility.getGson().fromJson(publicKeyJson, PublicKey.class);
            return publicKey;
        }
        return null;
    }

    private static class ActualIssuerAndKeys {
        public String iss;
        public List<PublicKey> keys;

        public ActualIssuerAndKeys(String iss, List<PublicKey> keys) {
            this.iss = iss;
            this.keys = keys;
        }
    }

    private boolean checkClaims(JsonObject claims) {
        if (!claims.has("iss") || !claims.has("exp")) {
            // need an issuer
            // need an expiration date (prevent footguns)
            return false;
        }
        String iss = claims.get("iss").getAsString();
        if (claims.has("sub")) {
            // if sub is present, must match issuer
            String sub = claims.get("sub").getAsString();
            if (!iss.equals(sub)) {
                return false;
            }
        }
        long exp = claims.get("exp").getAsLong();
        long nowInSeconds = System.currentTimeMillis() / 1000L;
        if (nowInSeconds > exp) {
            return false;
        }
        if (exp > nowInSeconds + 3600) {
            // need an expiration date less than an hour in the future (prevent footguns)
            return false;
        }
        long nbf = 0;
        if (claims.has("nbf")) {
            nbf = claims.get("nbf").getAsLong();
        }
        if (nowInSeconds + 300 < nbf) {
            // 5 minute clock skew for nbf
            return false;
        }
        if (claims.has("aud")) {
            JsonElement audElement = claims.get("aud");
            if (!checkAudience(audElement)) {
                return false;
            }
        }
        if (claims.has("jti")) {
            String jti = claims.get("jti").getAsString();
            if (!cordra.getKeyPairAuthJtiChecker().check(iss, jti, exp, nowInSeconds)) {
                return false;
            }
        }
        return true;
    }

    private boolean checkAudience(JsonElement audElement) {
        List<String> aud;
        if (audElement.isJsonPrimitive()) {
            aud = Collections.singletonList(audElement.getAsString());
        } else if (audElement.isJsonArray()) {
            aud = StreamSupport.stream(audElement.getAsJsonArray().spliterator(), false)
                    .map(JsonElement::getAsString)
                    .collect(Collectors.toList());
        } else {
            return false;
        }
        List<String> ids = cordra.getIds();
        boolean found = false;
        for (String audId : aud) {
            if (ids.contains(audId)) {
                found = true;
                break;
            }
        }
        return found;
    }

    private AuthenticationResult checkCredentials(String username, String password, boolean isChangePassword) throws CordraException {
        if ("admin".equalsIgnoreCase(username)) {
            if (cordra.checkAdminPassword(password)) {
                String userId = "admin";
                AuthenticationResult authResult = new AuthenticationResult(true, userId, username, null, true, null);
                return authResult;
            } else {
                throw new UnauthorizedCordraException("Authentication failed");
            }
        }
        CordraObject user = null;
        if (username.contains("/")) {
            // assume it is user id
            user = getUserObjectById(username);
            if (user != null) username = null;
        }
        if (user == null) {
            user = getUserObjectByUsername(username);
        }
        if (user == null) {
            // username could be user id
            user = getUserObjectById(username);
            if (user != null) username = null;
        }
        if (user == null) {
            throw new UnauthorizedCordraException("Authentication failed");
        } else if (!cordra.isUserAccountActive(user)) {
            throw new UnauthorizedCordraException("Account is inactive");
        } else {
            boolean isPasswordCorrect = cordra.checkUserPassword(user, password);

            if (isPasswordCorrect) {
                if (!isChangePassword) {
                    boolean requiresPasswordChange = cordra.isPasswordChangeRequired(user);
                    if (requiresPasswordChange) {
                        JsonObject responseObj = new JsonObject();
                        responseObj.addProperty("message", "Password change required");
                        responseObj.addProperty(UnauthorizedCordraException.PASSWORD_CHANGE_REQUIRED_PROPERTY, true);
                        throw new UnauthorizedCordraException(responseObj);
                    }
                }
                String userId = user.id;
                List<String> groupIds = cordra.getAclEnforcer().getGroupsForUser(userId);
                return new AuthenticationResult(true, userId, username, groupIds, true, null);
            } else {
                throw new UnauthorizedCordraException("Authentication failed");
            }
        }
    }

    public CordraObject getUserObjectByUsername(String username) throws CordraException {
        AuthCache authCache = cordra.getAuthCache();
        if (authCache != null) {
            String cachedUserId = authCache.getUserIdForUsername(username);
            if (cachedUserId != null) {
                try {
                    return cordra.getCordraObjectOrNullWithInternalMetadataAndPostProcess(cachedUserId, AuthenticationResult.admin());
                } catch (ScriptException | InterruptedException e) {
                    throw new InternalErrorCordraException("Error post processing user object during authentication", e);
                }
            }
        }
        CordraObject user = null;
        String q = "username:\"" + SearchUtil.escape(username) + "\" -isVersion:true -objatt_isVersion:true";
        cordra.ensureIndexUpToDateWhenAuthChange();
        try (SearchResults<CordraObject> results = cordra.searchRepo(q)) {
            for (CordraObject co : results) {
                JsonElement foundUsername = co.metadata.internalMetadata.get("username");
                if (foundUsername != null && username.equalsIgnoreCase(foundUsername.getAsString())) {
                    user = co;
                }
            }
        } catch (UncheckedCordraException e) {
            e.throwCause();
        }
        if (user != null && authCache != null) {
            authCache.setUserIdForUsername(username, user.id);
        }
        if (user != null) {
            try {
                CordraObject userResult = cordra.postProcess(AuthenticationResult.admin(), user);
                CordraService.restoreInternalMetadata(user, userResult);
                user = userResult;
            } catch (ScriptException | InterruptedException e) {
                throw new InternalErrorCordraException("Error post processing user object during authentication", e);
            }
        }
        return user;
    }

    private CordraObject getUserObjectById(String userId) throws CordraException {
        try {
            return cordra.getCordraObjectOrNullWithInternalMetadataAndPostProcess(userId, AuthenticationResult.admin());
        } catch (ScriptException | InterruptedException e) {
            throw new InternalErrorCordraException("Error post processing user object during authentication", e);
        }
    }
}
