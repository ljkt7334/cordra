package net.cnri.cordra.auth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

public class WwwAuthenticateFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // no-op
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (!(response instanceof HttpServletResponse)) {
            chain.doFilter(request, response);
            return;
        }
        if (response instanceof WwwAuthenticateHttpServletResponseWrapper) {
            chain.doFilter(request, response);
            return;
        }
        if (response instanceof HttpServletResponseWrapper && ((HttpServletResponseWrapper) response).isWrapperFor(WwwAuthenticateHttpServletResponseWrapper.class)) {
            chain.doFilter(request, response);
            return;
        }
        chain.doFilter(request, new WwwAuthenticateHttpServletResponseWrapper((HttpServletResponse) response));
        return;
    }

    @Override
    public void destroy() {
        // no-op
    }

    private static class WwwAuthenticateHttpServletResponseWrapper extends HttpServletResponseWrapper {

        private final HttpServletResponse response;

        public WwwAuthenticateHttpServletResponseWrapper(HttpServletResponse response) {
            super(response);
            this.response = response;
        }

        @Override
        public void setStatus(int sc) {
            super.setStatus(sc);
            if (sc == HttpServletResponse.SC_UNAUTHORIZED) {
                if (response.getHeader("WWW-Authenticate") == null) {
                    response.setHeader("WWW-Authenticate", "Bearer realm=\"cordra\"");
                }
            }
        }

        @Override
        @SuppressWarnings("deprecation")
        public void setStatus(int sc, String sm) {
            super.setStatus(sc, sm);
            if (sc == HttpServletResponse.SC_UNAUTHORIZED) {
                if (response.getHeader("WWW-Authenticate") == null) {
                    response.setHeader("WWW-Authenticate", "Bearer realm=\"cordra\"");
                }
            }
        }
    }

}
