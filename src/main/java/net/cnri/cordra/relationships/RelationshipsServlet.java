package net.cnri.cordra.relationships;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.cnri.cordra.InternalCordraClient;
import net.cnri.cordra.InternalCordraClientFactory;
import net.cnri.cordra.api.Options;
import net.cnri.cordra.auth.ServletAuthUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import net.cnri.cordra.GsonUtility;
import net.cnri.cordra.web.ServletErrorUtil;
import net.cnri.cordra.web.ServletUtil;

@WebServlet("/relationships/*")
public class RelationshipsServlet extends HttpServlet {

    private static Logger logger = LoggerFactory.getLogger(RelationshipsServlet.class);

    private InternalCordraClient internalCordra;

    private Gson gson;

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            gson = GsonUtility.getGson();
            internalCordra = InternalCordraClientFactory.get();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.setContentType("application/json");
            resp.setCharacterEncoding("UTF-8");
            String path = ServletUtil.getPath(req);
            if (path == null || "".equals(path)) {
                ServletErrorUtil.badRequest(resp, "Missing objectId");
            } else {
                String objectId = path.substring(1);
                boolean outboundOnly = ServletUtil.getBooleanParameter(req, "outboundOnly");
                Options options = ServletAuthUtil.getOptionsFromRequest(req, resp);
                Relationships relationships = internalCordra.getRelationshipsFor(objectId, outboundOnly, options);
                gson.toJson(relationships, resp.getWriter());
            }
        } catch (Exception e) {
            logger.error("Unexpected error getting relationships", e);
            ServletErrorUtil.internalServerError(resp);
        }
    }
}
