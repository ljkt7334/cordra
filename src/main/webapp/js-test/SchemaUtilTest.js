(function(){
"use strict";

test("getDeepCordraSchemaProperty", function () {
    var jsonNode = {
        "cordra": {
            "a": {
                "b": "good",
                "c": "good"
            }
        },
        "net.cnri.repository": {
            "a": {
                "b": "bad",
                "d": "good"
            },
            "e": "good"
        }
    };
    var atNothing = SchemaUtil.getDeepCordraSchemaProperty(jsonNode);
    equal(atNothing, jsonNode.cordra);
    var useCordraFirst = SchemaUtil.getDeepCordraSchemaProperty(jsonNode, "a", "b");
    equal(useCordraFirst, "good");
    var useCordraOnly = SchemaUtil.getDeepCordraSchemaProperty(jsonNode, "a", "c");
    equal(useCordraOnly, "good");
    var useOld = SchemaUtil.getDeepCordraSchemaProperty(jsonNode, "a", "d");
    equal(useOld, "good");
    var useOld2 = SchemaUtil.getDeepCordraSchemaProperty(jsonNode, "e");
    equal(useOld2, "good");
});

})();