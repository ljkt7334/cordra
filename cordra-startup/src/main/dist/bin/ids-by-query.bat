@echo off

set PRG=%~dp0%
set CORDRADIR=%PRG%..

set CP=%CORDRADIR%\sw\lib\*;%CORDRADIR%\sw\lib\cordra-client\*;%CORDRADIR%\sw\lib\servletContainer\*;%CORDRADIR%\data\lib\*

set ARGS=
:args-loop
if "%~1"=="" goto :args-done
set ARGS=%ARGS% %1
shift
goto :args-loop
:args-done

java -cp "%CP%" net.cnri.cordra.util.cmdline.IdsByQuery %ARGS%
