package net.cnri.cordra.schema.networknt;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.Keyword;
import com.networknt.schema.ValidationContext;
import com.networknt.schema.ValidationMessage;

public class JsonPrunerNotValidator implements JsonValidator {

    private JsonValidator delegate;

    public JsonPrunerNotValidator(String schemaPath, JsonNode schemaNode, JsonSchema parentSchema, ValidationContext validationContext, Keyword keyword) throws Exception {
        delegate = keyword.newValidator(schemaPath, schemaNode, parentSchema, validationContext);
    }

    @Override
    public Set<ValidationMessage> walk(JsonNode node, JsonNode rootNode, String at, boolean shouldValidateSchema) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode rootNode) {
        return validate(rootNode, rootNode, JsonValidator.AT_ROOT);
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode node, JsonNode rootNode, String at) {
        Map<String, Collection<String>> origState = JsonPrunerUtil.prunerStateThreadLocal.get();
        JsonPrunerUtil.prunerStateThreadLocal.set(new HashMap<>());
        try {
            return delegate.validate(node, rootNode, at);
        } finally {
            JsonPrunerUtil.prunerStateThreadLocal.set(origState);
        }
    }

}
