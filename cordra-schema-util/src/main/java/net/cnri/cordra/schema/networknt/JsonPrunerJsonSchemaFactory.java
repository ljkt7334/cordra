package net.cnri.cordra.schema.networknt;

import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion;
import com.networknt.schema.ValidatorTypeCode;

import net.cnri.cordra.schema.SchemaResolver;

public class JsonPrunerJsonSchemaFactory {
    private static MinimalJsonSchemaFactory.Builder BUILDER = new MinimalJsonSchemaFactory.Builder()
        .withValidationToSpec(SpecVersion.VersionFlag.V4) // validation affect oneOf
        .withAdditionalKeyword(new JsonPrunerPropertiesEtcKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.PROPERTIES)))
        .withAdditionalKeyword(new JsonPrunerPropertiesEtcKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.PATTERN_PROPERTIES)))
        .withAdditionalKeyword(new JsonPrunerPropertiesEtcKeyword(new ParentInfoCachingKeyword(ValidatorTypeCode.ADDITIONAL_PROPERTIES)))
        .withAdditionalKeyword(new JsonPrunerReplacingKeyword(ValidatorTypeCode.ANY_OF, JsonPrunerAnyOfValidator.class))
        .withAdditionalKeyword(new ParentInfoCachingKeyword(new JsonPrunerReplacingKeyword(ValidatorTypeCode.CONTAINS, JsonPrunerContainsValidator.class)))
        .withAdditionalKeyword(new JsonPrunerReplacingKeyword(ValidatorTypeCode.NOT, JsonPrunerNotValidator.class))
        .withAdditionalKeyword(new JsonPrunerReplacingKeyword(ValidatorTypeCode.ONE_OF, JsonPrunerOneOfValidator.class));

    public static JsonSchemaFactory withSchemaResolver(SchemaResolver schemaResolver) {
        JsonSchemaFactory res = BUILDER.build();
        NetworkntJsonSchemaUtil.setSchemaResolver(res, schemaResolver);
        return res;
    }
}
