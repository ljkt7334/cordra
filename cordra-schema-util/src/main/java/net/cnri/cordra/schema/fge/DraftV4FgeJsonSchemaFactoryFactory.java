package net.cnri.cordra.schema.fge;

import com.github.fge.jsonschema.SchemaVersion;
import com.github.fge.jsonschema.cfg.ValidationConfiguration;
import com.github.fge.jsonschema.cfg.ValidationConfigurationBuilder;
import com.github.fge.jsonschema.core.load.configuration.LoadingConfigurationBuilder;
import com.github.fge.jsonschema.format.draftv3.DateAttribute;
import com.github.fge.jsonschema.format.draftv3.TimeAttribute;
import com.github.fge.jsonschema.library.DraftV4Library;
import com.github.fge.jsonschema.library.LibraryBuilder;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import net.cnri.cordra.schema.SchemaResolver;

public class DraftV4FgeJsonSchemaFactoryFactory {
    private static JsonSchemaFactory INSTANCE;

    static {
        LibraryBuilder lib = DraftV4Library.get().thaw();
        lib.addFormatAttribute("date", DateAttribute.getInstance());
        lib.addFormatAttribute("time", TimeAttribute.getInstance());
        ValidationConfigurationBuilder vcb = ValidationConfiguration.newBuilder();
        FgeJsonSchemaUtil.clearLibraries(vcb);
        vcb.setDefaultLibrary(SchemaVersion.DRAFTV4.getLocation().toString(), lib.freeze());
        LoadingConfigurationBuilder lcb = FgeJsonSchemaUtil.buildRefAwareLoadingConfigurationBuilder();
        INSTANCE = JsonSchemaFactory.newBuilder()
            .setValidationConfiguration(vcb.freeze())
            .setLoadingConfiguration(lcb.freeze())
            .freeze();
    }

    public static JsonSchemaFactory newJsonSchemaFactory() {
        return newJsonSchemaFactory(null);
    }

    public static JsonSchemaFactory newJsonSchemaFactory(SchemaResolver resolver) {
        JsonSchemaFactory res = DraftV4FgeJsonSchemaFactoryFactory.INSTANCE.thaw().freeze();
        FgeJsonSchemaUtil.setSchemaResolver(res, resolver);
        return res;
    }

}
