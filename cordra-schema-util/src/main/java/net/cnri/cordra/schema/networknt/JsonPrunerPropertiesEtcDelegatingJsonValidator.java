package net.cnri.cordra.schema.networknt;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Sets;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.ValidationMessage;

public class JsonPrunerPropertiesEtcDelegatingJsonValidator implements JsonValidator {

    private final JsonValidator validator;
    private final boolean active;
    private final Set<String> properties;
    private final Set<Pattern> patternProperties;

    public JsonPrunerPropertiesEtcDelegatingJsonValidator(JsonValidator validator, String keyword, JsonSchema parentSchema) {
        this.validator = validator;
        if ("additionalProperties".equals(keyword)) {
            active = true;
        } else if ("properties".equals(keyword)) {
            if (parentSchema.getSchemaNode().has("additionalProperties")) {
                active = false;
            } else {
                active = true;
            }
        } else { // patternProperties
            if (parentSchema.getSchemaNode().has("additionalProperties")) {
                active = false;
            } else if (parentSchema.getSchemaNode().has("properties")) {
                active = false;
            } else {
                active = true;
            }
        }
        if (active) {
            JsonNode propertiesNode = parentSchema.getSchemaNode().get("properties");
            if (propertiesNode == null) {
                properties = Collections.emptySet();
            } else {
                properties = Sets.newHashSet(propertiesNode.fieldNames());
            }
            JsonNode patternPropertiesNode = parentSchema.getSchemaNode().get("patternProperties");
            if (patternPropertiesNode == null) {
                patternProperties = Collections.emptySet();
            } else {
                patternProperties = StreamSupport.stream(Spliterators.spliteratorUnknownSize(patternPropertiesNode.fieldNames(), Spliterator.ORDERED | Spliterator.NONNULL | Spliterator.IMMUTABLE), false)
                    .map(Pattern::compile)
                    .collect(Collectors.toSet());
            }
        } else {
            properties = null;
            patternProperties = null;
        }
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode rootNode) {
        return validate(rootNode, rootNode, JsonValidator.AT_ROOT);
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode node, JsonNode rootNode, String at) {
        Set<ValidationMessage> res = validator.validate(node, rootNode, at);
        if (active) {
            String jsonPointer = ParentInfoCachingUtil.determineJsonPointerFromAt(at);
            Map<String, Collection<String>> prunerState = JsonPrunerUtil.prunerStateThreadLocal.get();
            Collection<String> thisPrunerState = prunerState.computeIfAbsent(jsonPointer, k -> new HashSet<>());
            Iterator<String> iter = node.fieldNames();
            while (iter.hasNext()) {
                String prop = iter.next();
                if (properties.contains(prop)) thisPrunerState.add(prop);
                for (Pattern pattern : patternProperties) {
                    if (pattern.matcher(prop).find()) thisPrunerState.add(prop);
                }
                // it is an additional property
            }
        }
        return res;
    }

    @Override
    public Set<ValidationMessage> walk(JsonNode node, JsonNode rootNode, String at, boolean shouldValidateSchema) {
        throw new UnsupportedOperationException();
    }
}
