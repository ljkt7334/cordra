package net.cnri.cordra.schema.networknt;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.*;

public class ParentInfoCachingKeyword implements Keyword {

    private final Keyword delegate;

    public ParentInfoCachingKeyword(Keyword delegate) {
        this.delegate = delegate;
    }

    @Override
    public String getValue() {
        return delegate.getValue();
    }

    @Override
    public JsonValidator newValidator(String schemaPath, JsonNode schemaNode, JsonSchema parentSchema, ValidationContext validationContext) throws Exception {
        return new ParentInfoCachingJsonValidator(delegate.newValidator(schemaPath, schemaNode, parentSchema, validationContext));
    }

}
