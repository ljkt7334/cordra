package net.cnri.cordra.schema.networknt;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaException;
import com.networknt.schema.JsonValidator;
import com.networknt.schema.Keyword;
import com.networknt.schema.ValidationContext;
import com.networknt.schema.ValidationMessage;
import com.networknt.schema.ValidatorTypeCode;

public class JsonPrunerContainsValidator implements JsonValidator {

    private final JsonSchema schema;

    @SuppressWarnings("unused")
    public JsonPrunerContainsValidator(String schemaPath, JsonNode schemaNode, JsonSchema parentSchema, ValidationContext validationContext, Keyword keyword) throws Exception {
        if (schemaNode.isObject() || schemaNode.isBoolean()) {
            schema = new JsonSchema(validationContext, ValidatorTypeCode.CONTAINS.getValue(), parentSchema.getCurrentUri(), schemaNode, parentSchema);
        } else {
            schema = null;
        }
    }

    @Override
    public Set<ValidationMessage> walk(JsonNode node, JsonNode rootNode, String at, boolean shouldValidateSchema) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode rootNode) {
        return validate(rootNode, rootNode, JsonValidator.AT_ROOT);
    }

    @Override
    public Set<ValidationMessage> validate(JsonNode node, JsonNode rootNode, String at) {
        if (!node.isArray()) {
            // ignores non-arrays
            return Collections.emptySet();
        } else if (schema == null) {
            throw new JsonSchemaException("contains was not a valid JSON schema");
        } else if (node.size() == 0) {
            // Array was empty
            ValidationMessage message = ValidationMessage.of(ValidatorTypeCode.CONTAINS.getValue(), ValidatorTypeCode.CONTAINS,
                at, "but array was empty");
            return Collections.singleton(message);
        } else {
            Set<ValidationMessage> allErrors = new LinkedHashSet<>();
            boolean found = false;
            Map<String, Collection<String>> origState = JsonPrunerUtil.prunerStateThreadLocal.get();
            try {
                int i = 0;
                for (JsonNode n : node) {
                    Map<String, Collection<String>> thisState = new HashMap<>();
                    JsonPrunerUtil.prunerStateThreadLocal.set(thisState);
                    Set<ValidationMessage> errors = schema.validate(n, rootNode, at + "[" + i + "]");
                    if (errors.isEmpty()) {
                        found = true;
                        JsonPrunerUtil.mergeInto(origState, thisState);
                    } else {
                        allErrors.addAll(errors);
                    }
                    i++;
                }
            } finally {
                JsonPrunerUtil.prunerStateThreadLocal.set(origState);
            }
            if (found) {
                return Collections.emptySet();
            } else {
                return allErrors;
            }
        }
    }

}
