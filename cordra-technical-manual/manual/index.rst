Developer Resources
===================

.. toctree::
   :maxdepth: 2
   :hidden:
   
   Developer Resources (in PDF) <https://hdl.handle.net/20.1000/129>
   Cordra Software <introduction/index>
   Application Design <design/index>
   APIs <api/index>
   Clients <client/index>
   Configuration <configuration/index>
   Extensions and Applications <extensions/index>
   Tools <tools/index>
