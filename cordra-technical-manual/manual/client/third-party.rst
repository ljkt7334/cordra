Third Party Clients
===================

A Cordra REST API client library in Python is being developed (in part) by a few employees at NIST. A pre-alpha version
is available `here <https://github.com/usnistgov/CordraPy>`__.
