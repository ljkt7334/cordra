.. _javascript-lifecycle-hooks:

Lifecycle Hooks
==========================

Cordra validates incoming information against schemas as defined in the Type objects. Additional rules that validate
and/or enrich the information in the object can be configured to be applied by Cordra at various stages of the object
lifecycle. Such rules are expected to be specified in JavaScript and to be bound to the following lifecycle points:

* before an object is schema validated (with an opportunity, during create, to set the id or properties which help determine the id);
* during id generation;
* before schema validation but after id generation;
* before an object is stored, allowing for additional validation or side-effects after all other validation is done;
* before an object is indexed, allowing the object that is indexed to differ from the one that is stored;
* during handle record creation;
* after an object (or payload) has been retrieved from storage, but before it is returned;
* before an object is deleted, to forbid deletion under some circumstances;
* after an object is deleted;
* after an object is created or updated; and
* before executing a user-supplied query.

Lifecycle hooks, in our parlance, are the points of entry for your own JavaScript-based rules that Cordra processes.
In addition to the lifecycle hooks that are discussed in detail below, Cordra enables clients to invoke other rules
in an ad hoc fashion using :ref:`type-methods`.

Currently, various lifecycle hooks are enabled in Cordra for different actions: create, update, retrieve, and delete.

The following diagrams illustrate hooks that are enabled during various stages of the object lifecycle.

.. figure:: ../_static/lifecycle/cordra-create-lifecycle.png
        :align: center
        :alt: Create Lifecycle

.. figure:: ../_static/lifecycle/cordra-update-lifecycle.png
        :align: center
        :alt: Update Lifecycle

.. figure:: ../_static/lifecycle/cordra-retrieve-lifecycle.png
        :align: center
        :alt: Retrieve Lifecycle

.. figure:: ../_static/lifecycle/cordra-delete-lifecycle.png
        :align: center
        :alt: Delete Lifecycle

.. raw:: latex

    \clearpage

Using Hooks in JavaScript
-------------------------

Hooks in a Type Object
~~~~~~~~~~~~~~~~~~~~~~

Most lifecycle hooks are available for use as part of the JavaScript associated with each Type object. This means if you
want to leverage these hooks for multiple types of objects, then you will need to edit the JavaScript for each of the
types.  See :ref:`using-external-modules` for a method to share code among multiple types.

Here is the shell of the hooks that are available in each Type, which will be explained below.

.. code-block:: js
   :linenos:

    const cordra = require('cordra');
    const cordraUtil = require('cordraUtil');
    const schema = require('/cordra/schemas/Type.schema.json');
    const js = require('/cordra/schemas/Type');

    exports.beforeSchemaValidation = beforeSchemaValidation;
    exports.beforeSchemaValidationWithId = beforeSchemaValidationWithId;
    exports.beforeStorage = beforeStorage;
    exports.objectForIndexing = objectForIndexing;
    exports.onObjectResolution = onObjectResolution;
    exports.onPayloadResolution = onPayloadResolution;
    exports.beforeDelete = beforeDelete;
    exports.afterDelete = afterDelete;
    exports.afterCreateOrUpdate = afterCreateOrUpdate;
    exports.getAuthConfig = getAuthConfig;

    function beforeSchemaValidation(object, context) {
        /* Insert code here */
        return object;
    }

    function beforeSchemaValidationWithId(object, context) {
        /* Insert code here */
        return object;
    }

    function beforeStorage(object, context) {
        /* Insert code here */
    }

    function objectForIndexing(object, context) {
        /* Insert code here */
        return object;
    }

    function onObjectResolution(object, context) {
        /* Insert code here */
        return object;
    }

    function onPayloadResolution(object, context) {
        /* Insert code here; use context.directIo to write payload bytes */
    }

    function beforeDelete(object, context) {
        /* Insert code here */
    }

    function afterDelete(object, context) {
        /* Insert code here */
    }

    function afterCreateOrUpdate(object, context) {
        /* Insert code here */
    }

    function getAuthConfig(context) {
        /* Insert code here */
    }

Cordra provides two convenience JavaScript modules that can be imported for use within your JavaScript rules. These
modules allow you to search and retrieve objects, and verify hashes and secrets.  Additional modules allow you
to retrieve schemas and associated
JavaScript hooks, as discussed :ref:`here <using_cordra_modules>`. You can optionally include these modules in
your JavaScript, as shown on Lines 1-4.

You can also save external JavaScript libraries in Cordra for applying complex logic as discussed
:ref:`here <using-external-modules>`.

Lines 6-14 export references to the hooks that Cordra enables on a Type object: ``beforeSchemaValidation``, ``beforeSchemaValidationWithId``,
``beforeStorage``, ``objectForIndexing``, ``onObjectResolution``, ``onPayloadResolution``, ``beforeDelete``,
``afterDelete`` and ``afterUpdateOrCreate``,. When handling objects, Cordra will look for methods with these names and
run them if found. The methods must be exported in order for Cordra to see them. None of the methods is mandatory. You
only need to implement the ones you want.

Resolution of payloads will activate both ``onObjectResolution`` and ``onPayloadResolution``.  If ``onPayloadResolution`` accesses
the output via ``context.directIo`` (see :ref:`directIo`) the hook will fully control the output bytes, and the stored payload will
not be directly returned to the client.

The rest of the example shell shows the boilerplate for the methods. All, except for ``getAuthConfig``, take both an
``object`` and a ``context``. ``object`` is the JSON representation of the Cordra object. It may be modified and returned by
``beforeSchemaValidation``, ``beforeSchemaValidationWithId``, ``objectForIndexing``, and ``onObjectResolution``.

``object`` contains ``id``, ``type``, ``content``, ``acl``, ``metadata``, and ``payloads`` (which has payload metadata,
not the full payload data). ``content`` is the user defined JSON of the object.

``object`` has the following format::

    {
        "id": "test/abc",
        "type": "Document",
        "content": { },
        "acl": {
            "readers": [
                "test/user1",
                "test/user2"
            ],
            "writers": [
                "test/user1"
            ]
        },
        "metadata": {
            "createdOn": 1532638382843,
            "createdBy": "admin",
            "modifiedOn": 1532638383096,
            "modifiedBy": "admin",
            "txnId": 967
        }
    }

``context`` is an object with several useful properties.

============================   =====
Property Name                  Value
============================   =====
isNew                          Flag which is true for creations and false for modifications.
                               Applies to beforeSchemaValidation.
objectId                       The id of the object.
userId                         The id of the user performing the operation.
groups                         A list of the ids of groups to which the user belongs.
effectiveAcl                   The computed ACLs for the object, either from the object itself or inherited from configuration.
                               This is an object with "readers" and "writers" properties.
aclCreate                      The creation ACL for the type being created, in beforeSchemaValidation for a creation.
newPayloads                    A list of payload metadata for payloads being updated, in beforeSchemaValidation for an update operation.
payloadsToDelete               A list of payload names of payloads being deleted, in beforeSchemaValidation for an update operation.
requestContext                 A user-supplied requestContext query parameter.
payload                        The payload name for onPayloadResolution.
start, end                     User-supplied start and end points for a partial payload resolution for onPayloadResolution.
params                         The input supplied to a :ref:`type-methods` call.
directIo                       Can be used for more control over input/output with :ref:`type-methods` or onPayloadResolution; see :ref:`directIo`.
isSearch                       Flag set to true in onObjectResolution if the call is being made to produce search results.
isDryRun                       Set on a create or update according to the "dryRun" parameter.  Could be used
                               to prevent external side effects.
method                         Set to the method name in beforeStorage or afterCreateOrUpdate when activated after an updating type method call
                               rather than an ordinary create or update.
originalObject                 The object before it was updated, in beforeStorage or afterCreateOrUpdate.
beforeSchemaValidationResult   The object after beforeSchemaValidation and beforeSchemaValidationWithId but before other processing,
                               notably before the removal of properties with secureProperty,
                               in beforeStorage or afterCreateOrUpdate.
============================   =====

.. _beforeStorage:

Before Storage Hook
~~~~~~~~~~~~~~~~~~~

This ``beforeStorage`` hook is run immediately before the object is inserted into storage. It has gone through all other
processing and contains the generated id if an id was not included in the request. There is no return value for this
function. As such you cannot edit the object here; however, you can perform additional validation and throw an exception
if you want to reject the object.


.. _hooksForCordraDesignAndSchema:

Hooks for the Design Object and Type Objects
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The design object (of type CordraDesign) and Type objects (of type Schema) do not have separate Type objects.
These built-in types can still have lifecycle hooks, however.  Their JavaScript modules can be defined under a property
"builtInTypes" of the design object, specifically "builtInTypes.CordraDesign.javascript" and "builtInTypes.Schema.javascript".
See :ref:`design-object`.


.. _getAuthConfig:

Get Auth Config Hook
~~~~~~~~~~~~~~~~~~~~

This ``getAuthConfig`` hook is unlike other hooks defined on a Type object in that it is not called with an instance of
an object of that type.

The shell for this hook is as follows:

.. code-block:: js

    exports.getAuthConfig = getAuthConfig;

    function getAuthConfig(context) {
        return {
            "defaultAclRead": [
                "test/group1"
            ],
            "defaultAclWrite": [
                "test/group1"
            ],
            "aclCreate": [
                "test/group1"
            ]
        };
    }



.. _generateId:

Generate Object Id Hook
~~~~~~~~~~~~~~~~~~~~~~~

This hook, that is to be stored in the Design object, is for generating object ids when objects are created. The JavaScript can
be edited by selecting ``Design JavaScript`` from the ``Admin`` menu on the UI. The hook will be bound to the property
"javascript" in the Design object (so it can be edited there too, see :ref:`design-object`).

The shell for this hook is as follows:

.. code-block:: js

    exports.generateId = generateId;
    exports.isGenerateIdLoopable = true;

    function generateId(object, context) {
       var id;
       /* Insert code here */
       return id;
    }

The flag ``isGenerateIdLoopable`` when set to true tells Cordra that if an object with the same id already exists this
method can be called repeatedly until a unique id is found. If the implementation of generateId was deterministic,
which is to say it would always return the same id for a given input object, the ``isGenerateIdLoopable`` should NOT
be set to true.

If null or undefined or the empty string is returned, Cordra will use its default identifier-minting logic.

.. warning::

    In versions of Cordra before 2.4.0, ``generateId`` was guaranteed to run after schema validation.
    Cordra 2.4.0 introduced ``beforeSchemaValidationWithId``, which requires ``generateId`` to run
    before schema validation.  In order to preserve backward compatibility, ``generateId`` will
    currently still run after schema validation when ``beforeSchemaValidationWithId`` is unused; however,
    this flow should be considered deprecated and should not be relied on when writing new hook code.
    New ``generateId`` code should be robust to input which is not schema-valid, although throwing or
    returning null or undefined would suffice.

.. _authenticate:

Authenticate Hook
~~~~~~~~~~~~~~~~~

This hook is also looked for in the "javascript" property of the design object.  It will be executed for every
user request that requires authentication.

See :ref:`external-authentication-provider` for a substantial example of how this hook could be used.

The shell for this hook is as follows:

.. code-block:: js

    exports.authenticate = authenticate;

    function authenticate(authInfo, context) {
       /* Insert code here */

       return { /* ... */ };
    }

The authInfo passed into this function has the following structure:

.. code-block:: js

    {
        "username": "",
        "password": "",
        "token": "",
        "authHeader": "",
        "authTokenInput": {},
        "doipAuthentication": {},
        "doipClientId": "",
        "asUserId": ""
    }

The hook can inspect "username" and "password", or "token", for typical authentication scenarios; but the hook
also has access to the raw authentication data in various forms should it be needed.

.. tabularcolumns:: |\X{1}{3}|\X{2}{3}|

==================   ======================
Property Name        Description
==================   ======================
username             String.  The username from a Basic HTTP Authorization header, or from username/password input to /auth/token or DOIP authentication.

password             String.  The password from a Basic HTTP Authorization header, or from username/password input to /auth/token or DOIP authentication.

token                String.  The Bearer token from a Bearer HTTP Authorization header, or a JWT assertion made as /auth/token input, or a token from DOIP authentication.

authHeader           String.  The value of the request's HTTP Authorization header.

authTokenInput       Object. The input to the /auth/token endpoint or the
                     20.DOIP/Op.Auth.Token operation. See :ref:`auth-token-api`.

doipAuthentication   Object. If the request come in over the DOIP interface this
                     property will contain the the value of the passed in DOIP
                     authentication.

doipClientId         String. If the request came in over the DOIP interface this
                     property will contain the clientId, if available.

asUserId             String. The ``asUserId`` property may be optionally set to
                     indicate that the call should should be performed with the
                     permissions that user has been granted; see :ref:`as-user`.

                     Many authenticate hook use cases can ignore this.

                     Default Cordra authentication logic only allows admin to use As-User; the authenticate hook
                     can handle the As-User request by returning a userId which has the value of the provided
                     asUserId, and can allow users other than admin as desired.
                     Only set the output userId to the input asUserId when you really want to allow
                     the authenticating user the full privileges of the asUserId.
                     If the hook return userId does not match the
                     input asUserId, the default As-User logic will occur; to prevent even admin use of As-User,
                     throw an exception.

                     Note that if this property is set, and you are returning
                     custom groupIds which you wish to be available using As-User, you should set userId to the asUserId,
                     and return the groupIds for the asUserId.
==================   ======================

Returning null or undefined causes the default cordra authentication code to be executed in that case.

To reject an authentication attempt throw an exception.

To accept an authentication return a result object. The result object should contain at least ``"active": true`` and the userId. It can
optionally contain username and groupIds, where groupIds are the groups the user is a member of.

The active property on the result object should be set to true. If it is set to false it will cause an exception to be
thrown.

.. warning::

    Note that the userId "admin" will be given the full privileges of the Cordra admin
    user.  Thus take care that the behavior of any external authentication provider
    used by the authenticate hook does not give users full control over their own userIds.

An example successful authentication result object is shown below:

.. code-block:: js

    {
        "active": true
        "userId": "test/123",
        "username": "fred",
        "groupIds": [ "test/group1" ],
        "grantAuthenticatedAccess": true,
        "bypassCordraGroupObjects": true
    }


.. tabularcolumns:: |\X{1}{3}|\X{2}{3}|

=================================   ====================
Property Name                       Description
=================================   ====================
active                              Boolean. Set to true if the
                                    authentication is successful.

userId                              String. The userId of the authenticated user.

username                            String (Optional). The username of the
                                    authenticated user.

groupIds                            List of String (Optional). Custom groupIds the
                                    authenticated user is a member of.

bypassCordraGroupObjects            Boolean (Optional) Used in combination with ``groupIds``.
                                    If set to true only the returned ``groupIds`` will be used.
                                    If missing or set to false any Cordra group objects that
                                    contain this users id will be combined with the custom
                                    groupIds.

grantAuthenticatedAccess            Boolean (Optional). If set to true this user is considered
                                    "authenticated" by ACLs that use that keyword.
                                    If missing Cordra ACLs will only consider this user
                                    "authenticated" if their exists a Cordra object that
                                    corresponds with their userId. If a hook allows authentication
                                    by a wide open set of users, such as any users with an account
                                    with some existing global service, then it would be
                                    appropriate to set this false.  If your ACLs do not use the
                                    "authenticated" keyword, this has no effect.

exp                                 Number (Optional). Expiration in Epoch Unix Timestamp, the number
                                    of seconds since JAN 01 1970. (UTC) that the session should last
                                    for before expiration. If not supplied by default the session
                                    will not expire.
=================================   ====================

There is one additional special response which the authenticate hook can return:

.. code-block:: js

    {
        "anonymous": true
    }

This response causes Cordra to behave as if there was no authentication attempt.

.. warning::

    It is possible to lock out all users, including admin, by throwing an exception from the authenticate hook.
    Doing so would then prevent admin from signing in to edit the problem JavaScript. To recover from such a
    situation place the following example repoInit.json file in the data directory. Upon restart, this will delete all the
    JavaScript on the design object allowing admin to sign in to fix it.

    .. code-block:: js

        {
            "design": {
                "javascript": ""
            }
        }


.. _customizeQueryAndParams:

Customize Query and Params Hook
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This hook is also looked for in the "javascript" property of the design object.  It will be executed for every
user-supplied query to the Search API.  It can be used for example to restrict the query to exclude certain objects
based on the calling user. It can also be used to modify the query params supplied by the user for example to restrict
the number of results per page, or to throw an exception when a user uses search parameters which are to be
disallowed to that user.  Query restriction can be accomplished either by altering the query or adding filterQueries
(see :ref:`searchApi`).

The shell for this hook is as follows:

.. code-block:: js

    exports.customizeQueryAndParams = customizeQueryAndParams;

    function customizeQueryAndParams(queryAndParams, context) {
       /* Insert code here */
       queryAndParams.query = newQuery;
       queryAndParams.pageSize = 5;
       return queryAndParams;
    }


.. _customizeQuery:

Customize Query Hook
~~~~~~~~~~~~~~~~~~~~

This hook is similar to the above-mentioned customizeQueryAndParams but only allows customization of the query string. If
hooks customizeQueryAndParams and customizeQuery are both implemented, both will be executed with customizeQuery being run after.

The shell for this hook is as follows:

.. code-block:: js

    exports.customizeQuery = customizeQuery;

    function customizeQuery(query, context) {
       /* Insert code here */
       return newQuery;
    }


.. _createHandleValues:

Create Handle Values Hook
~~~~~~~~~~~~~~~~~~~~~~~~~

This hook is for specifying the handle record that is to be returned when handles are resolved
using handle client tools.  This hook is on the separate Design object property ``design.handleMintingConfig.javascript``,
which can be edited by selecting ``Handle Records`` from the ``Admin`` menu on the UI.

The shell for this hook is as follows:

.. code-block:: js

    exports.createHandleValues = createHandleValues;

    function createHandleValues(object, context) {
       const handleValues = [];
       /* Insert code here */
       return handleValues;
    }

If creating handle values with JavaScript it is important to consider that all Cordra objects, even if not publicly
visible, will have a handle record created. If you are storing data directly in the handle record you may wish to check
if the Cordra object is publicly accessible. You can do this by inspecting the 'context' argument. For example:

.. code-block:: js

    function isPublic(context) {
        const effectiveAcl = context.effectiveAcl;
        if (effectiveAcl.writers && effectiveAcl.writers.indexOf("public") !== -1) {
            return true;
        } else if (effectiveAcl.readers && effectiveAcl.readers.indexOf("public") !== -1) {
            return true;
        } else {
            return false;
        }
    }

.. _asynchronous_lifecycle_hooks:

Asynchronous Lifecycle Hooks
----------------------------

The return value of any hook (or type method) can be a JavaScript promise.
When a hook returns a promise, Cordra will wait until the promise is fulfilled or rejected.
If the promise is fulfilled, Cordra will use the value to which the promise resolved.
If the promise is rejected, Cordra will treat the rejection reason like it would a thrown exception.
This facility makes it possible to use asynchronous libraries to write Cordra hooks and type methods.

.. _throwing_errors_in_schema_javascript:

Throwing Errors in Schema JavaScript
------------------------------------

Errors thrown in as strings will end up in the server response, with the thrown string
as the error message.

.. code-block:: js

    throw "An error has occurred";

If the user requests are issued via the REST API, for beforeSchemaValidation and Type methods calls, this will be
returned to the user as a 400 Bad Request. For onObjectResolution and beforeDelete, this will be returned as 403
Forbidden. For search results where onObjectResolution throws an exception, the corresponding object will be omitted
from the search results (this can affect search results count).

If the user requests are issued via the DOIP interface, a "bad request" or "forbidden" error will be returned.

For more control over the server response, you can also throw a custom ``CordraError``, available via
the :ref:`cordra_module`. For example:

.. code-block:: js

    const cordra = require("cordra");

    const responseCode = 418; // defaults to 400 or (for resolution) 403 if undefined
    const response = {
        message: "Beverage Not Supported",
        requestedBeverage: "coffee",
        supportedBeverages: ["tea", "water"]
    };
    throw new cordra.CordraError(response, responseCode);

This will be translated into a server response with the given error message and response status code. If present,
the ``response`` object will be added to the body of the server response. This can be used to send extra
information about the error back to the caller.

As a convenience, if the first argument to ``new cordra.CordraError`` is a string, the response will be
``{"message":"that string"}``.

Thrown errors other than strings and CordraError will be seen by the user as 500 Internal Server Error.

.. _using_cordra_modules:

Cordra Modules
--------------

.. _cordra_module:

Cordra.js Module
~~~~~~~~~~~~~~~~

The builtin Cordra.js module has helpful functions, listed below, that may be useful when writing JavaScript code in Type methods.

**Note:** Lifecycle hooks are triggered when calls are made using the external APIs. Calls made to Cordra using the
helpful functions in the cordra.js module do not trigger any lifecycle hooks.

Search
""""""

Use the search function to find objects in the Cordra index::

    cordra.search(query, params)

This will return an array (in JSON sense) of Cordra objects matching the query.
``params`` is an optional object with optional fields ``pageNum``, ``pageSize``,
``sortFields``, ``facets``, and ``filterQueries``.

The default behavior is to get all results for a query, which corresponds to a ``pageNum`` of ``0``
and ``pageSize`` of ``-1``.
Caution should be used when requesting all results when the query might match a very large
number of objects.

The legacy form ``cordra.search(query, pageNum, pageSize, sortFields)`` still functions as well.

Note: Former versions of Cordra would return all results with pageSize=0.  To restore this former behavior, you can add
``"useLegacySearchPageSizeZeroReturnsAll":true`` to the Cordra Design object.  By default a search with pageSize=0
returns the number of matched objects but no object content.

Get
"""

Use get to get an object from Cordra by the object ID::

    cordra.get(objectId);

If an object with the given ID is found, it will be returned. Otherwise, ``null`` will be returned.

Payload Retrieval
"""""""""""""""""

Use any of the following to retrieve a payload from Cordra using the object ID and the payload name::

    cordra.getPayloadAsJavaInputStream(objectId, payloadName);
    cordra.getPayloadAsJavaReader(objectId, payloadName);
    cordra.getPayloadAsUint8Array(objectId, payloadName);
    cordra.getPayloadAsString(objectId, payloadName);
    cordra.getPayloadAsJson(objectId, payloadName);
    cordra.getPartialPayloadAsJavaInputStream(objectId, payloadName, start, end);
    cordra.getPartialPayloadAsJavaReader(objectId, payloadName, start, end);
    cordra.getPartialPayloadAsUint8Array(objectId, payloadName, start, end);
    cordra.getPartialPayloadAsString(objectId, payloadName, start, end);

CordraError
"""""""""""

See :ref:`throwing_errors_in_schema_javascript`.

.. _cordra_util_module:

CordraUtil.js Module
~~~~~~~~~~~~~~~~~~~~

Escape for Query
""""""""""""""""

Will modify a string for literal inclusion in a phrase query for calling ``cordra.search``::

    const query = '/property:"' + cordraUtil.escapeForQuery(s) + '"';

Verify Secret
"""""""""""""

Used to verify a given string against the hash stored for that property::

    cordraUtil.verifySecret(obj, jsonPointer, secretToVerify);

Return true or false, depending on the results of the verification.

Verify Hashes
"""""""""""""

Verifies the hashes on a cordra object property::

    cordraUtil.verifyHashes(obj);

Returns a verification report object indicating which of the object hashes verify.

Hash Json
"""""""""

Hashes a JSON object, JSON array or primitive::

    cordraUtil.hashJson(jsonElement);
    cordraUtil.hashJson(jsonElement, algorithm);

Returns a base16 encoded string of the SHA-256 hash (or other specified algorithm) of the input.
The input JSON is first canonicalized before being hashed.

Sign With Key
"""""""""""""

Signs a payload (a string) with a given private key in JWK format::

    const jws = cordraUtil.signWithKey(payload, jwk);

Returns a Json Web Signature in compact serialization.

Sign With Cordra Key
""""""""""""""""""""

Signs a payload (a string) with the private key of the Cordra instance::

    const jws = cordraUtil.signWithCordraKey(payload);

Returns a Json Web Signature in compact serialization.

The private key used is the same key used for administering an external handle server, and can be set by
including a file "privatekey" in the Cordra data directory.  See :ref:`external-handle-server` and also
:ref:`distributed-cordra-configuration` for the distributed version.

Retrieve Cordra Public Key
""""""""""""""""""""""""""

Returns the Cordra public key in JWK format::

    const jwk = cordraUtil.getCordraPublicKey();

Verify With Cordra Key
""""""""""""""""""""""

Verifies a JWS with the private key of the Cordra instance::

    const isValid = cordraUtil.verifyWithCordraKey(jws);

Verify With Key
"""""""""""""""

Verifies a JWS with the supplied private key in JWK format::

    const isValid = cordraUtil.verifyWithKey(jws, jwk);

Extract JWT Payload
"""""""""""""""""""

Extracts the payload of a JWT, returning the parsed JSON as a JavaScript object::

    const claimsObject = cordraUtil.extractJwtPayload(jws);

Get Groups for User
"""""""""""""""""""

Given a user id, returns a list of group ids, which are the Cordra group objects that reference the user id.
Does not interact with custom groups provided by the response of the "authenticate" hook.

::

    const groupIds = cordraUtil.getGroupsForUser(user);


Validate With Schema
""""""""""""""""""""

Given some variable ``input`` and a JSON schema, this function will validate the input against the JSON schema and
return a report::

    const input = {
        name: "foo",
        description: "bar"
    };

    const jsonSchema = {
        type: "object",
        required: [
            "name",
            "description"
        ],
        properties: {
           name: {
               type: "string"
           },
           description: {
               type: "string"
           }
       }
    };

    const report = cordraUtil.validateWithSchema(input, jsonSchema);


The resulting report with contain a boolean ``success`` and possibly a list of errors, where each error will have
a ``message`` and possibly a ``pointer`` indicating where in the JSON the error occurs.

The JsonSchema may use ``$ref`` to reference other schemas in Cordra. For example if you have a Cordra type named
``Foo`` you could do the following::

    const input = {
        name: "foo",
        description: "bar"
    };

    const jsonSchema = {
        "$ref": "Foo"
    };

    const report = cordraUtil.validateWithSchema(input, jsonSchema);


.. _cordra_schemas:

Cordra Schemas and JavaScript
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Schemas associated with type objects are available to the JavaScript via ``require('/cordra/schemas/Type.schema.json')``,
and JavaScript added to those type objects via ``require('/cordra/schemas/Type')``.  Here `Type` should be replaced
by the name of the particular type to be accessed.


.. _using-external-modules:

Using External Modules
----------------------

External JavaScript modules can be managed with a Cordra object as a payload configured to be a "referrable" source
of JavaScript modules. Typically, this can be done on a single object of a type called JavaScriptDirectory. Here are the
steps needed to create and populate the JavaScriptDirectory object.

#. Create a new schema in Cordra called "JavaScriptDirectory" and using the "javascript-directory" template.
#. Create a new JavaScriptDirectory object. Set the directory to ``/node_modules``. This will allow you to import modules
   by filename, instead of directory path and filename.
#. Add your JavaScript module files as payloads to the JavaScriptDirectory object. The payload name should match the
   filename and will be used when importing a module. For example, a payload named ``util.js`` could be importing using
   ``require('util');``

The use of external JavaScript modules affects reindexing.  It is currently necessary to ensure that objects of type
"Schema" and any sources of JavaScript (like type "JavaScriptDirectory") are indexed first.  See :ref:`reindexing`
for information.

JavaScript Version and Limitations
----------------------------------

Cordra uses the Nashorn JavaScript Engine packaged with Java. The version of JavaScript supported depends on the version
of Java used to run Cordra. Java 8 supports ECMAScript 5.1. As of the time of this writing, Java 9 supports some but not
all ECMAScript 6 features.

Cordra JavaScript does come prepopulated with a wide range of polyfills allowing features up to ECMAScript 2017.
It is thus in many cases straightforward to write ECMAScript 2017 code and transpile it (using for example Babel) to ES5 for use in Cordra.

In Java, there is a limit on the size of a single function. It is rare to run up against this limit writing Java code,
but it can happen when JavaScript is compiled to Java. This is especially true when using third-party libraries, which
may be minified in one large function. If you hit this limit, you will see the error "Code Too Large" in your logs.

.. _legacy-js:

Legacy JavaScript Hooks
-----------------------

In early versions of the Cordra 2.0 Beta software, the JavaScript hooks ``beforeSchemaValidation``,
``onObjectResolution``, and ``beforeDelete`` took the JSON content of the Cordra object, instead of the full Cordra
object (including id, type, content, acl, metadata, and payloads properties). Additionally the JavaScript ``cordra.get``
function returned only the content instead of the full Cordra object.

If a Cordra instance with JavaScript written for those earlier versions needs to be upgraded, and it is not yet possible
to adapt the JavaScript to the current API, then the following flag must be added to the Design object::

   "useLegacyContentOnlyJavaScriptHooks": true

For more information on editing the Design object, see :ref:`design-object`.

Cordra users upgrading from early versions of the Cordra 2.0 beta, who did not use schema JavaScript (apart from the
default User schema JavaScript, which will be automatically upgraded if it has not been edited), do not in general need
to take any action.


Examples of Hooks
-----------------

.. _userSchemaJsExample:

Example: User Schema JavaScript
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The default Cordra User schema comes with JavaScript that performs basic password validation.

.. code-block:: js

    const cordra = require("cordra");

    exports.beforeSchemaValidation = beforeSchemaValidation;

    function beforeSchemaValidation(object, context) {
        if (!object.content.id) object.content.id = "";
        if (!object.content.password) object.content.password = "";
        const password = object.content.password;
        if (context.isNew || password) {
            if (password.length < 8) {
                throw "Password is too short. Min length 8 characters";
            }
        }
        return object;
    }

This code will run before the given object is validated and stored. If this request is a create
(``context.isNew`` is true) or contains a ``password``, the password is checked to make sure it is long enough. If not,
an error is thrown. This error will be returned to the callee and can be displayed as desired.

Example: Document Modification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In this slightly more complicated example, we will bind lifecycle hooks to the Document type pre-defined in Cordra with
the following features:

* Add a timestamp to the description of the document in a way it is stored.
* Add a timestamp to the description when the object is resolved, but not actually store.
* Require that the description be changed to "DELETEME" before the document can be deleted.

To demonstrate loading JavaScript from an external file, the function to create the timestamp is in a file called
``util.js``. Create a JavaScript Directory (as described above) and upload this file as a payload named ``util.js``.

.. code-block:: js

    exports.getTimestampString = getTimestampString;

    function getTimestampString(isResolution) {
        const currentDate = new Date();
        if (isResolution) {
            return '\nResolved at: ' + currentDate;
        } else {
            return '\nLast saved: ' + currentDate;
        }
    }

Next, edit the Document type in Cordra and put the following in the JavaScript field.

.. code-block:: js

    const util = require('util');

    exports.beforeSchemaValidation = beforeSchemaValidation;
    exports.onObjectResolution = onObjectResolution;
    exports.beforeDelete = beforeDelete;

    function beforeSchemaValidation(object, context) {
        if (object.content.description !== 'DELETEME') {
            object.content.description += util.getTimestampString(false);
        }
        return object;
    }

    function onObjectResolution(object, context) {
        object.content.description += util.getTimestampString(true);
        return object;
    }

    function beforeDelete(object, context) {
        if (object.content.description !== 'DELETEME') {
            throw 'Description must be DELETEME before object can be deleted.';
        }
    }

Finally, create a new document in Cordra. You should see that whenever the document is updated, a new timestamp is
appended to the description. If you view the document's JSON, you should see a single resolution timestamp, which
changes on every resolution. Finally, if you try to delete the document without changing the description to "DELETEME"
you should see an error message.



.. _objectForIndexingExample:

Example: Modification of the Indexed Object
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible make changes to the object that is indexed such that it differs from the object that is stored. This is
achieved by writing a function called ``objectForIndexing``.

.. code-block:: js

    exports.objectForIndexing = objectForIndexing;

    function objectForIndexing(object, context) {
        if (object.content.name == "foo") {
            object.content.otherName = "bar";
        }
        return object;
    }

In this example if the incoming object has a property called ``name`` with the value ``foo``, a new property will be
added to the indexed object called ``otherName`` with the value ``bar``. The object that is stored will not contain
the new property but you will be able to search for this object via this property with the query ``/otherName:bar``.

.. _generateIdExample:

Example: Generating ID
~~~~~~~~~~~~~~~~~~~~~~

Example JavaScript for generating object ids is shown below. Here we generate a random suffix for the handle in base16
and append it to a prefix. By setting ``isGenerateIdLoopable`` to true, we ask Cordra to repeatedly call this method
until a unique id is generated.

.. code-block:: js

    const cordra = require('cordra');

    exports.generateId = generateId;
    exports.isGenerateIdLoopable = true;

    function generateId(object, context) {
        return "test/" + randomSuffix();
    }

    function randomSuffix() {
        return Math.random().toString(16).substr(2);
    }

.. _createHandleValuesExample:

Example: Creating Handle Values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Example JavaScript for creating handle values is shown below. The JavaScript puts a copy of the information from the
Cordra object in the Handle record.

.. code-block:: js

    exports.createHandleValues = createHandleValues;

    function createHandleValues(object) {
        const handleValues = [];
        const dataValue = {
            index: 500,
            type: 'CORDRA_OBJECT',
            timestamp: new Date(object.metadata.modifiedOn).toISOString(),
            data: {
                format: 'string',
                value: JSON.stringify(object.content)
            }
        };
        handleValues.push(dataValue);
        return handleValues;
    };
