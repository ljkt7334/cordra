package net.cnri.cordra.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Locale;

public class HttpUtil {
    public static String contentDispositionHeaderFor(String disposition, String filename) {
        if (filename == null) return disposition;
        String latin1Version;
        try {
            latin1Version = new String(filename.getBytes("ISO-8859-1"), "ISO-8859-1");
        } catch(UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
        String latin1VersionEscaped = latin1Version.replace("\n","?").replace("\\","\\\\").replace("\"","\\\"");
        if (filename.equals(latin1Version)) {
            return disposition + ";filename=\"" + latin1VersionEscaped + "\"";
        } else {
            try {
                return disposition + ";filename=\"" + latin1VersionEscaped + "\";filename*=UTF-8''" + URLEncoder.encode(filename,"UTF-8").replace("+","%20");
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
    }

    public static String getContentDispositionFilename(String contentDispositionHeader) {
        if (contentDispositionHeader == null) return null;
        int index = contentDispositionHeader.indexOf(';');
        if (index < 0) return null;
        String firstPart = contentDispositionHeader.substring(0, index);
        if (firstPart.contains("=") || firstPart.contains(",") || firstPart.contains("\"")) return null;
        String filename = null;
        String filenameStar = null;
        while (index >= 0) {
            int nameStart = index + 1;
            int equals = contentDispositionHeader.indexOf('=', nameStart);
            if (equals < 0) return null;
            String name = contentDispositionHeader.substring(nameStart, equals).trim().toLowerCase(Locale.ENGLISH);
            if (name.contains(",") || name.contains(";")) return null;
            int valueStart = skipWhitespace(contentDispositionHeader, equals + 1);
            if (valueStart >= contentDispositionHeader.length()) return null;
            char ch = contentDispositionHeader.charAt(valueStart);
            if (ch == ';') return null;
            boolean quoted = ch == '"';
            if (quoted) valueStart++;
            ch = ';';
            int valueEnd = valueStart;
            while (valueEnd < contentDispositionHeader.length()) {
                ch = contentDispositionHeader.charAt(valueEnd);
                if (quoted && ch == '"') break;
                if (!quoted && ch == ';') break;
                if (!quoted && (ch == ',' || ch == ' ')) return null;
                if (quoted && ch == '\\') {
                    valueEnd++;
                    if (valueEnd >= contentDispositionHeader.length()) break;
                }
                valueEnd++;
            }
            if (quoted && ch != '"') return null;
            if (quoted) {
                if (unexpectedCharactersAfterQuotedString(contentDispositionHeader, valueEnd)) return null;
            }
            if ("filename".equals(name) || "filename*".equals(name)) {
                String value = contentDispositionHeader.substring(valueStart, valueEnd).trim();
                if (quoted) {
                    value = unescapeQuotedStringContents(value);
                }
                if ("filename*".equals(name)) {
                    if (quoted) return null;
                    if (filenameStar != null) continue;
                    String decoding = rfc5987Decode(value);
                    if (decoding != null) {
                        filenameStar = decoding;
                    }
                } else {
                    if (filename != null) return null;
                    filename = value;
                }
            }
            index = contentDispositionHeader.indexOf(';', valueEnd);
        }
        if (filenameStar != null) filename = filenameStar;
        filename = stripPath(filename);
        return filename;
    }

    private static int skipWhitespace(String contentDispositionHeader, int index) {
        while (index < contentDispositionHeader.length()) {
            char ch = contentDispositionHeader.charAt(index);
            if (!Character.isWhitespace(ch)) break;
            index++;
        }
        return index;
    }

    private static boolean unexpectedCharactersAfterQuotedString(String contentDispositionHeader, int index) {
        int semicolon = contentDispositionHeader.indexOf(';', index);
        String remainder;
        if (semicolon < 0) {
            remainder = contentDispositionHeader.substring(index + 1);
        } else {
            remainder = contentDispositionHeader.substring(index + 1, semicolon);
        }
        return !remainder.trim().isEmpty();
    }

    private static String unescapeQuotedStringContents(String value) {
        StringBuilder sb = new StringBuilder(value);
        int escape = sb.indexOf("\\");
        while (escape >= 0) {
            sb.replace(escape, escape + 2, sb.substring(escape + 1, escape + 2));
            escape = sb.indexOf("\\", escape + 1);
        }
        value = sb.toString();
        return value;
    }

    private static String rfc5987Decode(String value) {
        int apos = value.indexOf('\'');
        int apos2 = -1;
        if (apos > 0) apos2 = value.indexOf('\'', apos + 1);
        if (apos2 < 0) return null;
        String enc = value.substring(0, apos);
        value = value.substring(apos2 + 1);
        try {
            return URLDecoder.decode(value.replace("+", "%2B"), enc);
        } catch (UnsupportedEncodingException e) {
            return null;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    private static String stripPath(String filename) {
        if (filename != null) {
            int lastSlash = filename.lastIndexOf('/');
            int lastBackslash = filename.lastIndexOf('\\');
            if (lastSlash > lastBackslash) filename = filename.substring(lastSlash + 1);
            else if (lastBackslash > lastSlash) filename = filename.substring(lastBackslash + 1);
        }
        return filename;
    }

    public static String getCharset(String mediaType) {
        if (mediaType == null) return "UTF-8";
        String[] parts = mediaType.split(";");
        String res = "UTF-8";
        for (int i = 1; i < parts.length; i++) {
            String part = parts[i].trim();
            int equalsIndex = part.indexOf('=');
            if (equalsIndex < 0) continue;
            String name = part.substring(0, equalsIndex).trim();
            String value = part.substring(equalsIndex+1).trim();
            if (name.equalsIgnoreCase("charset")) res = value;
        }
        return res;
    }
}
