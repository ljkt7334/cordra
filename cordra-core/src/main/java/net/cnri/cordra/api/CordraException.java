package net.cnri.cordra.api;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class CordraException extends Exception {
    private final int responseCode;
    private final JsonElement response;

    @Deprecated
    public CordraException(String message) {
        this(responseForMessage(message), 500);
    }

    @Deprecated
    public CordraException(Throwable cause) {
        this(500, cause);
    }

    @Deprecated
    public CordraException(String message, Throwable cause) {
        this(responseForMessage(message), 500, cause);
    }

    protected CordraException(JsonElement response) {
        this(response, 500);
    }

    protected CordraException(JsonElement response, Throwable cause) {
        this(response, 500, cause);
    }

    protected CordraException(JsonElement response, int responseCode) {
        super(messageForResponse(response));
        this.responseCode = responseCode;
        this.response = response;
    }

    protected CordraException(int responseCode, Throwable cause) {
        super(cause);
        this.responseCode = responseCode;
        this.response = null;
    }

    protected CordraException(JsonElement response, int responseCode, Throwable cause) {
        super(messageForResponse(response), cause);
        this.responseCode = responseCode;
        this.response = response;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public JsonElement getResponse() {
        return response;
    }

    public static String messageForResponse(JsonElement response) {
        if (response == null || response.isJsonNull()) return null;
        try {
            if (response.isJsonObject()) {
                return response.getAsJsonObject().get("message").getAsString();
            } else if (response.isJsonPrimitive()) {
                return response.getAsString();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static JsonElement responseForMessage(String message) {
        if (message == null) return null;
        JsonObject res = new JsonObject();
        res.addProperty("message", message);
        return res;
    }

    public static CordraException fromStatusCode(int statusCode) {
        return fromStatusCode(statusCode, null);
    }

    public static CordraException fromStatusCode(int statusCode, String responseString) {
        return fromStatusCode(statusCode, responseString, null);
    }

    public static CordraException fromStatusCode(int statusCode, String responseString, Throwable cause) {
        JsonElement response = null;
        if (responseString != null && !responseString.isEmpty()) {
            try {
                response = JsonParser.parseString(responseString);
            } catch (Exception e) {
                response = responseForMessage(responseString);
            }
        }
        return fromStatusCode(statusCode, response, cause);
    }

    public static CordraException fromStatusCode(int statusCode, JsonElement response, Throwable cause) {
        if (statusCode < 400) {
            return new InternalErrorCordraException(response, cause);
        } else if (400 == statusCode) {
            return new BadRequestCordraException(response, cause);
        } else if (401 == statusCode) {
            return new UnauthorizedCordraException(response, cause);
        } else if (403 == statusCode) {
            return new ForbiddenCordraException(response, cause);
        } else if (404 == statusCode) {
            return new NotFoundCordraException(response, cause);
        } else if (409 == statusCode) {
            return new ConflictCordraException(response, cause);
        } else if (statusCode < 500) {
            return new BadRequestCordraException(response, statusCode, cause);
        } else {
            return new InternalErrorCordraException(response, cause);
        }
    }
}
