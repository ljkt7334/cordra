package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class InternalErrorCordraException extends CordraException {

    public InternalErrorCordraException(String message, Throwable cause) {
        super(CordraException.responseForMessage(message), 500, cause);
    }

    public InternalErrorCordraException(String message) {
        super(CordraException.responseForMessage(message), 500);
    }

    public InternalErrorCordraException(Throwable cause) {
        super(500, cause);
    }

    public InternalErrorCordraException(JsonElement response, Throwable cause) {
        super(response, cause);
    }

    public InternalErrorCordraException(JsonElement response) {
        super(response);
    }
}
