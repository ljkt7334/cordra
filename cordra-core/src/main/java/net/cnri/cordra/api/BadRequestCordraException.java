package net.cnri.cordra.api;

import com.google.gson.JsonElement;

public class BadRequestCordraException extends CordraException {

    public BadRequestCordraException(String message) {
        this(CordraException.responseForMessage(message), 400);
    }

    public BadRequestCordraException(String message, Throwable cause) {
        this(CordraException.responseForMessage(message), 400, cause);
    }

    public BadRequestCordraException(Throwable cause) {
        this(400, cause);
    }

    public BadRequestCordraException(JsonElement response) {
        this(response, 400);
    }

    public BadRequestCordraException(JsonElement response, Throwable cause) {
        this(response, 400, cause);
    }

    public BadRequestCordraException(JsonElement response, int responseCode) {
        super(response, responseCode);
    }

    public BadRequestCordraException(int responseCode, Throwable cause) {
        super(responseCode, cause);
    }

    public BadRequestCordraException(JsonElement response, int responseCode, Throwable cause) {
        super(response, responseCode, cause);
    }

}
