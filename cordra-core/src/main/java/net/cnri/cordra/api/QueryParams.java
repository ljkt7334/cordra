package net.cnri.cordra.api;

import java.util.Collections;
import java.util.List;

/**
 * Parameters to a repository search, such as pagination and sorting.
 */
public class QueryParams {
    /**
     * Default query parameters.  Passing {@code null} to repository search methods amounts to using this.  No pagination and no sorting.
     */
    public static final QueryParams DEFAULT = new QueryParams();

    private final List<SortField> sortFields;
    private final int pageNum;
    private final int pageSize;
    private final List<FacetSpecification> facets;
    private final List<String> filterQueries;
    private final List<String> filter;

    // Included to support Gson
    /**
     * Construct default QueryParams, with pageSize = -1 (returns all results).
     * Prefer {@link #DEFAULT}.
     */
    public QueryParams() {
        this(0, -1);
    }

    /**
     * Construct a QueryParams.
     * @param pageNumber the page number to return.  Starts at 0.  Ignored if pageSize &lt;= 0.
     * @param pageSize the number of objects to return.  PageSize of &lt; 0 means return all.
     */
    public QueryParams(int pageNumber, int pageSize) {
        this(pageNumber, pageSize, Collections.emptyList());
    }

    public QueryParams(int pageNumber, int pageSize, List<SortField> sortFields) {
        this(pageNumber, pageSize, sortFields, null, null);
    }

    public QueryParams(int pageNumber, int pageSize, List<SortField> sortFields, List<FacetSpecification> facets, List<String> filterQueries) {
        this(pageNumber, pageSize, sortFields, facets, filterQueries, null);
    }

    public QueryParams(int pageNumber, int pageSize, List<SortField> sortFields, List<FacetSpecification> facets, List<String> filterQueries, List<String> filter) {
        this.pageNum = pageNumber;
        this.pageSize = pageSize;
        this.sortFields = sortFields;
        this.facets = facets;
        this.filterQueries = filterQueries;
        this.filter = filter;
    }

    @Deprecated
    public int getPageNumber() {
        return pageNum;
    }

    public int getPageNum() {
        return pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List<SortField> getSortFields() {
        return sortFields;
    }

    public List<String> getFilterQueries() { return filterQueries; }

    public List<FacetSpecification> getFacets() { return facets; }

    public List<String> getFilter() {
        return filter;
    }
}
